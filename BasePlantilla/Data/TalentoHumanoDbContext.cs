﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Data
{
    public class TalentoHumanoDbContext : DbContext
    {
        public TalentoHumanoDbContext(DbContextOptions<TalentoHumanoDbContext> options) : base(options)
        {
        }

        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Municipio> Municipios { get; set; }
        public DbSet<Banco> Bancos { get; set; }
        public DbSet<Sexo> Sexo { get; set; }
        public DbSet<TipoCuenta> TiposCuentas { get; set; }
        public DbSet<EstadoCivil> EstadosCivil { get; set; }
        public DbSet<TipoSangre> TiposSangre { get; set; }
        public DbSet<FondoPension> FondosPension { get; set; }
        public DbSet<Gerencias> Gerencias { get; set; }
        public DbSet<DepartamentoSiget> DepartamentosSiget { get; set; }
        public DbSet<Plaza> Plazas { get; set; }
        public DbSet<Empleado> Empleado { get; set; }
        public DbSet<EmpleadoPlaza> EmpleadoPlaza { get; set; }
        public DbSet<Permisos> Permisos { get; set; }
        public DbSet<TipoPermiso> TipoPermisos { get; set; }
        public DbSet<Vacaciones> Vacaciones { get; set; }
        public DbSet<Parametros> Parametros { get; set; }
        public DbSet<TipoAccionesDePersonal> TipoAccionPersonal { get; set; }
        public DbSet<TipoAcuerdo> TipoAcuerdos { get; set; }
        public DbSet<Acuerdos> Acuerdos { get; set; }
        public DbSet<AccionesDePersonal> AccionesDePersonal { get; set; }
        public DbSet<CentroEducativo> CentroEducativo { get; set; }
        public DbSet<Educacion> Educacion { get; set; }
        public DbSet<EducacionEmpleado> EducacionEmpleado { get; set; }
        public DbSet<Idiomas> Idiomas { get; set; }
        public DbSet<IdiomasEmpleado> IdiomasEmpleado { get; set; }
        public DbSet<Nacionalidad> Nacionalidades { get; set; }
        public DbSet<ProfesionOficio> ProfesionOficio { get; set; }
        public DbSet<FormaPago> FormaPagos { get; set; }
        public DbSet<EvaluacionesDesempeño> EvaluacionesDesempeños { get; set; }
        public DbSet<MiembrosFamilia> MiembrosFamilia { get; set; }
        public DbSet<GrupoFamiliarEmpleado> GrupoFamiliarEmpleado { get; set; }
        public DbSet<ReferenciasPersonales> ReferenciasPersonales { get; set; }
        public DbSet<FamiliaPlazas> FamiliaPlazas { get; set; }
        public DbSet<Documentos> Documentos { get; set; }
        public DbSet<DocumentoEmpleado> DocumentoEmpleado { get; set; }
        public DbSet<Candidato> Candidato { get; set; }
        public DbSet<Prestaciones> Prestaciones { get; set; }
        public DbSet<PrestacionEmpleado> PrestacionEmpleado { get; set; }
        public DbSet<Horarios> Horarios { get; set; }
        public DbSet<MarcasImportadas> MarcasImportadas { get; set; }
        public DbSet<Marcaciones> Marcaciones { get; set; }
        public DbSet<TiempoDescontable> TiempoDescontable { get; set; }
        public DbSet<HorariosEmpleado> HorariosEmpleado { get; set; }
        public DbSet<DiasFeriados> DiasFeriados { get; set; }
        public DbSet<Facilitadores> Facilitadores { get; set; }
        public DbSet<Capacitacion> Capacitacion { get; set; }
        public DbSet<DocumentosCapacitacion> DocumentosCapacitacion { get;set;}
        public DbSet<CapacitacionEmpleado> CapacitacionEmpleado { get; set; }
        public DbSet<TipoEvaluaciones> TipoEvaluaciones { get; set; }
        public DbSet<FasesEvaluaciones> FasesEvaluaciones { get; set; }
        public DbSet<PreguntasFases> PreguntasFases { get; set; }
        public DbSet<Aumentos> Aumentos { get; set; }
        public DbSet<AumentoDetalle> AumentoDetalle { get; set; }
        public DbSet<Descuentos> Descuentos { get; set; }
        public DbSet<DescuentosDetalle> DescuentosDetalle { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<EmpleadoPlaza>()
            //    .HasKey(x => new {x.EmpleadoId, x.CargoId});
            //base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<EmpleadoPlaza>()
                .HasOne(ep => ep.Empleado)
                .WithMany(e => e.EmpleadoPlaza)
                .HasForeignKey(ep => ep.EmpleadoId)
                .OnDelete(DeleteBehavior.NoAction);

            //modelBuilder.Entity<EmpleadoPlaza>()
            //    .HasOne(ep => ep.Cargo)
            //    .WithMany(p => p.EmpleadoCargos)
            //    .HasForeignKey(ep => ep.CargoId)
            //    .OnDelete(DeleteBehavior.NoAction);

            //modelBuilder.Entity<EmpleadoPlaza>()
            //    .HasOne(ep => ep.Plaza)
            //    .WithMany(p => p.EmpleadoPlazas)
            //    .HasForeignKey(ep => ep.PlazaId)
            //    .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Plaza>()
                .HasOne(p => p.DepartamentoSiget)
                .WithMany(sd => sd.Plazas)
                .HasForeignKey(p => p.DepartamentoSigetId)
                .OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<Plaza>()
            //    .HasOne(p => p.Cargo)
            //    .WithMany(sd => sd.Plazas)
            //    .HasForeignKey(p => p.CargoId)
            //    .OnDelete(DeleteBehavior.Restrict);
        }
    }

}
