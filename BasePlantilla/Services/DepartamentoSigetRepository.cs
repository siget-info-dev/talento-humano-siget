﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public class DepartamentoSigetRepository : IDepartamentoSigetRepository
    {
        private readonly TalentoHumanoDbContext _context;

        public DepartamentoSigetRepository(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        public IEnumerable<DepartamentoSiget> GetAll()
        {
            return _context.DepartamentosSiget
                .Include(x => x.Gerencia)
                .ToList();
        }

        public DepartamentoSiget Get(int id)
        {
            return _context.DepartamentosSiget
                .Include(x => x.Gerencia)
                .FirstOrDefault(x => x.Id == id);
        }

        public void Create(DepartamentoSiget DepartamentoSiget)
        {
            _context.DepartamentosSiget.Add(DepartamentoSiget);
        }

        public void Update(DepartamentoSiget DepartamentoSiget)
        {
            _context.DepartamentosSiget.Update(DepartamentoSiget);
        }

        public void Delete(DepartamentoSiget DepartamentoSiget)
        {
            _context.DepartamentosSiget.Remove(DepartamentoSiget);
        }

        public bool Save()
        {
            var result = _context.SaveChanges() >= 0;
            return result;
        }
    }
}
