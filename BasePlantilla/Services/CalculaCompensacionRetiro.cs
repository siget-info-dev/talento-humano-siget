﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public class CalculaCompensacionRetiro
    {
        //Todos los calculos de para la salida de un empleado
        //se tiene que tomar en cuenta 3 escenarios: muerte, despido y renuncia
        //Muerte: se les paga los dias del mes mas vacaciones(si aplicacan) hasta la fecha de muerte y los gastos de representacion si aplica
        //Despido: Se paga vacaciones mas salario hasta la fecha de salida y los gastos de representacion si aplica
        //Renuncia: se les paga todas las prestaciones proporcionales (bono, vacaciones, aguinaldo, gastos de representacion si aplica),
        //              mas un salario por año hasta el parametro  

        //PASOS:
        //1. Calcular tiempo de trabajo ( resta de fecha de ingreso y fecha de salida ), ""años"" y ""dias"" 
        //2. Se lee el parametro maximo de años de compensacion, si la antiguedad es menor que el maximo de años se hace los calculos en base a antiguedad
        //      caso contrario se hace por el maximo y se asigna . 
        //3. se hace el calculo del salario por dia: salarioDiario = salario/365
        //4. Se hace el calculo de la compensacion: (años * salario) + (salarioDiario * dias)
        //5. Se hace el calculo de la vacaciones (20 dias): dias * (salarioDiario*(20/30)) * parametro de periodos de compensacion de vacaciones
        //6. Se hace el calculo del aguinaldo: diasAguinaldo * salarioDiario
        //7. Se hace el calculo del bono: DiasBono * salarioDiario
        //8. Se hace la suma de todos los valores de compensacion.
        //9. Se verifica que no esta jubilado y se hace el calculo de AFP, caso contratrio ????????????????????????
        //10. Se calcula Renta: (Vacaciones + Bono + Aguinaldo) - AFP


        private readonly TalentoHumanoDbContext _context;

        public CalculaCompensacionRetiro(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        //Hace el calculo de los años que ha trabajado un empleado.
        public CompensacionRetiro CalculaAntiguedad(Empleado empleado, DateTime FechaRetiro)
        {
            // Declaracion de Variables
            var diasVacaciones = 0; 
            int diasCompensacion = 0 , diasAguinaldo = 0, diasBono = 0;
            bool CompensacionMaxima = false;
            Decimal BonoAntiguedadAnnos, BonoAntiguedadDias, BonoVacacion, BonoProporcional, BonoAguinaldo, Salario, TotalCompensacion;

            // Calculamos antiguedad en años y dias
            int AnnosAntiguedad = Convert.ToInt32(Math.Truncate((FechaRetiro.Subtract(empleado.FechaIngreso).TotalDays) / 365));
            DateTime FechaInicialCompensacion = empleado.FechaIngreso.AddYears(AnnosAntiguedad);
            int DiasAntiguedad = Convert.ToInt32(FechaRetiro.Subtract(FechaInicialCompensacion).TotalDays) + 1;

            // Verificamos el parametro de maximo de años a calcular.
            var maximoTiempoCompensa = Convert.ToInt32(_context.Parametros.Where(x => x.Nombre == "MaxTiempoCompensacion").Select(x => x.Valor));

            // Si dias es igual o mayor a 365 le sumamos 1 año y dias = 0.
            if (DiasAntiguedad >= 365)
            {
                AnnosAntiguedad++;
                DiasAntiguedad = 0;
            }

            // Si los años de antiguedad es mayor al parametro se hacen los calculos en base al parametro.
            if (AnnosAntiguedad < maximoTiempoCompensa)
            {
                AnnosAntiguedad = maximoTiempoCompensa;
                diasVacaciones = DiasAntiguedad;
                diasCompensacion = 0;
                CompensacionMaxima = true;
            }

            // calculo de vacaciones 
            var mesIngreso = empleado.FechaIngreso.Month;
            var mesSalida = FechaRetiro.Month;

            if (mesIngreso == mesSalida)
            {
                if (CompensacionMaxima)
                {
                    diasCompensacion = DiasAntiguedad;                    
                }
                diasVacaciones = 365 + DiasAntiguedad;
            }
            else {
                if (!CompensacionMaxima) {
                    diasCompensacion = DiasAntiguedad;
                }
                diasVacaciones = DiasAntiguedad + 1;
            }

            //Calculamos Aguinaldo 
            DateTime fechaAguinaldo = Convert.ToDateTime( _context.Parametros.Where(x => x.Nombre == "AguinaldoFecha").Select(x => x.Valor) + "/" + DateTime.Now.Year);

            if (fechaAguinaldo >= FechaRetiro) fechaAguinaldo = fechaAguinaldo.AddDays(-1);
            if (fechaAguinaldo < empleado.FechaIngreso) fechaAguinaldo = empleado.FechaIngreso;

            //Calculo de bono
            DateTime fechaBono = Convert.ToDateTime(_context.Parametros.Where(x => x.Nombre == "BonoFecha").Select(x => x.Valor) + "/" + DateTime.Now.Year);

            if (fechaBono >= FechaRetiro) fechaBono = fechaBono.AddDays(-1);
            if (fechaBono < empleado.FechaIngreso) fechaBono = empleado.FechaIngreso;

            // Se hace el calculo de todo en base a los dias calculados y el salario del empleado.

            diasAguinaldo = Convert.ToInt32(FechaRetiro.Subtract(fechaAguinaldo));

            diasBono = Convert.ToInt32(FechaRetiro.Subtract(fechaBono)) + 1;

            Salario = (empleado.EmpleadoPlaza.Where(x => x.FechaFinal == null).FirstOrDefault()).Salario;
            
            BonoAntiguedadAnnos = AnnosAntiguedad * Salario;
            BonoAntiguedadDias = DiasAntiguedad * (Salario / 365);
            BonoVacacion = diasVacaciones * ((Salario / 365) * (20 / 30)) * 
                           Convert.ToInt32(_context.Parametros.Where(x => x.Nombre == "PeriodosCompensacionVacacion").Select(x => x.Valor));
            BonoProporcional = diasBono + (Salario / 365);
            BonoAguinaldo = diasAguinaldo * (Salario / 365);

            TotalCompensacion = BonoAntiguedadAnnos + BonoAntiguedadDias + BonoVacacion + BonoProporcional + BonoAguinaldo;

            // Calculo de retenciones


            //esta parte de se tiene que deterner en el desarrollo por que el proceso hace consulta de la parte de los montos pagados en planilla por sistema previsional
            //esto para personal jubilado o no .

            // es jubilado 
            if (empleado.Jubilado)
            {
            }
            else { 
                
            }



            var algo = new CompensacionRetiro();
            return algo;
        }
    }
}
