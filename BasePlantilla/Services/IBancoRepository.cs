﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public interface IBancoRepository
    {
        IEnumerable<Banco> GetAll();
        Banco Get(int id);
        void Create(Banco banco);
        void Update(Banco banco);
        void Delete(Banco banco);
        bool Save();
    }
}
