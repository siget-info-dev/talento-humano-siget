﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public interface IFondoPensionRepository
    {
        IEnumerable<FondoPension> GetAll();
        FondoPension Get(int id);
        void Create(FondoPension fondoPension);
        void Update(FondoPension fondoPension);
        void Delete(FondoPension fondoPension);
        bool Save();
    }
}
