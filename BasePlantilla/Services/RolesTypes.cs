﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Services
{
    public class RolesTypes
    {
        public const string Administrador = "Administrador";
        public const string Auxiliar = "Auxiliar";
        public const string Jefe = "Jefe";
        public const string Usuario = "Usuario";
    }
}
