﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Services
{
    public class Graph
    {
        private static GraphServiceClient _graphClient;

        public readonly static string[] Scopes =
        {
            "User.Read"
        };

        public static void Initialize(GraphServiceClient graphClient)
        {
            _graphClient = graphClient;
        }

        //Datos del usuario firmado
        //graph.microsoft.com/v1.0/me/
        [Authorize]
        public static async Task<User> ConsultaUsuario()
        {

            return await _graphClient.Me.Request().GetAsync();
        }
    }
}
