﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public interface IEmpleadoRepository
    {
        IEnumerable<Empleado> GetAll();
        Empleado Get(int id);
        void Create(Empleado empleado);
        void Update(Empleado empleado);
        void Delete(Empleado empleado);
        bool Save();
    }
}
