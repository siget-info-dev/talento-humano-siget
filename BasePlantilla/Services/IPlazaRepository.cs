﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public interface IPlazaRepository
    {
        IEnumerable<Plaza> GetAll();
        Plaza Get(int id);
        void Create(Plaza plaza);
        void Update(Plaza plaza);
        void Delete(Plaza plaza);
        bool Save();
    }
}
