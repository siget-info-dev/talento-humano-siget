﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Net.Mail;


namespace SistemaGestionTalentoHumano.Services
{
    public class EnviaMail
    {
        public static void EnviaCorreo(string destino, string cuerpo, string mail, string mailPassword)
        {
            var mailHost = "smtp.office365.com";
            var mailPort = 587;

            MailMessage msg = new MailMessage();
            msg.To.Add(new MailAddress(destino));
            msg.From = new MailAddress(mail, "RRHH SIGET");
            msg.Subject = "Sistema de RRHH de la SIGET";
            msg.Body = cuerpo;
            msg.IsBodyHtml = true;
            SmtpClient client = new SmtpClient(mail, mailPort);

            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(mail, mailPassword);
            client.Port = mailPort;

            client.Host = mailHost;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;

            client.Send(msg);

            client.Dispose();
            msg.Dispose();
        }
    }
}
