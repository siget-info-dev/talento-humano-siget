﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public interface IGerenciaRepository
    {
        IEnumerable<Gerencias> GetAll();
        Gerencias Get(int id);
        void Create(Gerencias gerencia);
        void Update(Gerencias gerencia);
        void Delete(Gerencias gerencia);
        bool Save();
    }
}
