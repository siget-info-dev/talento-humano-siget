﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    interface IEmpleadoPlazaRepository
    {
        IEnumerable<EmpleadoPlaza> GetPlazasEmpleado(int empleadoId);
        EmpleadoPlaza GetPlaza(int id);
        void Create(EmpleadoPlaza empleadoPlaza);
        void Update(EmpleadoPlaza empleado);
        void Delete(EmpleadoPlaza empleado);
        bool Save();
    }
}
