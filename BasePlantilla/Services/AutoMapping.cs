﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.ViewModels;

namespace SistemaGestionTalentoHumano.Services
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Empleado, EmpleadoViewModel>().ReverseMap();
        }
    }
}
