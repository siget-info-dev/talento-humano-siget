﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Data;


// 13/05/2021 - BO
// Hace la comparacion de las marcas de lo empleados y las compara con los horarios, vacaciones, permisos, diasfestivos,
// y de existir alguna discrepancia, le coloca 1 en el campo revisar

//Pasos:
//1. Se obtiene el listado de los empleados activo = 1 y marcacion = 1, este es listado de los empleados que se les va controlar asistencia.
//2. Se obtiene listado de marcas de la tabla Marcaciones, por empleado en base al listado de paso 1, por fechas 
//3. Se verifica que el empleado tenga horario especial, caso contrario se aplica horario pordefecto = 1 y 
//   se comprara con las marcas (datos paso 2) si todo esta bien se le coloca revisar = 0
//4. De existir diferencia se hace la comparacion en la tabla de permisos, si las diferencia persisten se ingresa registro en 	  
//   tiempoDescontable (existen marcas para ese dia y falta tiempo de trabajo) y se coloca revisar = 1
//5. Si hace falta marca para el dia, se verifica en la tablas de diasFestivos y Vacaciones (busqueda por empleado), de existir diferencia se inserta tiempoDescontable.

namespace SistemaGestionTalentoHumano.Services
{
    public class ProcesaMarcasold
    {
        private readonly TalentoHumanoDbContext _context;

        public  ProcesaMarcasold(TalentoHumanoDbContext context) {
            _context = context;
        }
        
        // se genera el listado de dias a procesar.
        private IEnumerable<DateTime> diasEntreFechasold(DateTime desde, DateTime hasta) { for (var day = desde.Date; day.Date <= hasta.Date; day = day.AddDays(1)) yield return day; }

        public void procesaMarcasold(int? idEmpleado, DateTime fechaIncial, DateTime fechaFinal)
        {

            IQueryable<Empleado> ListadoEmpleados;

            //Paso 1
            if (idEmpleado == null)
            {
                ListadoEmpleados = _context.Empleado.Where(x => x.Activo == true).Where(x => x.Marcacion == true);
            }
            else
            {
                ListadoEmpleados = _context.Empleado.Where(x => x.Activo == true).Where(x => x.Marcacion == true).Where(x => x.CodigoEmpleado == idEmpleado);
            }

            foreach (var empleado in ListadoEmpleados)
            {
                //Paso 2
                IQueryable<Marcaciones> marcaciones = _context.Marcaciones.Where(x => x.Fecha <= fechaIncial).Where(x => x.Fecha >= fechaFinal)
                                                    .Where(x => x.CodigoEmpleado == empleado.CodigoEmpleado).Where(x => x.Revisar == 0);

                //Paso 3
                //Horario por defecto.
                Horarios horarioDefecto = _context.Horarios.Where(x => x.PorDefecto == true).FirstOrDefault();

                //Paso 4
                //Permisos
                //IQueryable<Permisos> permisos = _context.Permisos.Where(x => x.EmpleadoId == empleado.CodigoEmpleado);

                //paso 5
                //Vacaciones
                //IQueryable<Vacaciones> vacaciones = _context.Vacaciones.Where(x => x.EmpleadoId == empleado.CodigoEmpleado)
                //.Where(x => x.Inicio <= fechaIncial).Where(x => x.Fin >= fechaFinal);

                //Dias festivos
                //IQueryable<DiasFeriados> diasFestivos = _context.DiasFeriados.Where(x => x.Fecha <= fechaIncial).Where(x => x.Fecha >= fechaFinal);

                foreach (DateTime dia in diasEntreFechasold(fechaIncial, fechaFinal))
                {
                    //Si marcacionDeDia es nula, no existe marcacion para ese dia, se verifica que no sea feriado, tenda permiso de dia completo, o este de vaciones.
                    var marcacionDeDia = marcaciones.Where(x => x.Fecha == dia).FirstOrDefault();
                    TimeSpan horaEntrada, horaSalida;

                    if (marcacionDeDia == null)
                    {
                        //Dias festivos
                        var feriado = _context.DiasFeriados.Where(x => x.Fecha == dia).FirstOrDefault();
                        if (feriado != null)
                        {
                            //Si es diferente de nulo, la fecha evaluada es dia feriado.
                            continue;
                        }
                        //Vacaciones
                        var vacacion = from v in _context.Vacaciones where dia >= v.Inicio && dia <= v.Fin && v.EmpleadoId == empleado.CodigoEmpleado select v;
                        if (vacacion != null)
                        {
                            //Si es diferente de nulo, la fecha evaluada es vacacion para el empleado.
                            continue;
                        }
                        //Permiso
                        //Se hace la comparacion

                        var permiso = (from p in _context.Permisos
                                       where dia >= p.Inicio && dia <= p.Fin
                                             && p.EmpleadoId == empleado.CodigoEmpleado
                                             && p.AplicaDescuento == false
                                       select p).FirstOrDefault();

                        // hora de entrada debe de ser igual o menor a cero 
                        horaEntrada = horarioDefecto.Inicio.Subtract(permiso.Inicio);

                        // hora de salida debe de ser igual o mayor a cero 
                        horaSalida = horarioDefecto.Final.Subtract(permiso.Fin);

                        //Se hace la 
                        if (permiso != null && horaEntrada.TotalMinutes <= 0 && horaSalida.TotalMinutes >= 0)
                        {
                            //Si es diferente de nulo, la fecha evaluada tiene permiso para el empleado.
                            continue;
                        }

                        //No se encontro justificacion para que el empleado no marcara, se ingresa registro de descuento.
                        aplicaDescuentoold(dia, empleado.CodigoEmpleado, 8 * 60);
                    }

                    //Horarios especiales
                    //se hace la busqueda de horarios especiales, que este vigentes y sean el empleado.
                    var HorarioEspecial = from h in _context.Horarios
                                          where (from he in _context.HorariosEmpleado
                                                 where he.EmpleadoId == empleado.CodigoEmpleado
                                                 select he.HorariosId).Contains(h.Id)
                                          && dia >= h.Inicio && dia <= h.Final
                                          select h;

                    //ver las marcaciones si son en horario(normales) 
                    //Recuperamos las marcas de del evaluado.
                    horaEntrada = marcacionDeDia.Entrada.TimeOfDay.Subtract(horarioDefecto.Inicio.TimeOfDay);

                    //ver las marcaciones de entrada tardia, comparacion de marcas contra horario pordefecto=1

                    //se hace la resta de los tiempos en minutos, si los minutos son mayores que 0 se entiende entrada tardia, 
                    //caso contrario es entrada antes de tiempo(temprano).
                    if (horaEntrada.TotalMinutes > 0)
                    {
                        //revisamos permiso de llegada tarde.
                        var permisoLlegadaTarde = _context.Permisos.Where(x => x.EmpleadoId == empleado.CodigoEmpleado)
                                                    .Where(x => x.Inicio.Date.DayOfYear == dia.DayOfYear).FirstOrDefault();

                        var horarioLlegadaTarde = HorarioEspecial.Where(x => x.DiaSemana == (int)dia.DayOfWeek).FirstOrDefault().Inicio;

                        //sin permiso de llegada tardia.
                        if (permisoLlegadaTarde == null || horarioLlegadaTarde == null)
                        {
                            aplicaDescuentoold(dia, empleado.CodigoEmpleado, Convert.ToInt32(horaEntrada.TotalMinutes));
                            continue;
                        }

                        TimeSpan horaDePermisoEntrada = marcacionDeDia.Entrada.TimeOfDay.Subtract(permisoLlegadaTarde.Inicio.TimeOfDay);

                        TimeSpan horaDeHorarioEntrada = marcacionDeDia.Entrada.TimeOfDay.Subtract(horarioLlegadaTarde.TimeOfDay);

                        if (horaDePermisoEntrada.TotalMinutes > 0 || horaDeHorarioEntrada.TotalMinutes > 0)
                        {
                            aplicaDescuentoold(dia, empleado.CodigoEmpleado, Convert.ToInt32(horaDePermisoEntrada.TotalMinutes));
                            continue;
                        }
                    }

                    //ver las marcaciones de salida temprano, si el tiempo es mayor qur 0 se entiende como salida temprana.
                    horaSalida = horarioDefecto.Final.TimeOfDay.Subtract(marcacionDeDia.Salida.TimeOfDay);
                    if (horaSalida.TotalMinutes > 0)
                    {
                        //revisamos permiso de salida temprano.
                        var permisoSalidaTemprana = _context.Permisos.Where(x => x.EmpleadoId == empleado.CodigoEmpleado)
                                                      .Where(x => x.Fin.Date.DayOfYear == dia.DayOfYear).FirstOrDefault();

                        var horarioSalidaTemprano = HorarioEspecial.Where(x => x.DiaSemana == (int)dia.DayOfWeek).FirstOrDefault().Final;

                        //sin permiso de salida temprana.
                        if (permisoSalidaTemprana == null || horarioSalidaTemprano == null)
                        {
                            aplicaDescuentoold(dia, empleado.CodigoEmpleado, Convert.ToInt32(horaSalida.TotalMinutes));
                            continue;
                        }

                        TimeSpan horaDePermisoSalida = permisoSalidaTemprana.Fin.TimeOfDay.Subtract(marcacionDeDia.Salida.TimeOfDay);

                        TimeSpan horaDeHorarioSalida = horarioSalidaTemprano.TimeOfDay.Subtract(marcacionDeDia.Salida.TimeOfDay);

                        if (horaDePermisoSalida.TotalMinutes > 0 || horaDeHorarioSalida.TotalMinutes > 0)
                        {
                            aplicaDescuentoold(dia, empleado.CodigoEmpleado, Convert.ToInt32(horaSalida.TotalMinutes));
                            continue;
                        }
                    }
                }
            }
        }

        private void aplicaDescuentoold(DateTime Fecha, int CodigoEmpleado, int TiempoDescuento) {
            _context.TiempoDescontable.Add(new TiempoDescontable { Fecha = Fecha, CodigoEmpleado = CodigoEmpleado, MinutosADescontar = TiempoDescuento });
            _context.SaveChanges();
        }
    }
}
