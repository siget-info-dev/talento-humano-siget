﻿using SistemaGestionTalentoHumano.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Data;

namespace SistemaGestionTalentoHumano.Services
{
    public class FondoPensionRepository : IFondoPensionRepository
    {
        private readonly TalentoHumanoDbContext _context;

        public FondoPensionRepository(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        public void Create(FondoPension fondoPension)
        {
            _context.FondosPension.Add(fondoPension);
        }

        public void Delete(FondoPension fondoPension)
        {
            _context.FondosPension.Remove(fondoPension);
        }

        public FondoPension Get(int id)
        {
            return _context.FondosPension.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<FondoPension> GetAll()
        {
            return _context.FondosPension.ToList();
        }

        public bool Save()
        {
            var result = _context.SaveChanges() >= 0;
            return result;
        }

        public void Update(FondoPension fondoPension)
        {
            _context.FondosPension.Update(fondoPension);
        }
    }
}
