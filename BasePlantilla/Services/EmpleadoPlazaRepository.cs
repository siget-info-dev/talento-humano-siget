﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public class EmpleadoPlazaRepository : IEmpleadoPlazaRepository
    {
        private readonly TalentoHumanoDbContext _context;

        public EmpleadoPlazaRepository(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        public IEnumerable<EmpleadoPlaza> GetPlazasEmpleado(int empleadoId)
        {
            var plazasEmpleado = _context
                .EmpleadoPlaza
                //.Include(e => e.Cargo)
                .Include(e => e.Empleado)
                .Include(e => e.Plaza)
                .Where(e => e.EmpleadoId == empleadoId);
            return plazasEmpleado;
        }

        public EmpleadoPlaza GetPlaza(int id)
        {
            throw new NotImplementedException();
        }

        public void Create(EmpleadoPlaza empleadoPlaza)
        {
            // Lógica para crear nueva plaza Empleado
            var empleadoPlazaAnterior = _context.EmpleadoPlaza
                .FirstOrDefault(x => x.EmpleadoId == empleadoPlaza.EmpleadoId && x.Plaza.PlazaActiva == true);
            
            if (empleadoPlazaAnterior != null)
            {
                empleadoPlazaAnterior.Plaza.PlazaActiva = false;
                empleadoPlazaAnterior.FechaFinal = DateTime.Now;
            }

            empleadoPlaza.Plaza.PlazaActiva = true;
            _context.Add(empleadoPlaza);
        }

        public void Update(EmpleadoPlaza empleado)
        {
            throw new NotImplementedException();
        }

        public void Delete(EmpleadoPlaza empleado)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            var result = _context.SaveChanges() >= 0;
            return result;
        }
    }
}
