﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public class BancoRepository : IBancoRepository
    {
        private readonly TalentoHumanoDbContext _context;

        public BancoRepository(TalentoHumanoDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Banco> GetAll()
        {
            return _context.Bancos.ToList();
        }

        public Banco Get(int id)
        {
            return _context.Bancos.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Banco banco)
        {
            _context.Bancos.Add(banco);
        }

        public void Update(Banco banco)
        {
            _context.Bancos.Update(banco);
        }

        public void Delete(Banco banco)
        {
            _context.Bancos.Remove(banco);
        }

        public bool Save()
        {
            var result = _context.SaveChanges() >= 0;
            return result;
        }
    }
}
