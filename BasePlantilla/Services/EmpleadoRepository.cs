﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public class EmpleadoRepository : IEmpleadoRepository
    {
        private readonly TalentoHumanoDbContext _context;

        public EmpleadoRepository(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Empleado> GetAll()
        {
            var empleados = _context.Empleado.Include(x => x.EmpleadoPlaza).ThenInclude(y=> y.Plaza).ToList();

            ////Se coloca esta consulta, asi incluir empleadosplazas.plazas en la respuesta.
            //foreach (var Empleado in empleados) {
            //    foreach (var empleadosplaza in Empleado.EmpleadoPlazas) {
            //        empleadosplaza.Plaza = _context.Plazas.Where(x => x.Id == empleadosplaza.PlazaId).FirstOrDefault();
            //    }
            //}
                
            return empleados;
        }

        public Empleado Get(int id)
        {
            var empleado = _context.Empleado
                .Include(e => e.Banco)
                .Include(e => e.Sexo)
                .Include(e => e.EstadoCivil)
                .Include(e => e.FondoPension)
                .Include(e => e.FormaPago)
                .Include(e => e.TipoCuenta)
                .Include(e => e.TipoSangre)
                .Include(e => e.ReferenciasPersonales)
                .Include(e => e.EducacionEmpleado)
                    .ThenInclude(x => x.Educacion)
                 .Include(e => e.EducacionEmpleado)
                    .ThenInclude(x => x.CentroEducativo)
                .Include(e => e.GrupoFamiliarEmpleado)
                    .ThenInclude(x => x.MiembrosFamilia)
                .Include(e => e.EmpleadoPlaza)
                    .ThenInclude(x => x.Plaza)
                .Include(e => e.IdiomasEmpleado)
                    .ThenInclude(x => x.Idioma)
                .Include(e => e.Municipio)
                    .ThenInclude(x => x.Departamento)
                .FirstOrDefault(e => e.Id == id);

            //foreach (var empleadosplaza in Empleado.EmpleadoPlazas) {
            //    empleadosplaza.Plaza = _context.Plazas.Where(x => x.Id == empleadosplaza.PlazaId).FirstOrDefault();
            //}


            return empleado;
        }

        public void Create(Empleado empleado)
        {
            _context.Empleado.Add(empleado);
        }

        public void Update(Empleado empleado)
        {
            _context.Empleado.Update(empleado);
        }

        public void Delete(Empleado empleado)
        {
            _context.Empleado.Remove(empleado);
        }

        public bool Save()
        {
            var result = _context.SaveChanges() >= 0;
            return result;
        }
    }
}
