﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace SistemaGestionTalentoHumano.Services
{
    public class Tools
    {
        public static string GeneraCodigo()
        {
            Guid miGuid = Guid.NewGuid();
            return miGuid.ToString().Replace("-", string.Empty);
        }

        public static string subeArchivo(IFormFile archivo)
        {
            string uploadsFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Documentos");
            string NombreArchivo = GeneraCodigo() + Path.GetExtension(archivo.FileName);
            string filePath = Path.Combine(uploadsFolder, NombreArchivo);
            archivo.CopyTo(new FileStream(filePath, FileMode.Create));
            return NombreArchivo;
        }

        public static string subeFotoEmpleado(int idEmpleado, IFormFile archivo)
        {
            string uploadsFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\FotosEmpleados");
            string NombreArchivo = idEmpleado + Path.GetExtension(archivo.FileName);
            string filePath = Path.Combine(uploadsFolder, NombreArchivo);
            archivo.CopyTo(new FileStream(filePath, FileMode.Create));
            return NombreArchivo;
        }


        public static string RemplazaArchivo(string archivoARemplazar, IFormFile archivo) {
            string uploadsFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Documentos");
            
            //se elimina archivo viejo.
            File.Delete(Path.Combine(uploadsFolder, archivoARemplazar));

            string NombreArchivo = GeneraCodigo() + Path.GetExtension(archivo.FileName);
            string filePath = Path.Combine(uploadsFolder, NombreArchivo);
            archivo.CopyTo(new FileStream(filePath, FileMode.Create));
            return NombreArchivo;
        }

        public static string RemplazaFotoEmpleado(int idEmpleado, string ArchivoARemplazar, IFormFile archivo)
        {
            string uploadsFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\FotosEmpleados");
            File.Delete(Path.Combine(uploadsFolder, ArchivoARemplazar));

            string NombreArchivo = idEmpleado + Path.GetExtension(archivo.FileName);
            string filePath = Path.Combine(uploadsFolder, NombreArchivo);
            archivo.CopyTo(new FileStream(filePath, FileMode.Create));
            return NombreArchivo;
        }

        public static List<Dias> DiasSemana()
        {
            List<Dias> dia = new List<Dias>() {
                new Dias() { Id = 0, Dia = "Domingo" },
                new Dias() { Id = 1, Dia = "Lunes" },
                new Dias() { Id = 2, Dia = "Martes" },
                new Dias() { Id = 3, Dia = "Miércoles" },
                new Dias() { Id = 4, Dia = "Jueves" },
                new Dias() { Id = 5, Dia = "Viernes" },
                new Dias() { Id = 6, Dia = "Sábado" }
            };
            return dia;
        }

        public static string DiaSemanaById(int Id)
        {
            var listaDias = DiasSemana();
            var dia = listaDias.FirstOrDefault(x => x.Id == Id);
            return dia.Dia;
        }
    }

    public class Dias
    {
        public int Id { get; set; }
        public string Dia { get; set; }
    }


}
