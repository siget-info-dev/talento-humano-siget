﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public interface IDepartamentoSigetRepository
    {
        IEnumerable<DepartamentoSiget> GetAll();
        DepartamentoSiget Get(int id);
        void Create(DepartamentoSiget DepartamentoSiget);
        void Update(DepartamentoSiget DepartamentoSiget);
        void Delete(DepartamentoSiget DepartamentoSiget);
        bool Save();
    }
}
