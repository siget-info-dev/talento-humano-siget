﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public class PlazaRepository : IPlazaRepository
    {
        private readonly TalentoHumanoDbContext _context;

        public PlazaRepository(TalentoHumanoDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Plaza> GetAll()
        {
            return _context.Plazas
                .Include(x => x.DepartamentoSiget)
                //.Include(x => x.Cargo)
                .ToList();
        }

        public Plaza Get(int id)
        {
            return _context.Plazas
                .Include(x => x.DepartamentoSiget)
                .Include(x => x.FamiliaPlazas)
                .Include(x=> x.DepartamentoSiget.Gerencia)
                .FirstOrDefault(x => x.Id == id);
        }

        public void Create(Plaza plaza)
        {
            _context.Plazas.Add(plaza);
        }

        public void Update(Plaza plaza)
        {
            _context.Plazas.Update(plaza);
        }

        public void Delete(Plaza plaza)
        {
            _context.Plazas.Remove(plaza);
        }

        public bool Save()
        {
            var result = _context.SaveChanges() >= 0;
            return result;
        }
    }
}
