﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Services
{
    public class GerenciaRepository : IGerenciaRepository
    {
        private readonly TalentoHumanoDbContext _context;

        public GerenciaRepository(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Gerencias> GetAll()
        {
            return _context.Gerencias.ToList();
        }

        public Gerencias Get(int id)
        {
            return _context.Gerencias
                .Include(x => x.DepartamentosSiget)
                .FirstOrDefault(x => x.Id == id);
        }

        public void Create(Gerencias departamentoSiget)
        {
            _context.Gerencias.Add(departamentoSiget);
        }

        public void Update(Gerencias departamentoSiget)
        {
            _context.Gerencias.Update(departamentoSiget);
        }

        public void Delete(Gerencias departamentoSiget)
        {
            _context.Gerencias.Remove(departamentoSiget);
        }

        public bool Save()
        {
            var result = _context.SaveChanges() >= 0;
            return result;
        }
    }
}
