﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaGestionTalentoHumano.Migrations
{
    public partial class inicial3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AumentoDetalle_Aumentos_AumentosId",
                table: "AumentoDetalle");

            migrationBuilder.DropColumn(
                name: "Valor",
                table: "DescuentosDetalle");

            migrationBuilder.DropColumn(
                name: "AumentoId",
                table: "AumentoDetalle");

            migrationBuilder.AddColumn<bool>(
                name: "DescuentoAplicado",
                table: "Descuentos",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<float>(
                name: "Valor",
                table: "Descuentos",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AlterColumn<int>(
                name: "AumentosId",
                table: "AumentoDetalle",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AumentoDetalle_Aumentos_AumentosId",
                table: "AumentoDetalle",
                column: "AumentosId",
                principalTable: "Aumentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AumentoDetalle_Aumentos_AumentosId",
                table: "AumentoDetalle");

            migrationBuilder.DropColumn(
                name: "DescuentoAplicado",
                table: "Descuentos");

            migrationBuilder.DropColumn(
                name: "Valor",
                table: "Descuentos");

            migrationBuilder.AddColumn<float>(
                name: "Valor",
                table: "DescuentosDetalle",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AlterColumn<int>(
                name: "AumentosId",
                table: "AumentoDetalle",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AumentoId",
                table: "AumentoDetalle",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_AumentoDetalle_Aumentos_AumentosId",
                table: "AumentoDetalle",
                column: "AumentosId",
                principalTable: "Aumentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
