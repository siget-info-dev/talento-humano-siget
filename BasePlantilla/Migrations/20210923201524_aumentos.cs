﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaGestionTalentoHumano.Migrations
{
    public partial class aumentos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "PorcentajeAumento",
                table: "Aumentos",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PorcentajeAumento",
                table: "Aumentos");
        }
    }
}
