﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaGestionTalentoHumano.Migrations
{
    public partial class plazaEmpleados : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Observacion",
                table: "EmpleadoPlaza",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Observacion",
                table: "EmpleadoPlaza");
        }
    }
}
