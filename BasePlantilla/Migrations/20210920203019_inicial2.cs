﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaGestionTalentoHumano.Migrations
{
    public partial class inicial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Aumentos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aumentos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Descuentos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Descuentos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AumentoDetalle",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fecha = table.Column<DateTime>(nullable: false),
                    Valor = table.Column<float>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false),
                    AumentoId = table.Column<int>(nullable: false),
                    AumentosId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AumentoDetalle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AumentoDetalle_Aumentos_AumentosId",
                        column: x => x.AumentosId,
                        principalTable: "Aumentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AumentoDetalle_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DescuentosDetalle",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fecha = table.Column<DateTime>(nullable: false),
                    Valor = table.Column<float>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false),
                    DescuentosId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DescuentosDetalle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DescuentosDetalle_Descuentos_DescuentosId",
                        column: x => x.DescuentosId,
                        principalTable: "Descuentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DescuentosDetalle_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AumentoDetalle_AumentosId",
                table: "AumentoDetalle",
                column: "AumentosId");

            migrationBuilder.CreateIndex(
                name: "IX_AumentoDetalle_EmpleadoId",
                table: "AumentoDetalle",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_DescuentosDetalle_DescuentosId",
                table: "DescuentosDetalle",
                column: "DescuentosId");

            migrationBuilder.CreateIndex(
                name: "IX_DescuentosDetalle_EmpleadoId",
                table: "DescuentosDetalle",
                column: "EmpleadoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AumentoDetalle");

            migrationBuilder.DropTable(
                name: "DescuentosDetalle");

            migrationBuilder.DropTable(
                name: "Aumentos");

            migrationBuilder.DropTable(
                name: "Descuentos");
        }
    }
}
