﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaGestionTalentoHumano.Migrations
{
    public partial class aumentos3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "PorcentajeAumento",
                table: "Aumentos",
                type: "Decimal(8,2)",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "real");

            migrationBuilder.AlterColumn<decimal>(
                name: "Valor",
                table: "AumentoDetalle",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "real");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "PorcentajeAumento",
                table: "Aumentos",
                type: "real",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "Decimal(8,2)");

            migrationBuilder.AlterColumn<float>(
                name: "Valor",
                table: "AumentoDetalle",
                type: "real",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
