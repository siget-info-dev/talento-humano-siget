﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaGestionTalentoHumano.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bancos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bancos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CentroEducativo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CentroEducativo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Departamentos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departamentos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DiasFeriados",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(nullable: true),
                    Fecha = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiasFeriados", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Documentos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documentos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Educacion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Educacion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EstadosCivil",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstadosCivil", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Facilitadores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    Proveedor = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Facilitadores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FondosPension",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FondosPension", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FormaPagos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormaPagos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gerencias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gerencias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Horarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(nullable: true),
                    ToleranciaMaxima = table.Column<string>(nullable: true),
                    DiaSemana = table.Column<int>(nullable: false),
                    Inicio = table.Column<DateTime>(nullable: false),
                    Final = table.Column<DateTime>(nullable: false),
                    PorDefecto = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Horarios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Idiomas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Idioma = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Idiomas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Marcaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CodigoEmpleado = table.Column<int>(nullable: false),
                    Fecha = table.Column<DateTime>(nullable: false),
                    Entrada = table.Column<DateTime>(nullable: false),
                    Salida = table.Column<DateTime>(nullable: false),
                    Revisar = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Marcaciones", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MarcasImportadas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CodigoEmpleado = table.Column<int>(nullable: false),
                    FechaMarcacion = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarcasImportadas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MiembrosFamilia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Miembro = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MiembrosFamilia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Nacionalidades",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nacionalidades", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Parametros",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    Valor = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parametros", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Prestaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    PrestacionGerencial = table.Column<bool>(nullable: false),
                    PrestacionAplicada = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prestaciones", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProfesionOficio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfesionOficio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sexo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sexo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiempoDescontable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fecha = table.Column<DateTime>(nullable: false),
                    MinutosADescontar = table.Column<double>(nullable: false),
                    CodigoEmpleado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiempoDescontable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoAccionPersonal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Acciones = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoAccionPersonal", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoAcuerdos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Acuerdo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoAcuerdos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoEvaluaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoEvaluaciones", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoPermisos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Permiso = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoPermisos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposCuenta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposCuenta", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposSangre",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposSangre", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Municipios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false),
                    DepartamentoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Municipios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Municipios_Departamentos_DepartamentoId",
                        column: x => x.DepartamentoId,
                        principalTable: "Departamentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Capacitacion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    FechaInicio = table.Column<DateTime>(nullable: false),
                    FechaFin = table.Column<DateTime>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true),
                    Estado = table.Column<int>(nullable: false),
                    TipoCapacitacion = table.Column<int>(nullable: false),
                    FacilitadoresId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Capacitacion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Capacitacion_Facilitadores_FacilitadoresId",
                        column: x => x.FacilitadoresId,
                        principalTable: "Facilitadores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DepartamentosSiget",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<string>(maxLength: 4, nullable: false),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    GerenciaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartamentosSiget", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DepartamentosSiget_Gerencias_GerenciaId",
                        column: x => x.GerenciaId,
                        principalTable: "Gerencias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Candidato",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PrimerNombre = table.Column<string>(nullable: true),
                    SegundoNombre = table.Column<string>(nullable: true),
                    PrimerApellido = table.Column<string>(nullable: true),
                    SegundoApellido = table.Column<string>(nullable: true),
                    ApellidoCasada = table.Column<string>(nullable: true),
                    Telefono = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    NombreArchivo = table.Column<string>(nullable: true),
                    ProfesionOficioId = table.Column<int>(nullable: false),
                    EducacionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Candidato", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Candidato_Educacion_EducacionId",
                        column: x => x.EducacionId,
                        principalTable: "Educacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Candidato_ProfesionOficio_ProfesionOficioId",
                        column: x => x.ProfesionOficioId,
                        principalTable: "ProfesionOficio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FamiliaPlazas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    TipoEvaluacionesId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FamiliaPlazas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FamiliaPlazas_TipoEvaluaciones_TipoEvaluacionesId",
                        column: x => x.TipoEvaluacionesId,
                        principalTable: "TipoEvaluaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FasesEvaluaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NombreFase = table.Column<string>(nullable: true),
                    PorcentajeEvaluacion = table.Column<int>(nullable: false),
                    TipoEvaluacionesId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FasesEvaluaciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FasesEvaluaciones_TipoEvaluaciones_TipoEvaluacionesId",
                        column: x => x.TipoEvaluacionesId,
                        principalTable: "TipoEvaluaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Empleado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PrimerNombre = table.Column<string>(maxLength: 50, nullable: false),
                    SegundoNombre = table.Column<string>(maxLength: 50, nullable: true),
                    TercerNombre = table.Column<string>(maxLength: 50, nullable: true),
                    PrimerApellido = table.Column<string>(maxLength: 50, nullable: false),
                    SegundoApellido = table.Column<string>(maxLength: 50, nullable: true),
                    ApellidoCasada = table.Column<string>(maxLength: 50, nullable: true),
                    ConocidoPor = table.Column<string>(maxLength: 100, nullable: true),
                    EmailPersonal = table.Column<string>(maxLength: 50, nullable: true),
                    LugarNacimiento = table.Column<string>(nullable: true),
                    DireccionResidencia = table.Column<string>(nullable: false),
                    CodigoEmpleado = table.Column<int>(nullable: false),
                    NumeroCuenta = table.Column<decimal>(maxLength: 25, nullable: false),
                    Estatura = table.Column<float>(nullable: false),
                    TelefonoFijo = table.Column<string>(maxLength: 10, nullable: true),
                    TelefonoMovil = table.Column<string>(maxLength: 10, nullable: true),
                    RutaImagen = table.Column<string>(nullable: true),
                    Observaciones = table.Column<string>(nullable: true),
                    Dui = table.Column<string>(maxLength: 10, nullable: false),
                    Nit = table.Column<string>(maxLength: 17, nullable: false),
                    Isss = table.Column<string>(maxLength: 9, nullable: true),
                    NumeroFondoPension = table.Column<string>(maxLength: 12, nullable: true),
                    Pasaporte = table.Column<string>(maxLength: 15, nullable: true),
                    FechaIngreso = table.Column<DateTime>(type: "Date", nullable: false),
                    FechaRetiro = table.Column<DateTime>(type: "Date", nullable: true),
                    FechaNacimiento = table.Column<DateTime>(type: "Date", nullable: false),
                    FechaEmisionDui = table.Column<DateTime>(type: "Date", nullable: false),
                    FechaVencimientoDui = table.Column<DateTime>(type: "Date", nullable: false),
                    NombreContactoEmergencia = table.Column<string>(nullable: true),
                    TelefonoEmergencia = table.Column<string>(nullable: true),
                    DireccionEmergencia = table.Column<string>(nullable: true),
                    ObservacionesEmergencia = table.Column<string>(nullable: true),
                    Activo = table.Column<bool>(nullable: false),
                    Jubilado = table.Column<bool>(nullable: false),
                    Marcacion = table.Column<bool>(nullable: false),
                    PoseeLicencia = table.Column<bool>(nullable: false),
                    PrestacionesGerenciales = table.Column<bool>(nullable: false),
                    TipoSangreId = table.Column<int>(nullable: false),
                    NacionalidadId = table.Column<int>(nullable: false),
                    EstadoCivilId = table.Column<int>(nullable: false),
                    SexoId = table.Column<int>(nullable: false),
                    ProfesionOficioId = table.Column<int>(nullable: false),
                    FondoPensionId = table.Column<int>(nullable: false),
                    MunicipioId = table.Column<int>(nullable: false),
                    FormaPagoId = table.Column<int>(nullable: false),
                    BancoId = table.Column<int>(nullable: false),
                    TipoCuentaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empleado", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Empleado_Bancos_BancoId",
                        column: x => x.BancoId,
                        principalTable: "Bancos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_EstadosCivil_EstadoCivilId",
                        column: x => x.EstadoCivilId,
                        principalTable: "EstadosCivil",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_FondosPension_FondoPensionId",
                        column: x => x.FondoPensionId,
                        principalTable: "FondosPension",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_FormaPagos_FormaPagoId",
                        column: x => x.FormaPagoId,
                        principalTable: "FormaPagos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_Municipios_MunicipioId",
                        column: x => x.MunicipioId,
                        principalTable: "Municipios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_Nacionalidades_NacionalidadId",
                        column: x => x.NacionalidadId,
                        principalTable: "Nacionalidades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_ProfesionOficio_ProfesionOficioId",
                        column: x => x.ProfesionOficioId,
                        principalTable: "ProfesionOficio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_Sexo_SexoId",
                        column: x => x.SexoId,
                        principalTable: "Sexo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_TiposCuenta_TipoCuentaId",
                        column: x => x.TipoCuentaId,
                        principalTable: "TiposCuenta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Empleado_TiposSangre_TipoSangreId",
                        column: x => x.TipoSangreId,
                        principalTable: "TiposSangre",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumentosCapacitacion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(nullable: true),
                    NombreArchivo = table.Column<string>(nullable: true),
                    CapacitacionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentosCapacitacion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentosCapacitacion_Capacitacion_CapacitacionId",
                        column: x => x.CapacitacionId,
                        principalTable: "Capacitacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plazas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 240, nullable: false),
                    NombreCorto = table.Column<string>(maxLength: 60, nullable: true),
                    CodigoPlaza = table.Column<string>(nullable: true),
                    PlazaActiva = table.Column<bool>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    Acuerdo = table.Column<string>(nullable: true),
                    FamiliaPlazasId = table.Column<int>(nullable: false),
                    DepartamentoSigetId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plazas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Plazas_DepartamentosSiget_DepartamentoSigetId",
                        column: x => x.DepartamentoSigetId,
                        principalTable: "DepartamentosSiget",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Plazas_FamiliaPlazas_FamiliaPlazasId",
                        column: x => x.FamiliaPlazasId,
                        principalTable: "FamiliaPlazas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PreguntasFases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Pregunta = table.Column<string>(nullable: true),
                    FasesEvaluacionesId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreguntasFases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PreguntasFases_FasesEvaluaciones_FasesEvaluacionesId",
                        column: x => x.FasesEvaluacionesId,
                        principalTable: "FasesEvaluaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccionesDePersonal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Numero = table.Column<string>(maxLength: 12, nullable: true),
                    Comentarios = table.Column<string>(nullable: true),
                    FechaAccionPersonal = table.Column<DateTime>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false),
                    AccionesId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccionesDePersonal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccionesDePersonal_TipoAccionPersonal_AccionesId",
                        column: x => x.AccionesId,
                        principalTable: "TipoAccionPersonal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccionesDePersonal_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Acuerdos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NumeroAcuerdo = table.Column<string>(nullable: true),
                    FechaAcuerdo = table.Column<DateTime>(nullable: false),
                    Comentarios = table.Column<string>(nullable: true),
                    EmpleadoId = table.Column<int>(nullable: false),
                    AcuerdoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Acuerdos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Acuerdos_TipoAcuerdos_AcuerdoId",
                        column: x => x.AcuerdoId,
                        principalTable: "TipoAcuerdos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Acuerdos_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CapacitacionEmpleado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CapacitacionId = table.Column<int>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CapacitacionEmpleado", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CapacitacionEmpleado_Capacitacion_CapacitacionId",
                        column: x => x.CapacitacionId,
                        principalTable: "Capacitacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CapacitacionEmpleado_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumentoEmpleado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NombreArchivo = table.Column<string>(nullable: true),
                    DocumentoId = table.Column<int>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentoEmpleado", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentoEmpleado_Documentos_DocumentoId",
                        column: x => x.DocumentoId,
                        principalTable: "Documentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentoEmpleado_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EducacionEmpleado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GradoAcademico = table.Column<string>(nullable: true),
                    Año = table.Column<int>(nullable: false),
                    EducacionId = table.Column<int>(nullable: false),
                    ArchivoRespaldo = table.Column<string>(nullable: true),
                    EmpleadoId = table.Column<int>(nullable: false),
                    CentroEducativoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EducacionEmpleado", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EducacionEmpleado_CentroEducativo_CentroEducativoId",
                        column: x => x.CentroEducativoId,
                        principalTable: "CentroEducativo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EducacionEmpleado_Educacion_EducacionId",
                        column: x => x.EducacionId,
                        principalTable: "Educacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EducacionEmpleado_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GrupoFamiliarEmpleado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    FechaNacimiento = table.Column<DateTime>(nullable: false),
                    MiembrosFamiliaId = table.Column<int>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GrupoFamiliarEmpleado", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GrupoFamiliarEmpleado_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GrupoFamiliarEmpleado_MiembrosFamilia_MiembrosFamiliaId",
                        column: x => x.MiembrosFamiliaId,
                        principalTable: "MiembrosFamilia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HorariosEmpleado",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HorariosId = table.Column<int>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HorariosEmpleado", x => x.id);
                    table.ForeignKey(
                        name: "FK_HorariosEmpleado_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HorariosEmpleado_Horarios_HorariosId",
                        column: x => x.HorariosId,
                        principalTable: "Horarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IdiomaEmpleados",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NivelIdioma = table.Column<string>(nullable: true),
                    IdiomaId = table.Column<int>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdiomaEmpleados", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IdiomaEmpleados_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IdiomaEmpleados_Idiomas_IdiomaId",
                        column: x => x.IdiomaId,
                        principalTable: "Idiomas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Permisos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AplicaDescuento = table.Column<bool>(nullable: false),
                    Inicio = table.Column<DateTime>(nullable: false),
                    Fin = table.Column<DateTime>(nullable: false),
                    Comentario = table.Column<string>(nullable: true),
                    TipoPermisoId = table.Column<int>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permisos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Permisos_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Permisos_TipoPermisos_TipoPermisoId",
                        column: x => x.TipoPermisoId,
                        principalTable: "TipoPermisos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PrestacionEmpleado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FechaEntrega = table.Column<DateTime>(nullable: false),
                    PrestacionesId = table.Column<int>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrestacionEmpleado", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrestacionEmpleado_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrestacionEmpleado_Prestaciones_PrestacionesId",
                        column: x => x.PrestacionesId,
                        principalTable: "Prestaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReferenciasPersonales",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    LugarTrabajo = table.Column<string>(nullable: true),
                    Telefono = table.Column<string>(nullable: true),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReferenciasPersonales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReferenciasPersonales_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vacaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Inicio = table.Column<DateTime>(nullable: false),
                    Fin = table.Column<DateTime>(nullable: false),
                    Periodo = table.Column<int>(nullable: false),
                    Comentario = table.Column<string>(nullable: true),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vacaciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vacaciones_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EmpleadoPlaza",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FechaInicio = table.Column<DateTime>(type: "Date", nullable: false),
                    FechaFinal = table.Column<DateTime>(type: "Date", nullable: true),
                    NumeroContrato = table.Column<int>(nullable: true),
                    Salario = table.Column<decimal>(type: "Decimal(8,2)", nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false),
                    PlazaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmpleadoPlaza", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmpleadoPlaza_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EmpleadoPlaza_Plazas_PlazaId",
                        column: x => x.PlazaId,
                        principalTable: "Plazas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EvaluacionesDesempeños",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PeriodoEvaluacion = table.Column<string>(nullable: true),
                    Calificacion = table.Column<int>(nullable: false),
                    PreguntasFasesId = table.Column<int>(nullable: false),
                    EmpleadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvaluacionesDesempeños", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EvaluacionesDesempeños_Empleado_EmpleadoId",
                        column: x => x.EmpleadoId,
                        principalTable: "Empleado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EvaluacionesDesempeños_PreguntasFases_PreguntasFasesId",
                        column: x => x.PreguntasFasesId,
                        principalTable: "PreguntasFases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccionesDePersonal_AccionesId",
                table: "AccionesDePersonal",
                column: "AccionesId");

            migrationBuilder.CreateIndex(
                name: "IX_AccionesDePersonal_EmpleadoId",
                table: "AccionesDePersonal",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Acuerdos_AcuerdoId",
                table: "Acuerdos",
                column: "AcuerdoId");

            migrationBuilder.CreateIndex(
                name: "IX_Acuerdos_EmpleadoId",
                table: "Acuerdos",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Candidato_EducacionId",
                table: "Candidato",
                column: "EducacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Candidato_ProfesionOficioId",
                table: "Candidato",
                column: "ProfesionOficioId");

            migrationBuilder.CreateIndex(
                name: "IX_Capacitacion_FacilitadoresId",
                table: "Capacitacion",
                column: "FacilitadoresId");

            migrationBuilder.CreateIndex(
                name: "IX_CapacitacionEmpleado_CapacitacionId",
                table: "CapacitacionEmpleado",
                column: "CapacitacionId");

            migrationBuilder.CreateIndex(
                name: "IX_CapacitacionEmpleado_EmpleadoId",
                table: "CapacitacionEmpleado",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartamentosSiget_GerenciaId",
                table: "DepartamentosSiget",
                column: "GerenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentoEmpleado_DocumentoId",
                table: "DocumentoEmpleado",
                column: "DocumentoId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentoEmpleado_EmpleadoId",
                table: "DocumentoEmpleado",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentosCapacitacion_CapacitacionId",
                table: "DocumentosCapacitacion",
                column: "CapacitacionId");

            migrationBuilder.CreateIndex(
                name: "IX_EducacionEmpleado_CentroEducativoId",
                table: "EducacionEmpleado",
                column: "CentroEducativoId");

            migrationBuilder.CreateIndex(
                name: "IX_EducacionEmpleado_EducacionId",
                table: "EducacionEmpleado",
                column: "EducacionId");

            migrationBuilder.CreateIndex(
                name: "IX_EducacionEmpleado_EmpleadoId",
                table: "EducacionEmpleado",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_BancoId",
                table: "Empleado",
                column: "BancoId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_EstadoCivilId",
                table: "Empleado",
                column: "EstadoCivilId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_FondoPensionId",
                table: "Empleado",
                column: "FondoPensionId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_FormaPagoId",
                table: "Empleado",
                column: "FormaPagoId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_MunicipioId",
                table: "Empleado",
                column: "MunicipioId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_NacionalidadId",
                table: "Empleado",
                column: "NacionalidadId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_ProfesionOficioId",
                table: "Empleado",
                column: "ProfesionOficioId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_SexoId",
                table: "Empleado",
                column: "SexoId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_TipoCuentaId",
                table: "Empleado",
                column: "TipoCuentaId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_TipoSangreId",
                table: "Empleado",
                column: "TipoSangreId");

            migrationBuilder.CreateIndex(
                name: "IX_EmpleadoPlaza_EmpleadoId",
                table: "EmpleadoPlaza",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_EmpleadoPlaza_PlazaId",
                table: "EmpleadoPlaza",
                column: "PlazaId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluacionesDesempeños_EmpleadoId",
                table: "EvaluacionesDesempeños",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluacionesDesempeños_PreguntasFasesId",
                table: "EvaluacionesDesempeños",
                column: "PreguntasFasesId");

            migrationBuilder.CreateIndex(
                name: "IX_FamiliaPlazas_TipoEvaluacionesId",
                table: "FamiliaPlazas",
                column: "TipoEvaluacionesId");

            migrationBuilder.CreateIndex(
                name: "IX_FasesEvaluaciones_TipoEvaluacionesId",
                table: "FasesEvaluaciones",
                column: "TipoEvaluacionesId");

            migrationBuilder.CreateIndex(
                name: "IX_GrupoFamiliarEmpleado_EmpleadoId",
                table: "GrupoFamiliarEmpleado",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_GrupoFamiliarEmpleado_MiembrosFamiliaId",
                table: "GrupoFamiliarEmpleado",
                column: "MiembrosFamiliaId");

            migrationBuilder.CreateIndex(
                name: "IX_HorariosEmpleado_EmpleadoId",
                table: "HorariosEmpleado",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_HorariosEmpleado_HorariosId",
                table: "HorariosEmpleado",
                column: "HorariosId");

            migrationBuilder.CreateIndex(
                name: "IX_IdiomaEmpleados_EmpleadoId",
                table: "IdiomaEmpleados",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_IdiomaEmpleados_IdiomaId",
                table: "IdiomaEmpleados",
                column: "IdiomaId");

            migrationBuilder.CreateIndex(
                name: "IX_Municipios_DepartamentoId",
                table: "Municipios",
                column: "DepartamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Permisos_EmpleadoId",
                table: "Permisos",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Permisos_TipoPermisoId",
                table: "Permisos",
                column: "TipoPermisoId");

            migrationBuilder.CreateIndex(
                name: "IX_Plazas_DepartamentoSigetId",
                table: "Plazas",
                column: "DepartamentoSigetId");

            migrationBuilder.CreateIndex(
                name: "IX_Plazas_FamiliaPlazasId",
                table: "Plazas",
                column: "FamiliaPlazasId");

            migrationBuilder.CreateIndex(
                name: "IX_PreguntasFases_FasesEvaluacionesId",
                table: "PreguntasFases",
                column: "FasesEvaluacionesId");

            migrationBuilder.CreateIndex(
                name: "IX_PrestacionEmpleado_EmpleadoId",
                table: "PrestacionEmpleado",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_PrestacionEmpleado_PrestacionesId",
                table: "PrestacionEmpleado",
                column: "PrestacionesId");

            migrationBuilder.CreateIndex(
                name: "IX_ReferenciasPersonales_EmpleadoId",
                table: "ReferenciasPersonales",
                column: "EmpleadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Vacaciones_EmpleadoId",
                table: "Vacaciones",
                column: "EmpleadoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccionesDePersonal");

            migrationBuilder.DropTable(
                name: "Acuerdos");

            migrationBuilder.DropTable(
                name: "Candidato");

            migrationBuilder.DropTable(
                name: "CapacitacionEmpleado");

            migrationBuilder.DropTable(
                name: "DiasFeriados");

            migrationBuilder.DropTable(
                name: "DocumentoEmpleado");

            migrationBuilder.DropTable(
                name: "DocumentosCapacitacion");

            migrationBuilder.DropTable(
                name: "EducacionEmpleado");

            migrationBuilder.DropTable(
                name: "EmpleadoPlaza");

            migrationBuilder.DropTable(
                name: "EvaluacionesDesempeños");

            migrationBuilder.DropTable(
                name: "GrupoFamiliarEmpleado");

            migrationBuilder.DropTable(
                name: "HorariosEmpleado");

            migrationBuilder.DropTable(
                name: "IdiomaEmpleados");

            migrationBuilder.DropTable(
                name: "Marcaciones");

            migrationBuilder.DropTable(
                name: "MarcasImportadas");

            migrationBuilder.DropTable(
                name: "Parametros");

            migrationBuilder.DropTable(
                name: "Permisos");

            migrationBuilder.DropTable(
                name: "PrestacionEmpleado");

            migrationBuilder.DropTable(
                name: "ReferenciasPersonales");

            migrationBuilder.DropTable(
                name: "TiempoDescontable");

            migrationBuilder.DropTable(
                name: "Vacaciones");

            migrationBuilder.DropTable(
                name: "TipoAccionPersonal");

            migrationBuilder.DropTable(
                name: "TipoAcuerdos");

            migrationBuilder.DropTable(
                name: "Documentos");

            migrationBuilder.DropTable(
                name: "Capacitacion");

            migrationBuilder.DropTable(
                name: "CentroEducativo");

            migrationBuilder.DropTable(
                name: "Educacion");

            migrationBuilder.DropTable(
                name: "Plazas");

            migrationBuilder.DropTable(
                name: "PreguntasFases");

            migrationBuilder.DropTable(
                name: "MiembrosFamilia");

            migrationBuilder.DropTable(
                name: "Horarios");

            migrationBuilder.DropTable(
                name: "Idiomas");

            migrationBuilder.DropTable(
                name: "TipoPermisos");

            migrationBuilder.DropTable(
                name: "Prestaciones");

            migrationBuilder.DropTable(
                name: "Empleado");

            migrationBuilder.DropTable(
                name: "Facilitadores");

            migrationBuilder.DropTable(
                name: "DepartamentosSiget");

            migrationBuilder.DropTable(
                name: "FamiliaPlazas");

            migrationBuilder.DropTable(
                name: "FasesEvaluaciones");

            migrationBuilder.DropTable(
                name: "Bancos");

            migrationBuilder.DropTable(
                name: "EstadosCivil");

            migrationBuilder.DropTable(
                name: "FondosPension");

            migrationBuilder.DropTable(
                name: "FormaPagos");

            migrationBuilder.DropTable(
                name: "Municipios");

            migrationBuilder.DropTable(
                name: "Nacionalidades");

            migrationBuilder.DropTable(
                name: "ProfesionOficio");

            migrationBuilder.DropTable(
                name: "Sexo");

            migrationBuilder.DropTable(
                name: "TiposCuenta");

            migrationBuilder.DropTable(
                name: "TiposSangre");

            migrationBuilder.DropTable(
                name: "Gerencias");

            migrationBuilder.DropTable(
                name: "TipoEvaluaciones");

            migrationBuilder.DropTable(
                name: "Departamentos");
        }
    }
}
