﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Educacion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        //public IEnumerable<EducacionEmpleado> educacionEmpleados { get; set; }
    }
}
