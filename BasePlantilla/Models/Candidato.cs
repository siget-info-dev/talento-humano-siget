﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Candidato
    {
        public int Id { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string ApellidoCasada { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string NombreArchivo { get; set; }
        [NotMapped]
        public IFormFile FFArchivo { get; set; }
        
        //Relaciones
        public int ProfesionOficioId { get; set; }
        public ProfesionOficio ProfesionOficio { get; set; }
        public int EducacionId { get; set; }
        public Educacion Educacion { get; set; }

        public string GetNombreCompleto()
        {
            return $"{PrimerNombre} {SegundoNombre} {PrimerApellido} {SegundoApellido} {ApellidoCasada}";
        }
    }
}
