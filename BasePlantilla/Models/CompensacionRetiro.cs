﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class CompensacionRetiro
    {
        public int Id { get; set; }
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
        public DateTime FechaSalida { get; set; }
        public int AnnoAntiguedad { get; set; }
        public int DiasAntiguedad { get; set; }
        public int DiasAguinaldo { get; set; }
        public int DiasBono { get; set; }
        public int DiasVacaciones { get; set; }
        public decimal Salario { get; set; }
        public decimal BonoAntiguedadAnnos { get; set; }
        public decimal BonoAntiguedadDias { get; set; }
        public decimal VacacionProporcional { get; set; }
        public decimal BonoProporcional { get; set; }
        public decimal AguinaldoProporcional { get; set; }
        public decimal TotalCompensaciones { get; set; }
        public decimal MontoAFP { get; set; }
        public decimal MontoAFPPatrono { get; set; }
        public decimal MontoISR { get; set; }
        public int DiasNoTrabajado { get; set; }
        public decimal DescuentoDiasNoTrabajado { get; set; }
        public decimal DescuentoProcuraduria { get; set; }
        public decimal DescuentosOtros { get; set; }
        public decimal TotalLiquidacion { get; set; }
        public int CreaEmpleado { get; set; }
        public DateTime FechaCreado { get; set; }
    }
}
