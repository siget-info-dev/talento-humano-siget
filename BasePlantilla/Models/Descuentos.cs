﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Descuentos
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public float Valor { get; set; }
        public bool DescuentoAplicado { get; set; }
        //public bool EsMasivo { get; set; }
    }
}
