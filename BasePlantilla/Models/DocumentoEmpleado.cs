﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class DocumentoEmpleado
    {
        public int Id { get; set; }
        public string NombreArchivo { get; set; }
        [NotMapped]
        public IFormFile FFArchivo { get; set; }
        public int DocumentoId { get; set; }
        public Documentos Documento { get; set; }
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
        
    }
}
