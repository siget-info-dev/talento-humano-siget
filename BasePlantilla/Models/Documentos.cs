﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Documentos
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
