﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class EvaluacionesDesempeño
    {
        public int Id { get; set; }
        public string PeriodoEvaluacion { get; set; }
        public int Calificacion { get; set; }
        



        //Todo: ver que campos tiene que llevar estas evaluaciones. 

        public int PreguntasFasesId { get; set; }
        public PreguntasFases PreguntasFases { get; set; } 
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }

        public static implicit operator List<object>(EvaluacionesDesempeño v)
        {
            throw new NotImplementedException();
        }
    }
}
