﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Prestaciones
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool PrestacionGerencial { get; set; }
        public bool PrestacionAplicada { get; set; }
    }
}
