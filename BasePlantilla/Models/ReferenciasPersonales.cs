﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class ReferenciasPersonales
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string LugarTrabajo { get; set; }
        public string Telefono { get; set; }

        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
    }
}
