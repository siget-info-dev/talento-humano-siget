﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class MarcasImportadas
    {
        public int Id { get; set; }
        public int CodigoEmpleado { get; set; }
        public DateTime FechaMarcacion { get; set; }
    }
}
