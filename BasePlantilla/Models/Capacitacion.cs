﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Capacitacion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [DataType(DataType.Date)]
        public DateTime FechaInicio { get; set; }
        [DataType(DataType.Date)]
        public DateTime FechaFin { get; set; }
        public string Descripcion { get; set; }
        public int Estado { get; set; }
        public int TipoCapacitacion { get; set; }
        public int FacilitadoresId { get; set; }
        public Facilitadores Facilitadores { get; set; }

        public IEnumerable<CapacitacionEmpleado> CapacitacionEmpleado { get; set; }

    }
}
