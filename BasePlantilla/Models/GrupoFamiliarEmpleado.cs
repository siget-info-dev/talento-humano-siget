﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class GrupoFamiliarEmpleado
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        [Display(Name = "Fecha Nacimiento")]
        [DataType(DataType.Date)]
        public DateTime FechaNacimiento { get; set; }
        public int MiembrosFamiliaId { get; set; }
        public MiembrosFamilia MiembrosFamilia { get; set; }
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
    }
}
