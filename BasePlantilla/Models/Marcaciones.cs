﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Marcaciones
    {
        public int Id { get; set; }
        public int CodigoEmpleado { get; set; }
        [DataType(DataType.Date)]
        public DateTime Fecha { get; set; }
        [DataType(DataType.Time)]
        public DateTime Entrada { get; set; }
        [DataType(DataType.Time)]
        public DateTime Salida { get; set; }
        public int Revisar { get; set; }
    }
}
