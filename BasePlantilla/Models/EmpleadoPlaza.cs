﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    [Table("EmpleadoPlaza")]
    public class EmpleadoPlaza
    {
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "Date")]
        [DataType(DataType.Date)]
        public DateTime FechaInicio { get; set; }

        [Column(TypeName = "Date")]
        [DataType(DataType.Date)]
        public DateTime? FechaFinal { get; set; }

        public int? NumeroContrato { get; set; }

        [Column(TypeName = "Decimal(8,2)")]
        public decimal Salario { get; set; }
        public string Observacion { get; set; }


        //Relaciones
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }

        public int PlazaId { get; set; }
        public Plaza Plaza { get; set; }



    }
}
