﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class TipoEvaluaciones
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        //public int FamiliaPlazasId { get; set; }
        //public FamiliaPlazas FamiliaPlazas { get; set; }
    }
}
