﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class TipoPermiso
    {
        public int Id { get; set; }
        public string Permiso { get; set; }
        public int Estado { get; set; }
    }
}
