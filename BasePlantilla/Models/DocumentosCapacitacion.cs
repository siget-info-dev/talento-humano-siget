﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class DocumentosCapacitacion
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string NombreArchivo { get; set; }
        [NotMapped]
        public IFormFile FFArchivo { get; set; }
        public int CapacitacionId { get; set; }
        public Capacitacion Capacitacion { get; set; }
    }
}
