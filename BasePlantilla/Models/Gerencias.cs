﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    [Table("Gerencias")]
    public class Gerencias
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Nombre { get; set; }

        [Display(Name = "Departamentos/Unidad")]
        public IEnumerable<DepartamentoSiget> DepartamentosSiget { get; set; }

        //public IEnumerable<Plaza> Plazas { get; set; }

    }
}
