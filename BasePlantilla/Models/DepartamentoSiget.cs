﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    [Table("DepartamentosSiget")]
    public class DepartamentoSiget
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(4)]
        public string Codigo { get; set; }

        [Required]
        [MaxLength(100)]
        public string Nombre { get; set; }
        [Display(Name = "Gerencia")]
        public int GerenciaId { get; set; }
        public Gerencias Gerencia { get; set; }
        public IEnumerable<Plaza> Plazas { get; set; }

    }
}
