﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class FasesEvaluaciones
    {
        public int Id { get; set; }
        public string NombreFase { get; set; }
        public int PorcentajeEvaluacion { get; set; }
        public int TipoEvaluacionesId { get; set; }
        public TipoEvaluaciones TipoEvaluaciones { get; set; }
    }
}
