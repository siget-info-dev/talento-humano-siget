﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Facilitadores
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Proveedor { get; set; }
    }
}
