﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class TiempoDescontable
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public double MinutosADescontar { get; set; }
        public int CodigoEmpleado { get; set; }
       
    }
}
