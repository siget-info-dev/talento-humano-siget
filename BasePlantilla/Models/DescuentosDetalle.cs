﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class DescuentosDetalle
    {
        public int Id { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Fecha { get; set; }

        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
        public int DescuentosId { get; set; }
        public Descuentos Descuentos { get; set; }
    }
}
