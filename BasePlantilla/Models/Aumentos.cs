﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Aumentos
    {
        public int Id { get; set; }
        public string Nombre { set; get; }
        [Column(TypeName = "Decimal(8,2)")]
        public float PorcentajeAumento { get; set; }
        public bool AumentoAplicado { get; set; }
    }
}
