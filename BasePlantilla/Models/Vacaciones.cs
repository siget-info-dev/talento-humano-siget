﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Vacaciones
    {
        public int Id { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Inicio { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Fin { get; set; }
        public int Periodo { get; set; }
        public string Comentario { get; set; }
        //[NotMapped]
        //public int IdEmpleado { get; set; }




        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
    }
}
