﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class EducacionEmpleado
    {
        public int Id { get; set; }
        [Display(Name = "Grado Academico")]
        public string GradoAcademico { get; set; }
        public int Año { get; set; }
        [Display(Name = "Nivel Educacion")]
        public int EducacionId { get; set; }
        [NotMapped]
        public IFormFile FFArchivo { get; set; }
        public string ArchivoRespaldo { get; set; }
        [Display(Name = "Nivel Educacion")]
        public Educacion Educacion { get; set; }
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
        [Display(Name = "Centro Educativo")]
        public int CentroEducativoId { get; set; }
        [Display(Name = "Centro Educativo")]
        public CentroEducativo CentroEducativo { get; set; }
    }
}
