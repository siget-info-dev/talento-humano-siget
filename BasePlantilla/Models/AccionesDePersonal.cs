﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class AccionesDePersonal
    {
        public int Id { get; set; }
        [MaxLength(12)]
        public string Numero { get; set; }
        public string Comentarios { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime FechaAccionPersonal { get; set; }
        public DateTime Fecha { set { Fecha = DateTime.Now; } }

        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
        public int AccionesId { get; set; }
        public TipoAccionesDePersonal Acciones { get; set; }
    }
}
