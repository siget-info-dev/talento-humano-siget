﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Horarios
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string ToleranciaMaxima { get; set; }
        public int DiaSemana { get; set; }
        [NotMapped]
        public string Dia { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Final { get; set; }
        public bool PorDefecto { get; set; }
        public IEnumerable<HorariosEmpleado> HorariosEmpleado { get; set; }

    }
}
