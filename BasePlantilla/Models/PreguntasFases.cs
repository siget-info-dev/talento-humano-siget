﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class PreguntasFases
    {
        public int Id { get; set; }
        public string Pregunta { get; set; }
        
        public int FasesEvaluacionesId { get; set; }
        public FasesEvaluaciones FasesEvaluaciones { get; set; }
    }
}
