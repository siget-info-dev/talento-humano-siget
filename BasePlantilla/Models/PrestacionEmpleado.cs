﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class PrestacionEmpleado
    {
        public int Id { get; set; }
        public DateTime FechaEntrega { get; set; }
        
        //Relaciones
        public int PrestacionesId { get; set; }
        public Prestaciones Prestaciones { get; set; }
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
    }
}
