﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Empleado
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string PrimerNombre { get; set; }
        [MaxLength(50)]
        public string SegundoNombre { get; set; }
        [MaxLength(50)]
        public string TercerNombre { get; set; }
        [Required]
        [MaxLength(50)]
        public string PrimerApellido { get; set; }
        [MaxLength(50)]
        public string SegundoApellido { get; set; }
        [MaxLength(50)]
        public string ApellidoCasada { get; set; }
        [MaxLength(100)]
        public string ConocidoPor { get; set; }
        [MaxLength(50)]
        public string EmailPersonal { get; set; }
        public string LugarNacimiento { get; set; }
        [Required]
        public string DireccionResidencia { get; set; }
        public int CodigoEmpleado { get; set; }
        [MaxLength(25)]
        public ulong NumeroCuenta { get; set; }
        public float Estatura { get; set; }
        [MaxLength(10)]
        public string TelefonoFijo { get; set; }
        [MaxLength(10)]
        public string TelefonoMovil { get; set; }
        public string RutaImagen { get; set; }
        public string Observaciones { get; set; }

        //Documentos
        [Required]
        [MaxLength(10)]
        public string Dui { get; set; }
        [Required]
        [MaxLength(17)]
        public string Nit { get; set; }
        [MaxLength(9)]
        public string Isss { get; set; }
        [MaxLength(12)]
        public string NumeroFondoPension { get; set; }
        [MaxLength(15)]
        public string Pasaporte { get; set; }


        //Fechas
        [Required]
        [Column(TypeName = "Date")]
        public DateTime FechaIngreso { get; set; }
        [Column(TypeName = "Date")]
        public DateTime? FechaRetiro { get; set; }
        [Required]
        [Column(TypeName = "Date")]
        public DateTime FechaNacimiento { get; set; }
        [Column(TypeName = "Date")]
        public DateTime FechaEmisionDui { get; set; }
        [Column(TypeName = "Date")]
        public DateTime FechaVencimientoDui { get; set; }

        //Datos Emergencia
        public string NombreContactoEmergencia { get; set; }
        public string TelefonoEmergencia { get; set; }
        public string DireccionEmergencia { get; set; }
        public string ObservacionesEmergencia { get; set; }


        // Campos boleanos (Señales)
        public bool Activo { get; set; } = true;
        public bool Jubilado { get; set; } = false;
        public bool Marcacion { get; set; }
        public bool PoseeLicencia { get; set; }
        public bool PrestacionesGerenciales { get; set; } = false;



        //Relaciones
        [Required]
        public int TipoSangreId { get; set; }
        public TipoSangre TipoSangre { get; set; }
        [Required]
        public int NacionalidadId { get; set; }
        public Nacionalidad Nacionalidad { get; set; }
        [Required]
        public int EstadoCivilId { get; set; }
        public EstadoCivil EstadoCivil { get; set; }
        [Required]
        public int SexoId { get; set; }
        public Sexo Sexo { get; set; }
        [Required]
        public int ProfesionOficioId { get; set; }
        public ProfesionOficio ProfesionOficio { get; set; }
        [Required]
        public int? FondoPensionId { get; set; }
        public FondoPension FondoPension { get; set; }
        [Required]
        public int MunicipioId { get; set; }
        public Municipio Municipio { get; set; }
        [Required]
        public int FormaPagoId { get; set; }
        public FormaPago FormaPago { get; set; }
        [Required]
        public int? BancoId { get; set; }
        public Banco Banco { get; set; }
        [Required]
        public int? TipoCuentaId { get; set; }
        public TipoCuenta TipoCuenta { get; set; }
        //public int? GrupoFamiliarEmpleadoId { get; set; }
        //public GrupoFamiliarEmpleado GrupoFamiliarEmpleado { get; set; }
        //public int? ReferenciasPersonalesId { get; set; }
        //public ReferenciasPersonales ReferenciasPersonales { get; set; }
        public IEnumerable<EmpleadoPlaza> EmpleadoPlaza { get; set; }
        public IEnumerable<IdiomasEmpleado> IdiomasEmpleado { get; set; }
        public IEnumerable<GrupoFamiliarEmpleado> GrupoFamiliarEmpleado { get; set; }
        public IEnumerable<ReferenciasPersonales> ReferenciasPersonales { get; set; }
        public IEnumerable<EducacionEmpleado> EducacionEmpleado { get; set; }
        public IEnumerable<DocumentoEmpleado> DocumentoEmpleado { get; set; }
        public IEnumerable<DescuentosDetalle> DescuentosDetalle { get; set; }





        //Funciones
        public string GetNombreCompleto()
        {
            return $"{PrimerNombre} {SegundoNombre} {TercerNombre} {PrimerApellido} {SegundoApellido} {ApellidoCasada}";
        }

        public string GetCargoActual()
        {
            var cargo = EmpleadoPlaza?.Where(x => x.FechaFinal == null && x.EmpleadoId == Id).FirstOrDefault();
            //    //.FirstOrDefault(x => x.Plaza.PlazaActiva == true);

            return cargo == null ? "Sin cargo asignado" : $"{cargo?.Plaza.Nombre}";


        }


        //[Column(TypeName = "Decimal(4,1)")]
        //public decimal HorasTrabajo { get; set; }
        //[MaxLength(50)]
        //public string UserNameSiget { get; set; }

        //[MaxLength(50)]
        //public string EmailSiget { get; set; }

        //[MaxLength(10)] 
        //public string TelefonoInstitucional { get; set; }

        //[MaxLength(10)]
        //public string ExtensionInstitucional { get; set; }
    }
}
