﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class TipoSangre
    {
        public int Id { get; set; }

        [Required] 
        [MaxLength(10)]
        public string Nombre { get; set; }
    }
}
