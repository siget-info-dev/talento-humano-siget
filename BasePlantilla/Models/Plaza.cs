﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Plaza
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(240)]
        public string Nombre { get; set; }

        [MaxLength(60)]
        [Display(Name = "Nombre Corto")]
        public string NombreCorto { get; set; }
        [Display(Name = "Codigo Plaza")]
        public string CodigoPlaza { get; set; }
        [Display(Name = "Plaza Activa")]
        public bool PlazaActiva { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Fecha Creacion")]
        public DateTime FechaCreacion { get; set; }
        public string Acuerdo { get; set; }

        public int FamiliaPlazasId { get; set; }
        public FamiliaPlazas FamiliaPlazas { get; set; }

        [Display(Name = "Departamento/Unidad")]
        public int? DepartamentoSigetId { get; set; }
        public DepartamentoSiget DepartamentoSiget { get; set; }
    }
}
