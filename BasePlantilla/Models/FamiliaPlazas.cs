﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class FamiliaPlazas
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [Display(Name = "Tipo Evaluaciones")]
        public int TipoEvaluacionesId { get; set; }
        public TipoEvaluaciones TipoEvaluaciones { get; set; }

    }
}
