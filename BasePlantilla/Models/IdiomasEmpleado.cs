﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    [Table("IdiomaEmpleados")]
    public class IdiomasEmpleado
    {
        public int Id { get; set; }
        [Display(Name = "Nivel Idioma")]
        public string NivelIdioma { get; set; }
        [Display(Name = "Idioma")]
        public int IdiomaId { get; set; }
        public Idiomas Idioma { get; set; }
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
    }
}
