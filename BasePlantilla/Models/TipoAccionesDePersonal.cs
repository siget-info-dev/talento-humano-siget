﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class TipoAccionesDePersonal
    {
        public int Id { get; set; }

        public string Acciones { get; set; }
    }
}
