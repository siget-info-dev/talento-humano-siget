﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Acuerdos
    {
        public int Id { get; set; }
        public string NumeroAcuerdo { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime FechaAcuerdo { get; set; }
        public string Comentarios { get; set; }

        public DateTime Fecha { set { Fecha = DateTime.Now; } }
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
        public int AcuerdoId { get; set; }
        public TipoAcuerdo Acuerdo { get; set; }

    }
}
