﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class Permisos
    {
        public int Id { get; set; }
        public bool AplicaDescuento { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        //public int Estado { get; set; }
        public string Comentario { get; set; }
        //[NotMapped]
        //public int IdEmpleado { get; set; }
        [NotMapped]
        public int IdPermiso { get; set; }


        public TipoPermiso TipoPermiso { get; set; }
        public int TipoPermisoId { get; set; }
        public Empleado Empleado { get; set; }
        public int EmpleadoId { get; set; }
    }
}
