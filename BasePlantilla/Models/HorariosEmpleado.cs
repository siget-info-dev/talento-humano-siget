﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    public class HorariosEmpleado
    {
        public int id { get; set; }
        public int HorariosId { get; set; }
        public Horarios Horarios { get; set; }
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
    }
}
