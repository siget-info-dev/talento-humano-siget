﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaGestionTalentoHumano.Models
{
    [Table("Idiomas")]
    public class Idiomas
    {
        public int Id { get; set; }
        public string Idioma { get; set; }
    }
}
