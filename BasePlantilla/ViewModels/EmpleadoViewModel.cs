﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.ViewModels
{
    public class EmpleadoViewModel
    {
        public EmpleadoViewModel()
        {
        }

        //public EmpleadoViewModel(Empleado Empleado)
        //{
        //    Id = Empleado.Id;
        //    PrimerNombreDui = Empleado.PrimerNombreDui;
        //    SegundoNombreDui = Empleado.SegundoNombreDui;
        //    TercerNombreDui = Empleado.TercerNombreDui;
        //    PrimerApellidoDui = Empleado.PrimerApellidoDui;
        //    SegundoNombreDui = Empleado.SegundoNombreDui;
        //    ApellidoCasadaDui = Empleado.ApellidoCasadaDui;
        //    NombreCompleto = PrimerNombreDui + " " + SegundoNombreDui + " " + TercerNombreDui + " " + PrimerApellidoDui + " " +
        //                     SegundoApellidoDui + " " + ApellidoCasadaDui;
        //    SexoId = Empleado.SexoId;
        //    Sexo = Empleado.Sexo;
        //    EstadoCivilId = Empleado.EstadoCivilId;
        //    EstadoCivil = Empleado.EstadoCivil;
        //    TipoSangreId = Empleado.TipoSangreId;
        //    TipoSangre = Empleado.TipoSangre;
        //    TelefonoFijo = Empleado.TelefonoFijo;
        //    TelefonoMovil = Empleado.TelefonoMovil;
        //    EmailPersonal = Empleado.EmailPersonal;
        //    Dui = Empleado.Dui;
        //    Nit = Empleado.Nit;
        //    Isss = Empleado.Isss;
        //    FondoPensionId = Empleado.FondoPensionId;
        //    FondoPension = Empleado.FondoPension;
        //    Nup = Empleado.Nup;
        //    PoseeLicencia = Empleado.PoseeLicencia;
        //    Pasaporte = Empleado.Pasaporte;
        //    MunicipioId = Empleado.MunicipioId;
        //    Municipio = Empleado.Municipio;
        //    DireccionResidencia = Empleado.DireccionResidencia;

        //    NombreContactoEmergencia = Empleado.NombreContactoEmergencia;
        //    TelefonoEmergencia = Empleado.TelefonoEmergencia;
        //    DireccionEmergencia = Empleado.DireccionEmergencia;
        //    ObservacionesEmergencia = Empleado.ObservacionesEmergencia;

        //    CodigoEmpleado = Empleado.CodigoEmpleado;
        //    UserNameSiget = Empleado.UserNameSiget;
        //    EmailSiget = Empleado.EmailSiget;

        //    FormaPagoId = Empleado.FormaPagoId;
        //    FormaPago = Empleado.FormaPago;
        //    BancoId = Empleado.BancoId;
        //    Banco = Empleado.Banco;
        //    NumeroCuenta = Empleado.NumeroCuenta;
        //    TipoCuentaId = Empleado.TipoCuentaId;
        //    TipoCuenta = Empleado.TipoCuenta;
        //}

            public int Id { get; set; }

            [Required]
            [MaxLength(50)]
            [Display(Name = "Primer Nombre")]
            public string PrimerNombre { get; set; }

            [MaxLength(50)]
            [Display(Name = "Segundo Nombre")]
            public string SegundoNombre { get; set; }

            [MaxLength(50)]
            [Display(Name = "Tercer Nombre")]
            public string TercerNombre { get; set; }

            [Required]
            [MaxLength(50)]
            [Display(Name = "Primer Apellido")]
            public string PrimerApellido { get; set; }

            [MaxLength(50)]
            [Display(Name = "Segundo Apellido")]
            public string SegundoApellido { get; set; }

            [MaxLength(50)]
            [Display(Name = "Apellido Casada")]
            public string ApellidoCasada { get; set; }

            [MaxLength(100)]
            [Display(Name = "Conocido Por Segun Dui")]
            public string ConocidoPor { get; set; }

            public string NombreCompleto { get; set; }

            [MaxLength(10)]
            [Display(Name = "Telefono Fijo")]
            public string TelefonoFijo { get; set; }

            [MaxLength(10)]
            [Display(Name = "Telefono Movil")]
            public string TelefonoMovil { get; set; }

            [MaxLength(50)]
            [Display(Name = "Email Personal")]
            public string EmailPersonal { get; set; }

            [Required]
            [MaxLength(10)]
            [Display(Name = "DUI")]
            public string Dui { get; set; }

            [Required]
            [MaxLength(17)]
            [Display(Name = "NIT")]
            public string Nit { get; set; }

            [MaxLength(9)]
            [Display(Name = "ISSS")]
            public string Isss { get; set; }

            [MaxLength(12)]
            [Display(Name = "Numero Fondo Pension")]
            public string NumeroFondoPension { get; set; }

            [Display(Name = "Posee Licencia")]
            public bool PoseeLicencia { get; set; }

            [Display(Name = "Prestaciones Gerenciales")]
            public bool PrestacionesGerenciales { get; set; } = false;

            [MaxLength(15)]
            public string Pasaporte { get; set; }

            [Required]
            [DataType(DataType.Date)]
            [Display(Name = "Fecha Nacimiento")]
            public DateTime FechaNacimiento { get; set; }

            [Required]
            [Display(Name = "Direccion Residencia")]
            public string DireccionResidencia { get; set; }

            [Display(Name = "Nombre Contacto Emergencia")]
            public string NombreContactoEmergencia { get; set; }

            [Display(Name = "Telefono Emergencia")]
            public string TelefonoEmergencia { get; set; }

            [Display(Name = "Direccion Emergencia")]
            public string DireccionEmergencia { get; set; }

            [Display(Name = "Observaciones Emergencia")]
            public string ObservacionesEmergencia { get; set; }

            [Display(Name = "Codigo Empleado")]
            public int CodigoEmpleado { get; set; }

            [Display(Name = "Imagen")]
            public string RutaImagen { get; set; }
            public IFormFile FFFotoEmpleado { get; set; }

            [Required]
            [DataType(DataType.Date)]
            [Display(Name = "Fecha Ingreso")]
            public DateTime FechaIngreso { get; set; }

            [DataType(DataType.Date)]
            [Display(Name = "Fecha Retiro")]
            public DateTime? FechaRetiro { get; set; }

            public bool Activo { get; set; } = true;

            public bool Jubilado { get; set; } = false;

            public bool Marcacion { get; set; }

            public string Observaciones { get; set; }

            [Display(Name = "Numero Cuenta")]
            public string NumeroCuenta { get; set; }


            [DisplayFormat(NullDisplayText = "Sin cargo asignado")]
            [Display(Name = "Cargo Actual")]
            public string CargoActual { get; set; }

            [Display(Name = "Nivel Idioma")]
            public string NivelIdioma { get; set; }

            //referencias personales 
            [Display(Name = "Nombre Referencia")]
            public string NombreReferencia { get; set; }
            [Display(Name = "Lugar Trabajo")]
            public string LugarTrabajoReferencia { get; set; }
            [Display(Name = "Telefono Referencia")]
            public string TelefonoReferencia { get; set; }


            //Miembros de familia
            [Display(Name = "Nombre Miembro")]
            public string NombreMiembroFamilia { get; set; }
            [DataType(DataType.Date)]
            [Display(Name = "Fecha Nacimiento")]
            public DateTime FechaNacimientoMiembroFamilia { get; set; }

            [Display(Name = "Lugar Nacimiento")]
            public string LugarNacimiento { get; set; }

            [DataType(DataType.Date)]
            [Display(Name = "Fecha Emision Dui")]
            public DateTime FechaEmisionDui { get; set; }
            [DataType(DataType.Date)]
            [Display(Name = "Fecha Vencimiento Dui")]
            public DateTime FechaVencimientoDui { get; set; }
            public float Estatura { get; set; }

            //Nivel de Educacion
            public string TituloAcademico { get; set; }
            public string AñoTituloAcademico { get; set; }
            public int EducacionId { get; set; }
            public int CentroEducativoId { get; set; }



            //Relaciones
            [Required]
            [Display(Name = "Sexo")]
            public int SexoId { get; set; }
            public Sexo Sexo { get; set; }

            [Required]
            [Display(Name = "Estado Civil")]
            public int EstadoCivilId { get; set; }
            public EstadoCivil EstadoCivil { get; set; }

            [Display(Name = "Tipo Sangre")]
            public int TipoSangreId { get; set; }
            public TipoSangre TipoSangre { get; set; }

            [Required]
            [Display(Name = "Forma Pago")]
            public int FormaPagoId { get; set; }
            public FormaPago FormaPago { get; set; }

            [Display(Name = "Banco")]
            public int? BancoId { get; set; }
            public Banco Banco { get; set; }

            [Required]
            [Display(Name = "Departamento")]
            public int DepartamentoId { get; set; }

            [Required]
            [Display(Name = "Municipio")]
            public int MunicipioId { get; set; }
            public Municipio Municipio { get; set; }

            [Display(Name = "Fondo Pension")]
            public int? FondoPensionId { get; set; }
            public FondoPension FondoPension { get; set; }

            [Display(Name = "Tipo Cuenta")]
            public int TipoCuentaId { get; set; }
            public TipoCuenta TipoCuenta { get; set; }

            [Display(Name = "Idiomas")]
            public int IdiomaId { get; set; }
            public List<IdiomasEmpleado> IdiomasEmpleado { get; set; }

            [Display(Name = "Profesion Oficio")]
            public int ProfesionOficioId { get; set; }
            public ProfesionOficio ProfesionOficio { get; set; }

            [Display(Name = "Nacionalidad")]
            public int NacionalidadId { get; set; }
            public Nacionalidad Nacionalidad { get; set; }

            [Display(Name ="Parentesco")]
            public int GrupoFamiliarEmpleadoId { get; set; }
            public List<GrupoFamiliarEmpleado> GrupoFamiliarEmpleado { get; set; }

            [Display(Name = "Referencias Personales")]
            public int ReferenciasPersonalesId { get; set; }
            public List<ReferenciasPersonales> ReferenciasPersonales {get;set;}

            [Display(Name = "Educacion Empleado")]
            public int EducacionEmpleadoId { get; set; }
            public List<EducacionEmpleado> EducacionEmpleado { get; set; }
        public IEnumerable<EmpleadoPlaza> EmpleadoPlaza { get; set; }
    }
}
