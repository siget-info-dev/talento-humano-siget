﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.ViewModels
{
    public class EmpleadoPlazasViewModel
    {
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }

        public int CargoId { get; set; }        
        public Plaza Cargo { get; set; }

        public int PlazaId { get; set; }
        public Plaza Plaza { get; set; }

        [Required]
        [Column(TypeName = "Date")]
        public DateTime FechaInicio { get; set; }

        [Column(TypeName = "Date")]
        public DateTime FechaFinal { get; set; }

        public int? NumeroContrato { get; set; }

        [Column(TypeName = "Decimal(8,2)")]
        public decimal Salario { get; set; }

        public bool PlazaActiva { get; set; }
    }
}
