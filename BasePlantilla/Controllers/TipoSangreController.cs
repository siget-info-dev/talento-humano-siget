﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class TipoSangreController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public TipoSangreController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: TipoSangre
        public async Task<IActionResult> Index()
        {
            return View(await _context.TiposSangre.ToListAsync());
        }

        // GET: TipoSangre/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoSangre = await _context.TiposSangre
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoSangre == null)
            {
                return NotFound();
            }

            return View(tipoSangre);
        }

        // GET: TipoSangre/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoSangre/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre")] TipoSangre tipoSangre)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoSangre);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoSangre);
        }

        // GET: TipoSangre/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoSangre = await _context.TiposSangre.FindAsync(id);
            if (tipoSangre == null)
            {
                return NotFound();
            }
            return View(tipoSangre);
        }

        // POST: TipoSangre/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] TipoSangre tipoSangre)
        {
            if (id != tipoSangre.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoSangre);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoSangreExists(tipoSangre.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoSangre);
        }

        // GET: TipoSangre/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoSangre = await _context.TiposSangre
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoSangre == null)
            {
                return NotFound();
            }

            return View(tipoSangre);
        }

        // POST: TipoSangre/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoSangre = await _context.TiposSangre.FindAsync(id);
            _context.TiposSangre.Remove(tipoSangre);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoSangreExists(int id)
        {
            return _context.TiposSangre.Any(e => e.Id == id);
        }
    }
}
