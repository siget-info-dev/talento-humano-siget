﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlazasApiController : ControllerBase
    {
        private readonly TalentoHumanoDbContext _context;

        public PlazasApiController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: api/PlazasApi
        [HttpGet]
        public ActionResult<IEnumerable<Plaza>> GetPlazas(int? departamentoSigetId)
        {
            var plazas = _context.Plazas.ToList();
            if (departamentoSigetId != null)
            {
                plazas = plazas.Where(x => x.DepartamentoSigetId == departamentoSigetId).ToList();
            }
            return plazas;
        }

        // GET: api/PlazasApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Plaza>> GetPlaza(int id)
        {
            var plaza = await _context.Plazas.FindAsync(id);

            if (plaza == null)
            {
                return NotFound();
            }

            return plaza;
        }

        // PUT: api/PlazasApi/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlaza(int id, Plaza plaza)
        {
            if (id != plaza.Id)
            {
                return BadRequest();
            }

            _context.Entry(plaza).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlazaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PlazasApi
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Plaza>> PostPlaza(Plaza plaza)
        {
            _context.Plazas.Add(plaza);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPlaza", new { id = plaza.Id }, plaza);
        }

        // DELETE: api/PlazasApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Plaza>> DeletePlaza(int id)
        {
            var plaza = await _context.Plazas.FindAsync(id);
            if (plaza == null)
            {
                return NotFound();
            }

            _context.Plazas.Remove(plaza);
            await _context.SaveChangesAsync();

            return plaza;
        }

        private bool PlazaExists(int id)
        {
            return _context.Plazas.Any(e => e.Id == id);
        }
    }
}
