﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class DocumentosCapacitacionController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public DocumentosCapacitacionController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: DocumentosCapacitacion
        public async Task<IActionResult> Index(int Id)
        {
            var talentoHumanoDbContext = _context.DocumentosCapacitacion.Include(d => d.Capacitacion).Where(d => d.CapacitacionId == Id);
            ViewData["NombreCapacitacion"] = _context.Capacitacion.Where(x => x.Id == Id).Select(x => x.Nombre).FirstOrDefault();
            ViewData["IdCapacitacion"] = _context.Capacitacion.Where(x => x.Id == Id).Select(x => x.Id).FirstOrDefault();

            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: DocumentosCapacitacion/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var documentosCapacitacion = await _context.DocumentosCapacitacion
                .Include(d => d.Capacitacion)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (documentosCapacitacion == null)
            {
                return NotFound();
            }

            return View(documentosCapacitacion);
        }

        // GET: DocumentosCapacitacion/Create
        public IActionResult Create(int Id)
        {
            ViewData["NombreCapacitacion"] = _context.Capacitacion.Where(x => x.Id == Id).Select(x => x.Nombre).FirstOrDefault();
            ViewData["IdCapacitacion"] = _context.Capacitacion.Where(x => x.Id == Id).Select(x => x.Id).FirstOrDefault();
            ViewData["Id"] = Id;
            return View();
        }

        // POST: DocumentosCapacitacion/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Descripcion,NombreArchivo,CapacitacionId,FFArchivo")] DocumentosCapacitacion documentosCapacitacion)
        {
            if (ModelState.IsValid)
            {
                string NombreArchivo = "";
                if (documentosCapacitacion.FFArchivo != null)
                {
                    NombreArchivo = Tools.subeArchivo(documentosCapacitacion.FFArchivo);
                }
                documentosCapacitacion.NombreArchivo = NombreArchivo;

                _context.Add(documentosCapacitacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = documentosCapacitacion.CapacitacionId });
            }
            return View(documentosCapacitacion);
        }

        // GET: DocumentosCapacitacion/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var documentosCapacitacion = await _context.DocumentosCapacitacion.FindAsync(id);
            if (documentosCapacitacion == null)
            {
                return NotFound();
            }
            ViewData["IdCapacitacion"] = (from c in _context.Capacitacion
                                          where (from dc in _context.DocumentosCapacitacion
                                                 where dc.Id == id
                                                 select dc.CapacitacionId).Contains(c.Id)
                                          select c.Id).FirstOrDefault();

            return View(documentosCapacitacion);
        }

        // POST: DocumentosCapacitacion/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Descripcion,NombreArchivo,CapacitacionId")] DocumentosCapacitacion documentosCapacitacion)
        {
            if (id != documentosCapacitacion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(documentosCapacitacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DocumentosCapacitacionExists(documentosCapacitacion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CapacitacionId"] = new SelectList(_context.Capacitacion, "Id", "Id", documentosCapacitacion.CapacitacionId);
            return View(documentosCapacitacion);
        }

        // GET: DocumentosCapacitacion/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var documentosCapacitacion = await _context.DocumentosCapacitacion
                .Include(d => d.Capacitacion)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (documentosCapacitacion == null)
            {
                return NotFound();
            }
            ViewData["IdCapacitacion"] = (from c in _context.Capacitacion
                        where (from dc in _context.DocumentosCapacitacion
                               where dc.Id == id
                               select dc.CapacitacionId).Contains(c.Id)
                        select c.Id).FirstOrDefault();

            return View(documentosCapacitacion);
        }

        // POST: DocumentosCapacitacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var documentosCapacitacion = await _context.DocumentosCapacitacion.FindAsync(id);
            _context.DocumentosCapacitacion.Remove(documentosCapacitacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DocumentosCapacitacionExists(int id)
        {
            return _context.DocumentosCapacitacion.Any(e => e.Id == id);
        }


        public async Task<IActionResult> DescargaArchivo(int id)
        {
            var NombreArchivo = _context.DocumentosCapacitacion
                                    .Where(x => x.Id == id)
                                    .Select(x => x.NombreArchivo).FirstOrDefault();

            var ruta = Path.Combine(@"~\Documentos", NombreArchivo);
            return File(ruta, System.Net.Mime.MediaTypeNames.Application.Octet, NombreArchivo);

        }
    }
}
