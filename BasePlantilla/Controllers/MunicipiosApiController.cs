﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MunicipiosApiController : ControllerBase
    {
        private readonly TalentoHumanoDbContext _context;

        public MunicipiosApiController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: api/MunicipiosApi
        [HttpGet]
        public ActionResult<IEnumerable<Municipio>> GetMunicipios(int? departamentoId)
        {
            var municipios = _context.Municipios.ToList();
            if (departamentoId != null)
            {
                municipios = _context.Municipios.Where(x => x.DepartamentoId == departamentoId).ToList();
            }
            return municipios;
        }

        // GET: api/MunicipiosApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Municipio>> GetMunicipio(int id)
        {
            var municipio = await _context.Municipios.Include(x => x.Departamento).FirstOrDefaultAsync(x => x.Id == id);

            if (municipio == null)
            {
                return NotFound();
            }

            return municipio;
        }

        // PUT: api/MunicipiosApi/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMunicipio(int id, Municipio municipio)
        {
            if (id != municipio.Id)
            {
                return BadRequest();
            }

            _context.Entry(municipio).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MunicipioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MunicipiosApi
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Municipio>> PostMunicipio(Municipio municipio)
        {
            _context.Municipios.Add(municipio);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMunicipio", new { id = municipio.Id }, municipio);
        }

        // DELETE: api/MunicipiosApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Municipio>> DeleteMunicipio(int id)
        {
            var municipio = await _context.Municipios.FindAsync(id);
            if (municipio == null)
            {
                return NotFound();
            }

            _context.Municipios.Remove(municipio);
            await _context.SaveChangesAsync();

            return municipio;
        }

        private bool MunicipioExists(int id)
        {
            return _context.Municipios.Any(e => e.Id == id);
        }
    }
}
