﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class PreguntasFasesController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public PreguntasFasesController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: PreguntasFases
        public async Task<IActionResult> Index()
        {
            var talentoHumanoDbContext = _context.PreguntasFases.Include(p => p.FasesEvaluaciones);
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: PreguntasFases/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preguntasFases = await _context.PreguntasFases
                .Include(p => p.FasesEvaluaciones)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (preguntasFases == null)
            {
                return NotFound();
            }

            return View(preguntasFases);
        }

        // GET: PreguntasFases/Create
        public IActionResult Create()
        {
            ViewData["FasesEvaluacionesId"] = new SelectList(_context.FasesEvaluaciones, "Id", "NombreFase");
            return View();
        }

        // POST: PreguntasFases/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Pregunta,FasesEvaluacionesId")] PreguntasFases preguntasFases)
        {
            if (ModelState.IsValid)
            {
                _context.Add(preguntasFases);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FasesEvaluacionesId"] = new SelectList(_context.FasesEvaluaciones, "Id", "NombreFase", preguntasFases.FasesEvaluacionesId);
            return View(preguntasFases);
        }

        // GET: PreguntasFases/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preguntasFases = await _context.PreguntasFases.FindAsync(id);
            if (preguntasFases == null)
            {
                return NotFound();
            }
            ViewData["FasesEvaluacionesId"] = new SelectList(_context.FasesEvaluaciones, "Id", "NombreFase", preguntasFases.FasesEvaluacionesId);
            return View(preguntasFases);
        }

        // POST: PreguntasFases/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Pregunta,FasesEvaluacionesId")] PreguntasFases preguntasFases)
        {
            if (id != preguntasFases.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(preguntasFases);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PreguntasFasesExists(preguntasFases.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FasesEvaluacionesId"] = new SelectList(_context.FasesEvaluaciones, "Id", "NombreFase", preguntasFases.FasesEvaluacionesId);
            return View(preguntasFases);
        }

        // GET: PreguntasFases/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preguntasFases = await _context.PreguntasFases
                .Include(p => p.FasesEvaluaciones)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (preguntasFases == null)
            {
                return NotFound();
            }

            return View(preguntasFases);
        }

        // POST: PreguntasFases/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var preguntasFases = await _context.PreguntasFases.FindAsync(id);
            _context.PreguntasFases.Remove(preguntasFases);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PreguntasFasesExists(int id)
        {
            return _context.PreguntasFases.Any(e => e.Id == id);
        }
    }
}
