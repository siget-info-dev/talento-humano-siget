﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class VacacionesController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public VacacionesController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Vacaciones
        public async Task<IActionResult> Index()
        {
            return View(await _context.Vacaciones.Include(x => x.Empleado).ToListAsync());
        }

        // GET: Vacaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vacaciones = await _context.Vacaciones.Include(x => x.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (vacaciones == null)
            {
                return NotFound();
            }

            return View(vacaciones);
        }

        // GET: Vacaciones/Create
        public IActionResult Create()
        {
            var empleados = _context.Empleado.ToList().Select(x => new
            {
                x.Id,
                NombreCompleto = x.PrimerNombre
                                             + " " + x.SegundoNombre
                                             + " " + x.TercerNombre
                                             + " " + x.PrimerApellido
                                             + " " + x.SegundoApellido
                                             + " " + x.ApellidoCasada
            });

            ViewData["Empleados"] = new SelectList(empleados, "Id", "NombreCompleto");

            ViewData["Periodo"] = new SelectList(Enumerable.Range(DateTime.Now.Year - 1, 2));

            return View();
        }

        // POST: Vacaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,Inicio,Fin,Periodo,Comentario,EmpleadoId")] Vacaciones vacaciones)
        {
            if (ModelState.IsValid)
            {
                vacaciones.Empleado = _context.Empleado.FirstOrDefault(x => x.Id == vacaciones.EmpleadoId);

                var fechaIngreso = _context.Empleado.Where(x => x.Id == vacaciones.EmpleadoId).Select(x => x.FechaIngreso).ToList();

                var diasDesdeIngreso = vacaciones.Inicio - Convert.ToDateTime(fechaIngreso[0]);

                var diasDeVacacion = _context.Parametros.Where(x => x.Nombre == "DiasVacaciones").Select(x => x.Valor).ToList();

                var diasPrimeraVacacion = _context.Parametros.Where(x => x.Nombre == "DiasPrimeraVacacion").Select(x => x.Valor).ToList();

                var PeridosGozados = _context.Vacaciones.Where(x => x.EmpleadoId == vacaciones.EmpleadoId && x.Periodo == vacaciones.Periodo);

                // Validacion primera vacacion, la fecha de ingreso debe de ser mayor a la fecha de ingreso mas un año.
                if (!(diasDesdeIngreso.Days > Convert.ToInt32(diasPrimeraVacacion[0])))
                {
                    ModelState.AddModelError("EmpleadoId",
                        "El Empleado seleccionado, no puede programar vacaciones para las fechas seleccionadas, por no contar con un año al momento de incio de vacaciones.");
                    llenaCampos(vacaciones);
                    return View(vacaciones);
                }

                // Validacion vacaciones menores de 18 Dias 
                if ((vacaciones.Fin-vacaciones.Inicio).Days < Convert.ToInt32(diasDeVacacion[0]))
                {
                    ModelState.AddModelError("EmpleadoId",
                        "El periodo de vacaciones no puede ser menor a " + diasDeVacacion[0] + " Dias." );
                    llenaCampos(vacaciones);
                    return View(vacaciones);
                }


                // Validacion en el mismo periodo
                if (PeridosGozados.Count()  != 0)
                {
                    ModelState.AddModelError("EmpleadoId",
                        "El periodo de vacaciones seleccionado ya fue gozado.");
                    llenaCampos(vacaciones);
                    return View(vacaciones);
                }

                _context.Add(vacaciones);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            llenaCampos(vacaciones);
            return View(vacaciones);
        }

        // GET: Vacaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vacaciones = await _context.Vacaciones.Include(x => x.Empleado).FirstOrDefaultAsync();
            
            llenaCampos(vacaciones);

            //vacaciones.IdEmpleado = vacaciones.Empleado.Id;


            if (vacaciones == null)
            {
                return NotFound();
            }
            return View(vacaciones);
        }

        private void llenaCampos(Vacaciones vacaciones)
        {

            var empleados = _context.Empleado.ToList().Select(x => new
            {
                x.Id,
                NombreCompleto = x.PrimerNombre
                                              + " " + x.SegundoNombre
                                              + " " + x.TercerNombre
                                              + " " + x.PrimerApellido
                                              + " " + x.SegundoApellido
                                              + " " + x.ApellidoCasada
            }); //.Where(x => x.Id == vacaciones.Empleado.Id);
            
            ViewData["Empleados"] = new SelectList(empleados, "Id", "NombreCompleto");
            
            if (vacaciones.Empleado != null)
            {
                ViewData["Empleados"] = new SelectList(empleados, "Id", "NombreCompleto", vacaciones.EmpleadoId);
            }
            

            ViewData["Periodo"] = new SelectList(Enumerable.Range(DateTime.Now.Year - 1, 2));

        }

        // POST: Vacaciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Inicio,Fin,Periodo,Comentario,EmpleadoId")] Vacaciones vacaciones)
        {
            if (id != vacaciones.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vacaciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VacacionesExists(vacaciones.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vacaciones);
        }

        // GET: Vacaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vacaciones = await _context.Vacaciones.Include(x => x.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (vacaciones == null)
            {
                return NotFound();
            }

            return View(vacaciones);
        }

        // POST: Vacaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vacaciones = await _context.Vacaciones.FindAsync(id);
            _context.Vacaciones.Remove(vacaciones);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VacacionesExists(int id)
        {
            return _context.Vacaciones.Any(e => e.Id == id);
        }


    }
}
