﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class AccionesDePersonalController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public AccionesDePersonalController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: AccionesDePersonal
        public async Task<IActionResult> Index()
        {
            var talentoHumanoDbContext = _context.AccionesDePersonal.Include(a => a.Acciones).Include(a => a.Empleado);
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: AccionesDePersonal/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accionesDePersonal = await _context.AccionesDePersonal
                .Include(a => a.Acciones)
                .Include(a => a.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (accionesDePersonal == null)
            {
                return NotFound();
            }

            return View(accionesDePersonal);
        }

        // GET: AccionesDePersonal/Create
        public IActionResult Create()
        {
            var empleados = _context.Empleado.ToList().Select(x => new { x.Id, nombreCompleto = x.GetNombreCompleto() });

            ViewData["AccionesId"] = new SelectList(_context.TipoAccionPersonal, "Id", "Acciones");
            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "nombreCompleto");
            return View();
        }

        // POST: AccionesDePersonal/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Numero,Comentarios,FechaAccionPersonal,EmpleadoId,AccionesId")] AccionesDePersonal accionesDePersonal)
        {
            if (ModelState.IsValid)
            {
                _context.Add(accionesDePersonal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            var empleados = _context.Empleado.ToList().Select(x => new { x.Id, nombreCompleto = x.GetNombreCompleto() });

            ViewData["AccionesId"] = new SelectList(_context.TipoAccionPersonal, "Id", "Acciones", accionesDePersonal.AccionesId);
            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "nombreCompleto", accionesDePersonal.EmpleadoId);
            return View(accionesDePersonal);
        }

        // GET: AccionesDePersonal/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accionesDePersonal = await _context.AccionesDePersonal.FindAsync(id);
            if (accionesDePersonal == null)
            {
                return NotFound();
            }

            var empleados = _context.Empleado.ToList().Select(x => new { x.Id, nombreCompleto = x.GetNombreCompleto() });

            ViewData["AccionesId"] = new SelectList(_context.TipoAccionPersonal, "Id", "Acciones", accionesDePersonal.AccionesId);
            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "nombreCompleto", accionesDePersonal.EmpleadoId);
            return View(accionesDePersonal);
        }

        // POST: AccionesDePersonal/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Numero,Comentarios,FechaAccionPersonal,EmpleadoId,AccionesId")] AccionesDePersonal accionesDePersonal)
        {
            if (id != accionesDePersonal.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(accionesDePersonal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AccionesDePersonalExists(accionesDePersonal.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            var empleados = _context.Empleado.ToList().Select(x => new { x.Id, nombreCompleto = x.GetNombreCompleto() });

            ViewData["AccionesId"] = new SelectList(_context.TipoAccionPersonal, "Id", "Acciones", accionesDePersonal.AccionesId);
            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "nombreCompleto", accionesDePersonal.EmpleadoId);
            return View(accionesDePersonal);
        }

        // GET: AccionesDePersonal/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accionesDePersonal = await _context.AccionesDePersonal
                .Include(a => a.Acciones)
                .Include(a => a.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (accionesDePersonal == null)
            {
                return NotFound();
            }

            return View(accionesDePersonal);
        }

        // POST: AccionesDePersonal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var accionesDePersonal = await _context.AccionesDePersonal.FindAsync(id);
            _context.AccionesDePersonal.Remove(accionesDePersonal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AccionesDePersonalExists(int id)
        {
            return _context.AccionesDePersonal.Any(e => e.Id == id);
        }
    }
}
