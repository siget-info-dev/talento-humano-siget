﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class EducacionEmpleadoController : Controller
    {
        private readonly TalentoHumanoDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;


        public EducacionEmpleadoController(TalentoHumanoDbContext context, IHostingEnvironment HostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = HostingEnvironment;
        }

        // GET: EducacionEmpleadoes
        public async Task<IActionResult> Index(int? id)
        {
            var talentoHumanoDbContext = _context.EducacionEmpleado
                .Include(e => e.CentroEducativo)
                .Include(e => e.Educacion)
                .Include(e => e.Empleado)
                .Where(e=> e.EmpleadoId == id);

            ViewData["CodigoEmpleado"] = id;
            ViewData["NombreEmpleado"] = _context.Empleado.ToList().Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: EducacionEmpleadoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var educacionEmpleado = await _context.EducacionEmpleado
                .Include(e => e.CentroEducativo)
                .Include(e => e.Educacion)
                .Include(e => e.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (educacionEmpleado == null)
            {
                return NotFound();
            }

            return View(educacionEmpleado);
        }

        // GET: EducacionEmpleadoes/Create
        public IActionResult Create(int? id)
        {
            ViewData["NombreEmpleado"] = _context.Empleado.Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            ViewData["EmpleadoId"] = id;
            ViewData["CentroEducativoId"] = new SelectList(_context.CentroEducativo, "Id", "Nombre");
            ViewData["EducacionId"] = new SelectList(_context.Educacion, "Id", "Nombre");
            return View();
        }

        // POST: EducacionEmpleadoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GradoAcademico,Año,EducacionId,EmpleadoId,CentroEducativoId,FFArchivo")] EducacionEmpleado educacionEmpleado)
        {

            if (ModelState.IsValid)
            {
                string NombreArchivo= "";
                if (educacionEmpleado.FFArchivo != null)
                {
                    NombreArchivo = Services.Tools.subeArchivo(educacionEmpleado.FFArchivo);
                }

                educacionEmpleado.ArchivoRespaldo = NombreArchivo;
                _context.Add(educacionEmpleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new {id = educacionEmpleado.EmpleadoId } );
            }
            ViewData["CentroEducativoId"] = new SelectList(_context.CentroEducativo, "Id", "Nombre", educacionEmpleado.CentroEducativoId);
            ViewData["EducacionId"] = new SelectList(_context.Educacion, "Id", "Nombre", educacionEmpleado.EducacionId);
            //ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", educacionEmpleado.EmpleadoId);
            return View(educacionEmpleado);
        }

        // GET: EducacionEmpleadoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var educacionEmpleado =  _context.EducacionEmpleado.Include(x=> x.Empleado).Where(x=> x.Id == id).FirstOrDefault();
            if (educacionEmpleado == null)
            {
                return NotFound();
            }
            ViewData["CentroEducativoId"] = new SelectList(_context.CentroEducativo, "Id", "Nombre",  educacionEmpleado.CentroEducativoId);
            ViewData["EducacionId"] = new SelectList(_context.Educacion, "Id", "Nombre", educacionEmpleado.EducacionId);
            ViewData["EmpleadoId"] = educacionEmpleado.EmpleadoId;
            return View(educacionEmpleado);
        }

        // POST: EducacionEmpleadoes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,GradoAcademico,Año,EducacionId,EmpleadoId,CentroEducativoId,FFArchivo")] EducacionEmpleado educacionEmpleado)
        {
            if (id != educacionEmpleado.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string NombreArchivo = "";
                    if (educacionEmpleado.FFArchivo != null)
                    {
                        var archivoActual = _context.EducacionEmpleado.Where(x => x.Id == educacionEmpleado.Id).Select(x => x.ArchivoRespaldo).FirstOrDefault();
                        NombreArchivo = Services.Tools.RemplazaArchivo(archivoActual, educacionEmpleado.FFArchivo);
                    }

                    educacionEmpleado.ArchivoRespaldo = NombreArchivo;

                    _context.Update(educacionEmpleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EducacionEmpleadoExists(educacionEmpleado.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { id = educacionEmpleado.EmpleadoId });
            }
            ViewData["CentroEducativoId"] = new SelectList(_context.CentroEducativo, "Id", "Id", educacionEmpleado.CentroEducativoId);
            ViewData["EducacionId"] = new SelectList(_context.Educacion, "Id", "Id", educacionEmpleado.EducacionId);
            ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", educacionEmpleado.EmpleadoId);
            return View(educacionEmpleado);
        }

        // GET: EducacionEmpleadoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var educacionEmpleado = await _context.EducacionEmpleado
                .Include(e => e.CentroEducativo)
                .Include(e => e.Educacion)
                .Include(e => e.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (educacionEmpleado == null)
            {
                return NotFound();
            }

            return View(educacionEmpleado);
        }

        // POST: EducacionEmpleadoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var educacionEmpleado = await _context.EducacionEmpleado.FindAsync(id);
            _context.EducacionEmpleado.Remove(educacionEmpleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { id = educacionEmpleado.EmpleadoId });
        }

        private bool EducacionEmpleadoExists(int id)
        {
            return _context.EducacionEmpleado.Any(e => e.Id == id);
        }
    }
}
