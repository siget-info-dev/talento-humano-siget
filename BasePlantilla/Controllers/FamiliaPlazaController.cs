﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class FamiliaPlazaController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public FamiliaPlazaController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: FamiliaPlaza
        public async Task<IActionResult> Index()
        {
            return View(await _context.FamiliaPlazas.Include(x=>x.TipoEvaluaciones).ToListAsync());
        }

        // GET: FamiliaPlaza/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var familiaPlazas = await _context.FamiliaPlazas.Include(x=> x.TipoEvaluaciones)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (familiaPlazas == null)
            {
                return NotFound();
            }

            return View(familiaPlazas);
        }

        // GET: FamiliaPlaza/Create
        public IActionResult Create()
        {
            ViewData["TipoEvaluacionesId"] = new SelectList( _context.TipoEvaluaciones, "Id", "Nombre");
            return View();
        }

        // POST: FamiliaPlaza/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,TipoEvaluacionesId")] FamiliaPlazas familiaPlazas)
        {
            if (ModelState.IsValid)
            {
                _context.Add(familiaPlazas);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["TipoEvaluacionesId"] = new SelectList(_context.TipoEvaluaciones, "Id", "Nombre");
            return View(familiaPlazas);
        }

        // GET: FamiliaPlaza/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var familiaPlazas = await _context.FamiliaPlazas.FindAsync(id);
            if (familiaPlazas == null)
            {
                return NotFound();
            }
            ViewData["TipoEvaluacionesId"] = new SelectList(_context.TipoEvaluaciones, "Id", "Nombre");
            return View(familiaPlazas);
        }

        // POST: FamiliaPlaza/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,TipoEvaluacionesId")] FamiliaPlazas familiaPlazas)
        {
            if (id != familiaPlazas.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(familiaPlazas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FamiliaPlazasExists(familiaPlazas.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(familiaPlazas);
        }

        // GET: FamiliaPlaza/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var familiaPlazas = await _context.FamiliaPlazas.Include(x => x.TipoEvaluaciones)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (familiaPlazas == null)
            {
                return NotFound();
            }

            return View(familiaPlazas);
        }

        // POST: FamiliaPlaza/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var familiaPlazas = await _context.FamiliaPlazas.FindAsync(id);
            _context.FamiliaPlazas.Remove(familiaPlazas);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FamiliaPlazasExists(int id)
        {
            return _context.FamiliaPlazas.Any(e => e.Id == id);
        }
    }
}
