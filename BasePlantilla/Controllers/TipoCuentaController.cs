﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class TipoCuentaController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public TipoCuentaController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: TipoCuenta
        public async Task<IActionResult> Index()
        {
            return View(await _context.TiposCuentas.ToListAsync());
        }

        // GET: TipoCuenta/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoCuenta = await _context.TiposCuentas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoCuenta == null)
            {
                return NotFound();
            }

            return View(tipoCuenta);
        }

        // GET: TipoCuenta/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoCuenta/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre")] TipoCuenta tipoCuenta)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoCuenta);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoCuenta);
        }

        // GET: TipoCuenta/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoCuenta = await _context.TiposCuentas.FindAsync(id);
            if (tipoCuenta == null)
            {
                return NotFound();
            }
            return View(tipoCuenta);
        }

        // POST: TipoCuenta/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] TipoCuenta tipoCuenta)
        {
            if (id != tipoCuenta.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoCuenta);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoCuentaExists(tipoCuenta.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoCuenta);
        }

        // GET: TipoCuenta/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoCuenta = await _context.TiposCuentas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoCuenta == null)
            {
                return NotFound();
            }

            return View(tipoCuenta);
        }

        // POST: TipoCuenta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoCuenta = await _context.TiposCuentas.FindAsync(id);
            _context.TiposCuentas.Remove(tipoCuenta);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoCuentaExists(int id)
        {
            return _context.TiposCuentas.Any(e => e.Id == id);
        }
    }
}
