﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class EducacionController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public EducacionController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Educacion
        public async Task<IActionResult> Index()
        {
            return View(await _context.Educacion.ToListAsync());
        }

        // GET: Educacion/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var educacion = await _context.Educacion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (educacion == null)
            {
                return NotFound();
            }

            return View(educacion);
        }

        // GET: Educacion/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Educacion/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre")] Educacion educacion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(educacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(educacion);
        }

        // GET: Educacion/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var educacion = await _context.Educacion.FindAsync(id);
            if (educacion == null)
            {
                return NotFound();
            }
            return View(educacion);
        }

        // POST: Educacion/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] Educacion educacion)
        {
            if (id != educacion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(educacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EducacionExists(educacion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(educacion);
        }

        // GET: Educacion/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var educacion = await _context.Educacion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (educacion == null)
            {
                return NotFound();
            }

            return View(educacion);
        }

        // POST: Educacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var educacion = await _context.Educacion.FindAsync(id);
            _context.Educacion.Remove(educacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EducacionExists(int id)
        {
            return _context.Educacion.Any(e => e.Id == id);
        }
    }
}
