﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class GerenciasController : Controller
    {
        private readonly IGerenciaRepository _gerenciaRepository;

        public GerenciasController(IGerenciaRepository gerenciaRepository)
        {
            _gerenciaRepository = gerenciaRepository;
        }

        // GET: Gerencia
        public IActionResult Index(int? tipoAlerta)
        {
            ViewBag.TipoAlerta = tipoAlerta;
            return View(_gerenciaRepository.GetAll());
        }

        // GET: Gerencia/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var gerencia = _gerenciaRepository.Get(id);

            if (gerencia == null)
            {
                return NotFound();
            }

            return View(gerencia);
        }

        // GET: Gerencia/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Gerencia/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Codigo,Nombre")] Gerencias gerencia)
        {
            if (ModelState.IsValid)
            {
                _gerenciaRepository.Create(gerencia);

                if (!_gerenciaRepository.Save())
                {
                    throw new Exception($"Error al guardar el objeto");
                }

                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }
            return View(gerencia);
        }

        // GET: Gerencia/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var gerencia = _gerenciaRepository.Get(id);

            if (gerencia == null)
            {
                return NotFound();
            }

            return View(gerencia);
        }

        // POST: Gerencia/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Codigo,Nombre")] Gerencias gerencia)
        {
            if (id != gerencia.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _gerenciaRepository.Update(gerencia);

                if (!_gerenciaRepository.Save())
                {
                    throw new Exception($"Error al actualizar el objeto con id: {id}.");
                }

                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }
            return View(gerencia);
        }

        // GET: Gerencia/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var gerencia = _gerenciaRepository.Get(id);

            if (gerencia == null)
            {
                return NotFound();
            }

            return View(gerencia);
        }

        // POST: Gerencia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var gerencia = _gerenciaRepository.Get(id);
            _gerenciaRepository.Delete(gerencia);

            if (!_gerenciaRepository.Save())
            {
                throw new Exception($"Error al eliminar el objeto con id: {id}.");
            }

            return RedirectToAction(nameof(Index), new { tipoAlerta = 2 });
        }
    }
}
