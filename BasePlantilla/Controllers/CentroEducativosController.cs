﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class CentroEducativosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public CentroEducativosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: CentroEducativos
        public async Task<IActionResult> Index()
        {
            return View(await _context.CentroEducativo.ToListAsync());
        }

        // GET: CentroEducativos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var centroEducativo = await _context.CentroEducativo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (centroEducativo == null)
            {
                return NotFound();
            }

            return View(centroEducativo);
        }

        // GET: CentroEducativos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CentroEducativos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre")] CentroEducativo centroEducativo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(centroEducativo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(centroEducativo);
        }

        // GET: CentroEducativos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var centroEducativo = await _context.CentroEducativo.FindAsync(id);
            if (centroEducativo == null)
            {
                return NotFound();
            }
            return View(centroEducativo);
        }

        // POST: CentroEducativos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] CentroEducativo centroEducativo)
        {
            if (id != centroEducativo.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(centroEducativo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CentroEducativoExists(centroEducativo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(centroEducativo);
        }

        // GET: CentroEducativos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var centroEducativo = await _context.CentroEducativo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (centroEducativo == null)
            {
                return NotFound();
            }

            return View(centroEducativo);
        }

        // POST: CentroEducativos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var centroEducativo = await _context.CentroEducativo.FindAsync(id);
            _context.CentroEducativo.Remove(centroEducativo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CentroEducativoExists(int id)
        {
            return _context.CentroEducativo.Any(e => e.Id == id);
        }
    }
}
