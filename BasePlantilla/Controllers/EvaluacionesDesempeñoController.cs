﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class EvaluacionesDesempeñoController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public EvaluacionesDesempeñoController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: EvaluacionesDesempeño
        public async Task<IActionResult> Index(int id)
        {
            var talentoHumanoDbContext = _context.EvaluacionesDesempeños
                .Where(x => x.EmpleadoId == id)
                .GroupBy(x => x.PeriodoEvaluacion)
                .Select(x => new EvaluacionesDesempeño
                {
                    EmpleadoId = id,
                    PeriodoEvaluacion = x.Key,
                    Calificacion = x.Sum(x => x.Calificacion)
                }).ToList();

            return View(talentoHumanoDbContext);
        }

        // GET: EvaluacionesDesempeño/Details/5
        public async Task<IActionResult> Details(int EmpleadoId, string Periodo)
        {
            var evaluacionesDesempeño = await _context.EvaluacionesDesempeños
                .Include(x => x.PreguntasFases)
                    .ThenInclude(x=>x.FasesEvaluaciones)
                        .ThenInclude(x=>x.TipoEvaluaciones)
                        .Include(e => e.Empleado)
                            .ThenInclude(x => x.EmpleadoPlaza)
                                .ThenInclude(x=>x.Plaza)
                                    .ThenInclude(x=>x.FamiliaPlazas)
                .Where(m => m.EmpleadoId == EmpleadoId && m.PeriodoEvaluacion == Periodo).ToListAsync();

            //var evaluacionesDesempeño2 = await _context.EvaluacionesDesempeños
            //    .Include(e => e.Empleado)
            //    .Include(e => e.PreguntasFases)
            //    .FirstOrDefaultAsync(m => m.EmpleadoId == EmpleadoId && m.PeriodoEvaluacion == Periodo);
            if (evaluacionesDesempeño == null)
            {
                return NotFound();
            }

            return View(evaluacionesDesempeño);
        }

        // GET: EvaluacionesDesempeño/Create
        public IActionResult Create()
        {
            //var result = _context.EvaluacionesDesempeños.Include(x => x.PreguntasFases).ThenInclude(x => x.FasesEvaluaciones).ThenInclude(x => x.TipoEvaluaciones);

            var result = _context.PreguntasFases.Include(x => x.FasesEvaluaciones).ThenInclude(x => x.TipoEvaluaciones);

            List<SelectListItem> lista = new List<SelectListItem>() {
                new SelectListItem{ Text="1", Value="1"},
                new SelectListItem{ Text="2", Value="2"},
                new SelectListItem{ Text="3", Value="3"},
                new SelectListItem{ Text="4", Value="4"},
                new SelectListItem{ Text="5", Value="5"}};

            ViewData["ListaCalificacion"] = lista;

            ViewData["PreguntasFasesId"] = new SelectList(_context.PreguntasFases, "Id", "Pregunta");
            return View(result);
        }

        // POST: EvaluacionesDesempeño/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IEnumerable<EvaluacionesDesempeño> evaluacionesDesempeño)
            //[Bind("Id,PeriodoEvaluacion,Calificacion,PreguntasFasesId,EmpleadoId")] EvaluacionesDesempeño evaluacionesDesempeño)
        {
            var preguntas = _context.PreguntasFases;

            foreach (var pregunta in preguntas) {
                EvaluacionesDesempeño evaluacion = new EvaluacionesDesempeño();

                evaluacion.PeriodoEvaluacion = "2020";
                evaluacion.Calificacion = 4;
                evaluacion.PreguntasFasesId = pregunta.Id;
                evaluacion.EmpleadoId = 13;
                _context.Add(evaluacion);

            }
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));


            //if (ModelState.IsValid)
            //{
            //    _context.Add(evaluacionesDesempeño);
            //     _context.SaveChangesAsync();
            //    return RedirectToAction(nameof(Index));
            //}
            //ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", evaluacionesDesempeño.EmpleadoId);
            //ViewData["PreguntasFasesId"] = new SelectList(_context.PreguntasFases, "Id", "Pregunta", evaluacionesDesempeño.PreguntasFasesId);
           // return View(evaluacionesDesempeño);
        }

        // GET: EvaluacionesDesempeño/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluacionesDesempeño = await _context.EvaluacionesDesempeños.FindAsync(id);
            if (evaluacionesDesempeño == null)
            {
                return NotFound();
            }
            ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", evaluacionesDesempeño.EmpleadoId);
            ViewData["PreguntasFasesId"] = new SelectList(_context.PreguntasFases, "Id", "Pregunta", evaluacionesDesempeño.PreguntasFasesId);
            return View(evaluacionesDesempeño);
        }

        // POST: EvaluacionesDesempeño/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,PeriodoEvaluacion,Calificacion,PreguntasFasesId,EmpleadoId")] EvaluacionesDesempeño evaluacionesDesempeño)
        {
            if (id != evaluacionesDesempeño.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(evaluacionesDesempeño);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EvaluacionesDesempeñoExists(evaluacionesDesempeño.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", evaluacionesDesempeño.EmpleadoId);
            ViewData["PreguntasFasesId"] = new SelectList(_context.PreguntasFases, "Id", "Pregunta", evaluacionesDesempeño.PreguntasFasesId);
            return View(evaluacionesDesempeño);
        }

        // GET: EvaluacionesDesempeño/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluacionesDesempeño = await _context.EvaluacionesDesempeños
                .Include(e => e.Empleado)
                .Include(e => e.PreguntasFases)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (evaluacionesDesempeño == null)
            {
                return NotFound();
            }

            return View(evaluacionesDesempeño);
        }

        // POST: EvaluacionesDesempeño/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var evaluacionesDesempeño = await _context.EvaluacionesDesempeños.FindAsync(id);
            _context.EvaluacionesDesempeños.Remove(evaluacionesDesempeño);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EvaluacionesDesempeñoExists(int id)
        {
            return _context.EvaluacionesDesempeños.Any(e => e.Id == id);
        }
    }
}
