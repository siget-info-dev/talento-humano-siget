﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class GrupoFamiliarEmpleadosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public GrupoFamiliarEmpleadosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: GrupoFamiliarEmpleados
        public async Task<IActionResult> Index(int? id)
        {
            ViewData["CodigoEmpleado"] = id;
            ViewData["NombreEmpleado"] = _context.Empleado.ToList().Where(x => x.Id == id)
                .Select(x => x.GetNombreCompleto()).FirstOrDefault();

            var talentoHumanoDbContext = _context.GrupoFamiliarEmpleado
                .Include(g => g.Empleado)
                .Include(g => g.MiembrosFamilia)
                .Where(i => i.EmpleadoId == id);
            
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: GrupoFamiliarEmpleados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grupoFamiliarEmpleado = await _context.GrupoFamiliarEmpleado
                .Include(g => g.Empleado)
                .Include(g => g.MiembrosFamilia)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (grupoFamiliarEmpleado == null)
            {
                return NotFound();
            }

            return View(grupoFamiliarEmpleado);
        }

        // GET: GrupoFamiliarEmpleados/Create
        public IActionResult Create(int id)
        {
            ViewData["NombreEmpleado"] = _context.Empleado.Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            ViewData["EmpleadoId"] = id;
            ViewData["MiembrosFamiliaId"] = new SelectList(_context.MiembrosFamilia, "Id", "Miembro");
            return View();
        }

        // POST: GrupoFamiliarEmpleados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,FechaNacimiento,MiembrosFamiliaId,EmpleadoId")] GrupoFamiliarEmpleado grupoFamiliarEmpleado)
        {
            if (ModelState.IsValid)
            {
                _context.Add(grupoFamiliarEmpleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = grupoFamiliarEmpleado.EmpleadoId });
            }

            return View(grupoFamiliarEmpleado);
        }

        // GET: GrupoFamiliarEmpleados/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grupoFamiliarEmpleado = await _context.GrupoFamiliarEmpleado
                        .Include(x => x.Empleado)
                        .Where(x => x.Id == id).FirstOrDefaultAsync();

            if (grupoFamiliarEmpleado == null)
            {
                return NotFound();
            }
            ViewData["MiembrosFamiliaId"] = new SelectList(_context.MiembrosFamilia, "Id", "Miembro", grupoFamiliarEmpleado.MiembrosFamiliaId);
            return View(grupoFamiliarEmpleado);
        }

        // POST: GrupoFamiliarEmpleados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,FechaNacimiento,MiembrosFamiliaId,EmpleadoId")] GrupoFamiliarEmpleado grupoFamiliarEmpleado)
        {
            if (id != grupoFamiliarEmpleado.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(grupoFamiliarEmpleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GrupoFamiliarEmpleadoExists(grupoFamiliarEmpleado.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { id = grupoFamiliarEmpleado.EmpleadoId });
            }
            //ViewData["MiembrosFamiliaId"] = new SelectList(_context.MiembrosFamilia, "Id", "Id", grupoFamiliarEmpleado.MiembrosFamiliaId);
            return View(grupoFamiliarEmpleado);
        }

        // GET: GrupoFamiliarEmpleados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grupoFamiliarEmpleado = await _context.GrupoFamiliarEmpleado
                .Include(g => g.Empleado)
                .Include(g => g.MiembrosFamilia)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (grupoFamiliarEmpleado == null)
            {
                return NotFound();
            }

            return View(grupoFamiliarEmpleado);
        }

        // POST: GrupoFamiliarEmpleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var grupoFamiliarEmpleado = await _context.GrupoFamiliarEmpleado.FindAsync(id);
            _context.GrupoFamiliarEmpleado.Remove(grupoFamiliarEmpleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { id = grupoFamiliarEmpleado.EmpleadoId});
        }

        private bool GrupoFamiliarEmpleadoExists(int id)
        {
            return _context.GrupoFamiliarEmpleado.Any(e => e.Id == id);
        }
    }
}
