﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class DescuentosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public DescuentosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Descuentos
        public async Task<IActionResult> Index()
        {
            return View(await _context.Descuentos.ToListAsync());
        }

        // GET: Descuentos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var descuentos =  _context.DescuentosDetalle.Include(x=>x.Empleado).Where(m => m.DescuentosId == id);
            var descuento = _context.Descuentos.Where(x => x.Id == id).FirstOrDefault();
            ViewData["DatosDescuento"] = descuento.Nombre + " por $" + descuento.Valor;
            ViewData["IdDescuento"] = id;

            if (descuentos == null)
            {
                return NotFound();
            }

            return View(descuentos);
        }

        // GET: Descuentos/DetalleEmpleado/5
        public async Task<IActionResult> DetalleEmpleado(int id)
        {
            var descuentos = _context.DescuentosDetalle.Include(x => x.Descuentos).Where(m => m.EmpleadoId == id);

            if (descuentos == null)
            {
                return NotFound();
            }

            ViewData["idEmpleado"] = id;
            ViewData["nombreEmpleado"] = _context.Empleado.Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();

            return View(descuentos);
        }

        // GET: Descuentos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Descuentos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,Valor")] Descuentos descuentos)
        {
            descuentos.DescuentoAplicado = false;
            if (ModelState.IsValid)
            {
                _context.Add(descuentos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(descuentos);
        }

        // GET: Descuentos/CreateEmpleado
        public IActionResult CreateEmpleado(int id)
        {
            ViewData["idEmpleado"] = id;
            ViewData["nombreEmpleado"] = _context.Empleado.Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();

            return View();
        }

        // POST: Descuentos/CreateEmpleado
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateEmpleado([Bind("Id,Nombre,Valor")] Descuentos descuentos)
        {
            descuentos.DescuentoAplicado = true;
            var empleadoId = descuentos.Id;
            if (ModelState.IsValid)
            {
                descuentos.Id = 0;
                _context.Add(descuentos);
                await _context.SaveChangesAsync();

                DescuentosDetalle detalle = new DescuentosDetalle
                {
                    Fecha = DateTime.Now,
                    DescuentosId = descuentos.Id,
                    EmpleadoId = empleadoId
                };

                _context.Add(detalle);

                await _context.SaveChangesAsync();
                return RedirectToAction("DetalleEmpleado", "Descuentos", new { id =  empleadoId });

            }

            return RedirectToAction("DetalleEmpleado", "Descuentos", new { id =  empleadoId });
        }


        // GET: Descuentos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var descuentos = await _context.Descuentos.FindAsync(id);
            if (descuentos == null)
            {
                return NotFound();
            }
            return View(descuentos);
        }

        // POST: Descuentos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,Valor")] Descuentos descuentos)
        {
            if (id != descuentos.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(descuentos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DescuentosExists(descuentos.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(descuentos);
        }

        // GET: Descuentos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var descuentos = await _context.Descuentos.FirstOrDefaultAsync(m => m.Id == id);
            if (descuentos == null)
            {
                return NotFound();
            }

            return View(descuentos);
        }

        // POST: Descuentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var descuentos = await _context.Descuentos.FindAsync(id);
            _context.Descuentos.Remove(descuentos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DescuentosExists(int id)
        {
            return _context.Descuentos.Any(e => e.Id == id);
        }

        public IActionResult GenerarListado(int DescuentosId)
        {
            var Descuentos = _context.Descuentos.Where(x => x.Id == DescuentosId).Where(x => x.DescuentoAplicado == false).FirstOrDefault();

            if (Descuentos == null){
                return RedirectToAction(nameof(Index));
            }

            IQueryable<Empleado> empleados = _context.Empleado.Where(x => x.Activo == true);

            foreach (var empleado in empleados)
            {
                DescuentosDetalle detalle = new DescuentosDetalle
                {
                    Fecha = DateTime.Now,
                    EmpleadoId = empleado.Id,
                    DescuentosId = Descuentos.Id
                };
                _context.Add(detalle);
            }

            Descuentos.DescuentoAplicado = true;

            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        public IActionResult AgregarEmpleado(int Id)
        {
            var listaEmpleadosDescuento = _context.DescuentosDetalle.Include(x=>x.Empleado).Where(x => x.DescuentosId == Id).Select(x=>x.EmpleadoId);

            var ListaEmpleados = _context.Empleado.Where(x => !listaEmpleadosDescuento.Any(y => x.Id == y)).ToList().Select(x => new {
                x.Id,
                NombreCompleto = x.PrimerNombre
                                 + " " + x.SegundoNombre
                                 + " " + x.TercerNombre
                                 + " " + x.PrimerApellido
                                 + " " + x.SegundoApellido
                                 + " " + x.ApellidoCasada
            }).OrderBy(x => x.NombreCompleto); 

            ViewData["IdDescuentos"] = Id;

            ViewData["ListadoEmpleados"] = new SelectList(ListaEmpleados, "Id", "NombreCompleto");
            return View();
        }

        [HttpPost]
        public IActionResult AgregarEmpleado(int id, [Bind("EmpleadoId")] DescuentosDetalle detalle)
        {
            detalle.Fecha = DateTime.Now;
            detalle.DescuentosId = id;
            if (ModelState.IsValid)
            {
                _context.Add(detalle);
                _context.SaveChangesAsync();
                return RedirectToAction("Index", "Descuentos");
            }
            return View(detalle);
        }
    }
}
