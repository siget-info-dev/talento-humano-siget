﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class TipoPermisosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public TipoPermisosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: TipoPermisos
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoPermisos.ToListAsync());
        }

        // GET: TipoPermisos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPermiso = await _context.TipoPermisos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoPermiso == null)
            {
                return NotFound();
            }

            return View(tipoPermiso);
        }

        // GET: TipoPermisos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoPermisos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Permiso,Estado")] TipoPermiso tipoPermiso)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoPermiso);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoPermiso);
        }

        // GET: TipoPermisos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPermiso = await _context.TipoPermisos.FindAsync(id);
            if (tipoPermiso == null)
            {
                return NotFound();
            }
            return View(tipoPermiso);
        }

        // POST: TipoPermisos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Permiso,Estado")] TipoPermiso tipoPermiso)
        {
            if (id != tipoPermiso.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoPermiso);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoPermisoExists(tipoPermiso.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoPermiso);
        }

        // GET: TipoPermisos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPermiso = await _context.TipoPermisos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoPermiso == null)
            {
                return NotFound();
            }

            return View(tipoPermiso);
        }

        // POST: TipoPermisos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoPermiso = await _context.TipoPermisos.FindAsync(id);
            _context.TipoPermisos.Remove(tipoPermiso);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoPermisoExists(int id)
        {
            return _context.TipoPermisos.Any(e => e.Id == id);
        }
    }
}
