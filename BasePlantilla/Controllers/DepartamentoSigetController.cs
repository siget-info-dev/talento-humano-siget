﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class DepartamentoSigetController : Controller
    {
        private readonly IDepartamentoSigetRepository _departamentoSigetRepository;
        private readonly IGerenciaRepository _gerenciaRepository;

        public DepartamentoSigetController(
            IDepartamentoSigetRepository DepartamentoSigetRepository,
            IGerenciaRepository gerenciaRepository)
        {
            _departamentoSigetRepository = DepartamentoSigetRepository;
            _gerenciaRepository = gerenciaRepository;
        }

        // GET: departamentosSiget
        public IActionResult Index()//int? tipoAlerta)
        {
            //ViewBag.TipoAlerta = tipoAlerta;
            return View(_departamentoSigetRepository.GetAll());
        }

        // GET: departamentosSiget/Details/5
        public IActionResult Details(int id)
        {
            var DepartamentoSiget = _departamentoSigetRepository.Get(id);

            if (DepartamentoSiget == null)
            {
                return NotFound();
            }

            return View(DepartamentoSiget);
        }

        // GET: departamentosSiget/Create
        public IActionResult Create()
        {
            LoadViewData();
            return View();
        }

        // POST: departamentosSiget/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Codigo,Nombre,DepartamentoSigetId")] DepartamentoSiget departamentoSiget)
        {
            if (ModelState.IsValid)
            {
                _departamentoSigetRepository.Create(departamentoSiget);

                if (!_departamentoSigetRepository.Save())
                {
                    throw new Exception($"Error al guardar el objeto");
                }

                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }
            LoadViewData();
            return View(departamentoSiget);
        }

        // GET: departamentosSiget/Edit/5
        public IActionResult Edit(int id)
        {
            var departamentoSiget = _departamentoSigetRepository.Get(id);

            if (departamentoSiget == null)
            {
                return NotFound();
            }

            LoadViewData();
            return View(departamentoSiget);
        }

        // POST: departamentosSiget/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Codigo,Nombre,DepartamentoSigetId")] DepartamentoSiget departamentoSiget)
        {
            if (id != departamentoSiget.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _departamentoSigetRepository.Update(departamentoSiget);

                if (!_departamentoSigetRepository.Save())
                {
                    throw new Exception($"Error al actualizar el objeto con id: {id}.");
                }

                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }

            LoadViewData();
            return View(departamentoSiget);
        }

        // GET: departamentosSiget/Delete/5
        public IActionResult Delete(int id)
        {
            var departamentoSiget = _departamentoSigetRepository.Get(id);

            if (departamentoSiget == null)
            {
                return NotFound();
            }

            return View(departamentoSiget);
        }

        // POST: departamentosSiget/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var departamentoSiget = _departamentoSigetRepository.Get(id);
            _departamentoSigetRepository.Delete(departamentoSiget);

            if (!_departamentoSigetRepository.Save())
            {
                throw new Exception($"Error al eliminar el objeto con id: {id}.");
            }

            return RedirectToAction(nameof(Index), new { tipoAlerta = 2 });
        }

        private void LoadViewData()
        {
            ViewData["GerenciasSiget"] =
                new SelectList(_gerenciaRepository.GetAll(), "Id", nameof(Gerencias.Nombre));
        }
    }
}
