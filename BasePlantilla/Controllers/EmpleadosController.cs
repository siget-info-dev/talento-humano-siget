﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;
using SistemaGestionTalentoHumano.ViewModels;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class EmpleadosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;
        private readonly IEmpleadoRepository _empleadoRepository;

        private readonly IMapper _mapper;

        public EmpleadosController(
            TalentoHumanoDbContext context,
            IEmpleadoRepository empleadoRepository,
            IMapper mapper)
        {
            _context = context;
            _empleadoRepository = empleadoRepository;
            _mapper = mapper;
        }

        // GET: Empleados
        public IActionResult Index()
        {
            var empleados = _empleadoRepository
                .GetAll();

            IList<EmpleadoViewModel> empleadosViewModel =
                empleados.Select(empleado => _mapper.Map<EmpleadoViewModel>(empleado)).ToList();

            return View(empleadosViewModel);
        }

        // GET: Empleados/Details/5
        public IActionResult Details(int id)
        {
            var empleado = _empleadoRepository.Get(id);

            if (empleado == null)
            {
                return NotFound();
            }

            var empleadoViewModel = _mapper.Map<EmpleadoViewModel>(empleado);

            return View(empleadoViewModel);
        }

        // GET: Empleados/Create
        public IActionResult Create()
        {
            LoadViewData();
            return View();
        }

        // POST: Empleados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EmpleadoViewModel empleadoViewModel)
        {
            if (ModelState.IsValid)
            {
                if (!_empleadoRepository.Save())
                {
                    throw new Exception($"Error al guardar el objeto");
                }

                var empleado = _mapper.Map<Empleado>(empleadoViewModel);
                _empleadoRepository.Create(empleado);
                _empleadoRepository.Save();

                //Se coloca como codigo de empleado el id del registro, 
                empleado.CodigoEmpleado = empleado.Id;
                _context.Update(empleado);
                _context.SaveChangesAsync();

                //Guardar la foto del empleado con el codigo como nombre 
                if (empleado.RutaImagen != null) {
                    empleado.RutaImagen = empleado.Id + Path.GetExtension(empleadoViewModel.FFFotoEmpleado.FileName);
                    Tools.subeFotoEmpleado(empleado.Id, empleadoViewModel.FFFotoEmpleado);
                }

                return RedirectToAction(nameof(Index));
            }
            LoadViewData(empleadoViewModel);
            return View(empleadoViewModel);
        }

        // GET: Empleados/Edit/5
        public IActionResult Edit(int id)
        {
            var empleado = _empleadoRepository.Get(id);

            if (empleado == null)
            {
                return NotFound();
            }

            var empleadoViewModel = _mapper.Map<EmpleadoViewModel>(empleado);

            empleadoViewModel.DepartamentoId = empleadoViewModel.Municipio.DepartamentoId;

            LoadViewData(empleadoViewModel);
            return View(empleadoViewModel);
        }

        // POST: Empleados/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EmpleadoViewModel empleadoViewModel)
        //[Bind("Id,PrimerNombreDui,SegundoNombreDui,TercerNombreDui,PrimerApellidoDui,SegundoApellidoDui," +
        //"ApellidoCasadaDui,ConocidoPorDui,Sexo,EstadoCivilId,TipoSangreId,TelefonoFijo,TelefonoMovil,EmailPersonal,Dui,Nit,Isss,FondoPensionId,Nup," +
        //"PoseeLicencia,Pasaporte,MunicipioId,FechaNacimiento,DireccionResidencia,NombreContactoEmergencia,TelefonoEmergencia,DireccionEmergencia," +
        //"ObservacionesEmergencia,CodigoEmpleado,RutaImagen,UserNameSiget,EmailSiget,TelefonoInstitucional,ExtensionInstitucional,FechaIngreso,FechaRetiro," +
        //"Activo,Jubilado,Marcacion,HorasTrabajo,Observaciones,FormaPagoId,BancoId,NumeroCuenta,TipoCuentaId")] Empleado Empleado)
        {
            if (id != empleadoViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var image = _context.Empleado.Where(x => x.Id == id).Select(x => x.RutaImagen);

                    if (empleadoViewModel.RutaImagen != null) { 
                    }

                    var empleado = _mapper.Map<Empleado>(empleadoViewModel);
                    _context.Update(empleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmpleadoExists(empleadoViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BancoId"] = new SelectList(_context.Bancos, "Id", "Codigo", empleadoViewModel.BancoId);
            ViewData["EstadoCivilId"] = new SelectList(_context.EstadosCivil, "Id", "Codigo", empleadoViewModel.EstadoCivilId);
            ViewData["FondoPensionId"] = new SelectList(_context.FondosPension, "Id", "Codigo", empleadoViewModel.FondoPensionId);
            ViewData["FormaPagoId"] = new SelectList(_context.Set<FormaPago>(), "Id", "Codigo", empleadoViewModel.FormaPagoId);
            ViewData["MunicipioId"] = new SelectList(_context.Municipios, "Id", "Nombre", empleadoViewModel.MunicipioId);
            ViewData["TipoCuentaId"] = new SelectList(_context.TiposCuentas, "Id", "Codigo", empleadoViewModel.TipoCuentaId);
            ViewData["TipoSangreId"] = new SelectList(_context.TiposSangre, "Id", "Nombre", empleadoViewModel.TipoSangreId);
            return View(empleadoViewModel);
        }

        // GET: Empleados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empleado = await _context.Empleado
                .Include(e => e.Banco)
                .Include(e => e.EstadoCivil)
                .Include(e => e.FondoPension)
                .Include(e => e.FormaPago)
                .Include(e => e.Municipio)
                .Include(e => e.TipoCuenta)
                .Include(e => e.TipoSangre)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (empleado == null)
            {
                return NotFound();
            }

            return View(empleado);
        }

        // POST: Empleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var empleado = await _context.Empleado.FindAsync(id);
            _context.Empleado.Remove(empleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmpleadoExists(int id)
        {
            return _context.Empleado.Any(e => e.Id == id);
        }

        private void LoadViewData(EmpleadoViewModel empleadoViewModel = null)
        {
            ViewData["Bancos"] = new SelectList(_context.Bancos, "Id", "Nombre");
            ViewData["EstadosCivil"] = new SelectList(_context.EstadosCivil, "Id", "Nombre");
            ViewData["FondosPension"] = new SelectList(_context.FondosPension, "Id", "Nombre");
            ViewData["FormasPago"] = new SelectList(_context.Set<FormaPago>(), "Id", "Nombre");
            ViewData["Departamentos"] = new SelectList(_context.Departamentos, "Id", "Nombre");
            ViewData["TiposCuenta"] = new SelectList(_context.TiposCuentas, "Id", "Nombre");
            ViewData["TiposSangre"] = new SelectList(_context.TiposSangre, "Id", "Nombre");
            ViewData["Sexos"] = new SelectList(_context.Sexo, "Id", "Nombre");
            ViewData["TiposIdiomas"] = new SelectList(_context.Idiomas, "Id", "Idioma");
            ViewData["Familiares"] = new SelectList(_context.MiembrosFamilia, "Id", "Miembro");
            ViewData["Nacionalidades"] = new SelectList(_context.Nacionalidades, "Id", "Nombre");
            ViewData["ProfesionOficio"] = new SelectList(_context.ProfesionOficio, "Id", "Nombre");
            ViewData["CentroEducativo"] = new SelectList(_context.CentroEducativo, "Id", "Nombre");
            ViewData["NivelEducacion"] = new SelectList(_context.Educacion, "Id", "Nombre");

            if (empleadoViewModel != null)
            {
                var municipios = _context.Municipios.Where(x => x.DepartamentoId == empleadoViewModel.DepartamentoId);
                ViewData["Municipios"] = new SelectList(municipios, "Id", "Nombre");
            }
        }

        public ActionResult<IEnumerable<Idiomas>> GetIdioma(int? id)
        {
            var nombreIdioma = _context.Idiomas.ToList();
            if (id != null) { nombreIdioma = _context.Idiomas.Where(x => x.Id == id).ToList(); }
            return nombreIdioma;
        }

        public ActionResult<IEnumerable<MiembrosFamilia>> GetFamilia(int? id)
        {
            var NombreFamiliar = _context.MiembrosFamilia.ToList();
            if (id != null) { NombreFamiliar = _context.MiembrosFamilia.Where(x => x.Id == id).ToList(); }
            return NombreFamiliar;
        }

        public ActionResult<IEnumerable<CentroEducativo>> GetCentroEducativo(int? id)
        {
            var centroEducativo = _context.CentroEducativo.ToList();
            if (id != null) { centroEducativo = _context.CentroEducativo.Where(x => x.Id == id).ToList(); }
            return centroEducativo;
        }

        public ActionResult<IEnumerable<Educacion>> GetNivelEducacion(int? id)
        {
            var NivelEducativo = _context.Educacion.ToList();
            if (id != null) { NivelEducativo = _context.Educacion.Where(x => x.Id == id).ToList(); }
            return NivelEducativo;
        }

        public IActionResult NivelEducacion(int id) {
            return RedirectToAction("index", "EducacionEmpleado", new { id });
        }

        public IActionResult IdiomaEducacion(int id) {
            return RedirectToAction("index", "IdiomasEmpleados", new { id });
        }

        public IActionResult GrupoFamiliarEmpleados(int id)
        {
            return RedirectToAction("index", "GrupoFamiliarEmpleados", new { id });
        }

        public IActionResult ReferenciasPersonales(int id) {
            return RedirectToAction("index", "ReferenciasPersonales", new { id });
        }

        public IActionResult DocumentoEmpleados(int id) {
            return RedirectToAction("index", "DocumentoEmpleados", new { id });
        }

        public IActionResult PrestacionEmpleados(int id)
        {
            return RedirectToAction("index", "PrestacionEmpleados", new { id });
        }

        public IActionResult EvaluacionesDesempeño(int id) 
        {
            return RedirectToAction("index", "EvaluacionesDesempeño", new { id });
        }

        public IActionResult DescuentoEmpleado(int Id) {
            return RedirectToAction("DetalleEmpleado", "Descuentos", new { Id });
        }

        public IActionResult AumentoEmpleado(int Id)
        {
            return RedirectToAction("DetalleEmpleado", "Aumentos", new { Id });
        }
    }
}
