﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class AumentosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public AumentosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Aumentos
        public async Task<IActionResult> Index()
        {
            return View(await _context.Aumentos.ToListAsync());
        }

        // GET: Aumentos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aumentos = _context.AumentoDetalle.Include(x => x.Empleado).Where(m => m.AumentosId == id);
            var aumento = _context.Aumentos.Where(x => x.Id == id).FirstOrDefault();
            ViewData["DatosAumento"] = aumento.Nombre + " por " + aumento.PorcentajeAumento;
            ViewData["IdAumento"] = id;

            if (aumentos == null)
            {
                return NotFound();
            }

            return View(aumentos);
        }

        // GET: Aumentos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Aumentos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,PorcentajeAumento")] Aumentos aumentos)
        {
            aumentos.AumentoAplicado = false;
            if (ModelState.IsValid)
            {
                _context.Add(aumentos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(aumentos);
        }

        // GET: Aumentos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aumentos = await _context.Aumentos.FindAsync(id);
            if (aumentos == null)
            {
                return NotFound();
            }
            return View(aumentos);
        }

        // POST: Aumentos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,PorcentajeAumento")] Aumentos aumentos)
        {
            if (id != aumentos.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(aumentos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AumentosExists(aumentos.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(aumentos);
        }

        // GET: Aumentos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aumentos = await _context.Aumentos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aumentos == null)
            {
                return NotFound();
            }

            return View(aumentos);
        }

        // POST: Aumentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var aumentos = await _context.Aumentos.FindAsync(id);
            _context.Aumentos.Remove(aumentos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AumentosExists(int id)
        {

            ViewData["idEmpleado"] = id;

            return _context.Aumentos.Any(e => e.Id == id);
        }

        public IActionResult GenerarListado(int AumentoId) { 
            Aumentos aumento = _context.Aumentos.Where(x=>x.Id == AumentoId).Where(x => x.AumentoAplicado == false).FirstOrDefault();

            if (aumento == null) {
                return RedirectToAction(nameof(Index));
            }

            IQueryable<Empleado> empleados = _context.Empleado.Include(x=>x.EmpleadoPlaza).ThenInclude(x=>x.Plaza).Where(x => x.Activo == true);

            foreach (var empleado in empleados)
            {
                var plazaActual = empleado.EmpleadoPlaza.Where(x => x.Plaza.PlazaActiva == true).FirstOrDefault();

                if (plazaActual == null) {  
                    continue;
                }

                var salarioAumento = plazaActual.Salario * (1 + (Convert.ToDecimal(aumento.PorcentajeAumento) / 100));

                AumentoDetalle detalle = new AumentoDetalle
                {
                    Fecha = DateTime.Now,
                    EmpleadoId = empleado.Id,
                    AumentosId = aumento.Id,
                    Valor = salarioAumento
                };
                _context.Add(detalle);

                plazaActual.FechaFinal = DateTime.Now;

                EmpleadoPlaza plazaAumento = new EmpleadoPlaza
                {
                    FechaInicio = DateTime.Now,
                    NumeroContrato = plazaActual.NumeroContrato,
                    Salario = salarioAumento,
                    EmpleadoId = plazaActual.EmpleadoId,
                    PlazaId = plazaActual.PlazaId
                };

                _context.Add(plazaAumento);
            }

            aumento.AumentoAplicado = true;

            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        // GET: Aumento/DetalleEmpleado/5
        public async Task<IActionResult> DetalleEmpleado(int id)
        {
            var aumentos = _context.AumentoDetalle.Include(x => x.Aumentos).Where(m => m.EmpleadoId == id);
            ViewData["idEmpleado"] = id;

            if (aumentos == null)
            {
                return NotFound();
            }

            return View(aumentos);
        }

        public IActionResult AgregarEmpleado(int Id)
        {
            var listaEmpleadosAumento = _context.AumentoDetalle.Include(x => x.Empleado).Where(x => x.AumentosId == Id).Select(x => x.EmpleadoId);

            var ListaEmpleados = _context.Empleado.Where(x => !listaEmpleadosAumento.Any(y => x.Id == y)).ToList().Select(x => new {
                x.Id,
                NombreCompleto = x.PrimerNombre
                                 + " " + x.SegundoNombre
                                 + " " + x.TercerNombre
                                 + " " + x.PrimerApellido
                                 + " " + x.SegundoApellido
                                 + " " + x.ApellidoCasada
            }).OrderBy(x => x.NombreCompleto);

            ViewData["IdDescuentos"] = Id;

            ViewData["ListadoEmpleados"] = new SelectList(ListaEmpleados, "Id", "NombreCompleto");
            return View();
        }

        public IActionResult CreateEmpleadoAumento(int id)
        {
            ViewData["idEmpleado"] = id;
            ViewData["nombreEmpleado"] = _context.Empleado.Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();

            return View();
        }

        // POST: Descuentos/CreateEmpleado
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateEmpleadoAumento([Bind("Id,Nombre,PorcentajeAumento")] Aumentos aumento)
        {
            aumento.AumentoAplicado = true;
            var empleadoId = aumento.Id;
            if (ModelState.IsValid)
            {
                aumento.Id = 0;
                _context.Add(aumento);
                await _context.SaveChangesAsync();

                Empleado empleado = _context.Empleado.Include(x => x.EmpleadoPlaza).
                                        ThenInclude(x => x.Plaza).Where(x => x.Activo == true)
                                        .Where(x => x.Id == empleadoId).FirstOrDefault();

                var plazaActual = empleado.EmpleadoPlaza.Where(x => x.Plaza.PlazaActiva == true).FirstOrDefault();

                var salarioAumento = plazaActual.Salario * (1 + (Convert.ToDecimal(aumento.PorcentajeAumento) / 100));

                AumentoDetalle detalle = new AumentoDetalle
                {
                    Fecha = DateTime.Now,
                    AumentosId = aumento.Id,
                    EmpleadoId = empleadoId,
                    Valor = salarioAumento
                };

                _context.Add(detalle);

                await _context.SaveChangesAsync();
                return RedirectToAction("DetalleEmpleado", "Aumentos", new { id = empleadoId });

            }

            return RedirectToAction("DetalleEmpleado", "Aumentos", new { id = empleadoId });
            }
    }
}
