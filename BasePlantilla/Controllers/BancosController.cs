﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class BancosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;
        private readonly IBancoRepository _bancoRepository;

        public BancosController(TalentoHumanoDbContext context, IBancoRepository bancoRepository)
        {
            _context = context;
            _bancoRepository = bancoRepository;
        }

        // GET: Bancos
        public IActionResult Index(int? tipoAlerta)
        {
            ViewBag.TipoAlerta = tipoAlerta;
            return View(_bancoRepository.GetAll());
        }

        // GET: Bancos/Details/5
        public IActionResult Details(int id)
        {
            var banco = _bancoRepository.Get(id);
            if (banco == null)
            {
                return NotFound();
            }

            return View(banco);
        }

        // GET: Bancos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Bancos/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Codigo,Nombre")] Banco banco)
        {
            if (ModelState.IsValid)
            {
                _bancoRepository.Create(banco);
                if (!_bancoRepository.Save())
                {
                    throw new Exception($"Error al guardar el objeto");
                }
                return RedirectToAction(nameof(Index),new {tipoAlerta = 1});
            }
            return View(banco);
        }

        // GET: Bancos/Edit/5
        public IActionResult Edit(int id)
        {
            var banco = _bancoRepository.Get(id);
            if (banco == null)
            {
                return NotFound();
            }
            return View(banco);
        }

        // POST: Bancos/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Codigo,Nombre")] Banco banco)
        {
            if (id != banco.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bancoRepository.Update(banco);

                if (!_bancoRepository.Save())
                {
                    throw new Exception($"Error al actualizar el objeto con id: {id}.");
                }
                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }
            return View(banco);
        }

        // GET: Bancos/Delete/5
        public IActionResult Delete(int id)
        {
            var banco = _bancoRepository.Get(id);

            if (banco == null)
            {
                return NotFound();
            }

            return View(banco);
        }

        // POST: Bancos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var banco = _bancoRepository.Get(id);
            _bancoRepository.Delete(banco);

            if (!_bancoRepository.Save())
            {
                throw new Exception($"Error al eliminar el objeto con id: {id}.");
            }

            return RedirectToAction(nameof(Index), new { tipoAlerta = 2 });
        }

    }
}
