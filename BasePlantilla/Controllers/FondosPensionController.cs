﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class FondosPensionController : Controller
    {
        private readonly IFondoPensionRepository _fondoPensionRepository;

        public FondosPensionController(IFondoPensionRepository fondoPensionRepository)
        {
            _fondoPensionRepository = fondoPensionRepository;
        }

        // GET: FondosPension
        public IActionResult Index(int? tipoAlerta)
        {
            ViewBag.TipoAlerta = tipoAlerta;
            return View(_fondoPensionRepository.GetAll());
        }

        // GET: FondosPension/Details/5
        public IActionResult Details(int id)
        {
            var fondoPension = _fondoPensionRepository.Get(id);

            if (fondoPension == null)
            {
                return NotFound();
            }

            return View(fondoPension);
        }

        // GET: FondosPension/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FondosPension/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Codigo,Nombre")] FondoPension fondoPension)
        {
            if (ModelState.IsValid)
            {
                _fondoPensionRepository.Create(fondoPension);

                if (!_fondoPensionRepository.Save())
                {
                    throw new Exception($"Error al guardar el objeto");
                }

                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }
            return View(fondoPension);
        }

        // GET: FondosPension/Edit/5
        public IActionResult Edit(int id)
        {
            var fondoPension = _fondoPensionRepository.Get(id);

            if (fondoPension == null)
            {
                return NotFound();
            }

            return View(fondoPension);
        }

        // POST: FondosPension/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Codigo,Nombre")] FondoPension fondoPension)
        {
            if (id != fondoPension.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _fondoPensionRepository.Update(fondoPension);

                if (!_fondoPensionRepository.Save())
                {
                    throw new Exception($"Error al actualizar el objeto con id: {id}.");
                }

                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }

            return View(fondoPension);
        }

        // GET: FondosPension/Delete/5
        public IActionResult Delete(int id)
        {
            var fondoPension = _fondoPensionRepository.Get(id);
            if (fondoPension == null)
            {
                return NotFound();
            }

            return View(fondoPension);
        }

        // POST: FondosPension/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var fondoPension = _fondoPensionRepository.Get(id);
            _fondoPensionRepository.Delete(fondoPension);

            if (!_fondoPensionRepository.Save())
            {
                throw new Exception($"Error al eliminar el objeto con id: {id}.");
            }

            return RedirectToAction(nameof(Index), new { tipoAlerta = 2 });
        }
    }
}
