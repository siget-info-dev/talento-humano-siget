﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class EmpleadoPlazasController : Controller
    {
        private readonly TalentoHumanoDbContext _context;
        private readonly IEmpleadoRepository _empleadoRepository;

        public EmpleadoPlazasController(
            TalentoHumanoDbContext context,
            IEmpleadoRepository empleadoRepository)
        {
            _context = context;
            _empleadoRepository = empleadoRepository;
        }

        // GET: EmpleadoPlazas
        public async Task<IActionResult> Index(int empleadoId, int tipoAlerta)
        {
            ViewBag.TipoAlerta = tipoAlerta;
            var talentoHumanoDbContext = _context
                .EmpleadoPlaza
                //.Include(e => e.Cargo)
                .Include(e => e.Empleado)
                .Include(e => e.Plaza)
                .Where(e => e.EmpleadoId == empleadoId);
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: EmpleadoPlazas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empleadoPlaza = await _context.EmpleadoPlaza
                //.Include(e => e.Cargo)
                .Include(e => e.Empleado)
                .Include(e => e.Plaza)
                .FirstOrDefaultAsync(m => m.Id == id);

            ViewData["EmpleadoId"] = empleadoPlaza.EmpleadoId;

            if (empleadoPlaza == null)
            {
                return NotFound();
            }

            return View(empleadoPlaza);
        }

        // GET: EmpleadoPlazas/Create
        public IActionResult Create(int empleadoId)
        {
            var empleado = _empleadoRepository.Get(empleadoId);
            var empleadoPlaza = new EmpleadoPlaza {Empleado = empleado};
            empleadoPlaza.FechaInicio = DateTime.Today;

            ViewData["DepartamentosSiget"] = new SelectList(_context.DepartamentosSiget, "Id", "Nombre");
            return View(empleadoPlaza);
        }

        // POST: EmpleadoPlazas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmpleadoId,CargoId,PlazaId,FechaInicio,FechaFinal,NumeroContrato,Salario,Observacion")] EmpleadoPlaza empleadoPlaza)
        {
            if (ModelState.IsValid)
            {
                // Logica para crear nueva plaza Empleado
                var empleadoPlazaAnterior = _context.EmpleadoPlaza.Include(X=>X.Plaza)
                    .FirstOrDefault(x => x.EmpleadoId == empleadoPlaza.EmpleadoId && x.Plaza.PlazaActiva == true);
                if (empleadoPlazaAnterior != null)
                {
                    empleadoPlazaAnterior.Plaza.PlazaActiva = false;
                    empleadoPlazaAnterior.FechaFinal = DateTime.Now;
                }
                empleadoPlaza.Plaza = _context.Plazas.Where(x => x.Id == empleadoPlaza.PlazaId).FirstOrDefault();
                empleadoPlaza.Plaza.PlazaActiva = true;
                _context.Add(empleadoPlaza);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new {empleadoId = empleadoPlaza.EmpleadoId, tipoAlerta = 1});
            }
            ViewData["DepartamentosSiget"] = new SelectList(_context.DepartamentosSiget, "Id", "Nombre");
            //ViewData["CargoId"] = new SelectList(_context.Plazas, "Id", "Codigo", empleadoPlaza.CargoId);
            //ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", empleadoPlaza.EmpleadoId);
            //ViewData["PlazaId"] = new SelectList(_context.Plazas, "Id", "Codigo", empleadoPlaza.PlazaId);
            return View(empleadoPlaza);
        }

        // GET: EmpleadoPlazas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empleadoPlaza = await _context.EmpleadoPlaza.FindAsync(id);
            if (empleadoPlaza == null)
            {
                return NotFound();
            }

            empleadoPlaza.Plaza = _context.Plazas.Where(x => x.Id == empleadoPlaza.PlazaId).FirstOrDefault();

            //ViewData["CargoId"] = new SelectList(_context.Plazas, "Id", "Codigo", empleadoPlaza.CargoId);
            ViewData["EmpleadoId"] = empleadoPlaza.EmpleadoId;
            ViewData["PlazaId"] = new SelectList(_context.Plazas, "Id", "Nombre", empleadoPlaza.PlazaId);
            return View(empleadoPlaza);
        }

        // POST: EmpleadoPlazas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmpleadoId,CargoId,PlazaId,FechaInicio,FechaFinal,NumeroContrato,Salario,PlazaActiva,Observacion")] EmpleadoPlaza empleadoPlaza)
        {
            if (id != empleadoPlaza.EmpleadoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(empleadoPlaza);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmpleadoPlazaExists(empleadoPlaza.EmpleadoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CargoId"] = new SelectList(_context.Plazas, "Id", "Codigo", empleadoPlaza.CargoId);
            ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", empleadoPlaza.EmpleadoId);
            ViewData["PlazaId"] = new SelectList(_context.Plazas, "Id", "Codigo", empleadoPlaza.PlazaId);
            return View(empleadoPlaza);
        }

        // GET: EmpleadoPlazas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empleadoPlaza = await _context.EmpleadoPlaza
                //.Include(e => e.Cargo)
                .Include(e => e.Empleado)
                .Include(e => e.Plaza)
                .FirstOrDefaultAsync(m => m.EmpleadoId == id);
            if (empleadoPlaza == null)
            {
                return NotFound();
            }

            return View(empleadoPlaza);
        }

        // POST: EmpleadoPlazas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var empleadoPlaza = await _context.EmpleadoPlaza.FindAsync(id);
            
            //No se tendria que poder eliminar plazas asignadas.

            //_context.EmpleadoPlaza.Remove(empleadoPlaza);
            //await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        private bool EmpleadoPlazaExists(int id)
        {
            return _context.EmpleadoPlaza.Any(e => e.EmpleadoId == id);
        }
    }
}
