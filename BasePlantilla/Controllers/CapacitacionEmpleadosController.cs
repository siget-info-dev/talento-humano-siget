﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class CapacitacionEmpleadosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public CapacitacionEmpleadosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: CapacitacionEmpleados
        public async Task<IActionResult> Index(int Id)
        {
            var talentoHumanoDbContext = _context.CapacitacionEmpleado.Where(c=>c.CapacitacionId == Id)
                                            .Include(c => c.Capacitacion).Include(c => c.Empleado);

            ViewData["NombreCapacitacion"] = _context.Capacitacion.Where(x => x.Id == Id).Select(x => x.Nombre).FirstOrDefault();
            ViewData["IdCapacitacion"] = _context.Capacitacion.Where(x => x.Id == Id).Select(x => x.Id).FirstOrDefault();

            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: CapacitacionEmpleados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var capacitacionEmpleado = await _context.CapacitacionEmpleado
                .Include(c => c.Capacitacion)
                .Include(c => c.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (capacitacionEmpleado == null)
            {
                return NotFound();
            }
            
            ViewData["CapacitacionId"] = _context.Capacitacion.Where(x => x.Id == id).Select(x => x.Id).FirstOrDefault();

            return View(capacitacionEmpleado);
        }

        // GET: CapacitacionEmpleados/Create
        public IActionResult Create(int Id)
        {
            var empleados = _context.Empleado.ToList().Select(x => new
            {
                x.Id,
                NombreCompleto = x.PrimerNombre
                                 + " " + x.SegundoNombre
                                 + " " + x.TercerNombre
                                 + " " + x.PrimerApellido
                                 + " " + x.SegundoApellido
                                 + " " + x.ApellidoCasada
            });

            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "NombreCompleto");
            ViewData["IdCapacitacion"] = _context.Capacitacion.Where(x => x.Id == Id).Select(x => x.Id).FirstOrDefault();
            ViewData["Id"] = Id;

            return View();
        }

        // POST: CapacitacionEmpleados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CapacitacionId,EmpleadoId")] CapacitacionEmpleado capacitacionEmpleado)
        {
            if (ModelState.IsValid)
            {
                _context.Add(capacitacionEmpleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = capacitacionEmpleado.CapacitacionId });
            }
            ViewData["CapacitacionId"] = new SelectList(_context.Capacitacion, "Id", "Id", capacitacionEmpleado.CapacitacionId);
            ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", capacitacionEmpleado.EmpleadoId);
            return View(capacitacionEmpleado);
        }

        // GET: CapacitacionEmpleados/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var capacitacionEmpleado = await _context.CapacitacionEmpleado.FindAsync(id);
            if (capacitacionEmpleado == null)
            {
                return NotFound();
            }

            var empleados = _context.Empleado.ToList().Select(x => new
            {
                x.Id,
                NombreCompleto = x.PrimerNombre
                     + " " + x.SegundoNombre
                     + " " + x.TercerNombre
                     + " " + x.PrimerApellido
                     + " " + x.SegundoApellido
                     + " " + x.ApellidoCasada
            });

            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "NombreCompleto");
            ViewData["CapacitacionId"] = _context.CapacitacionEmpleado.Where(x => x.Id == id).Select(x => x.CapacitacionId).FirstOrDefault();
            return View(capacitacionEmpleado);
        }

        // POST: CapacitacionEmpleados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CapacitacionId,EmpleadoId")] CapacitacionEmpleado capacitacionEmpleado)
        {
            if (id != capacitacionEmpleado.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(capacitacionEmpleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CapacitacionEmpleadoExists(capacitacionEmpleado.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { id = capacitacionEmpleado.CapacitacionId });
            }
            var empleados = _context.Empleado.ToList().Select(x => new
            {
                x.Id,
                NombreCompleto = x.PrimerNombre
                                 + " " + x.SegundoNombre
                                 + " " + x.TercerNombre
                                 + " " + x.PrimerApellido
                                 + " " + x.SegundoApellido
                                 + " " + x.ApellidoCasada
            });

            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "NombreCompleto");

            ViewData["IdCapacitacion"] = (from c in _context.Capacitacion
                                          where (from dc in _context.CapacitacionEmpleado
                                                 where dc.Id == id
                                                 select dc.CapacitacionId).Contains(c.Id)
                                          select c.Id).FirstOrDefault();

            return View(capacitacionEmpleado);
        }

        // GET: CapacitacionEmpleados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var capacitacionEmpleado = await _context.CapacitacionEmpleado
                .Include(c => c.Capacitacion)
                .Include(c => c.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (capacitacionEmpleado == null)
            {
                return NotFound();
            }
            ViewData["IdCapacitacion"] = capacitacionEmpleado.CapacitacionId;
            return View(capacitacionEmpleado);
        }

        // POST: CapacitacionEmpleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var capacitacionEmpleado = await _context.CapacitacionEmpleado.FindAsync(id);
            _context.CapacitacionEmpleado.Remove(capacitacionEmpleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { id = capacitacionEmpleado.CapacitacionId });
        }

        private bool CapacitacionEmpleadoExists(int id)
        {
            return _context.CapacitacionEmpleado.Any(e => e.Id == id);
        }
    }
}
