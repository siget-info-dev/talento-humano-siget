﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class TipoAccionesDePersonalController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public TipoAccionesDePersonalController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: TipoAccionesDePersonal
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoAccionPersonal.ToListAsync());
        }

        // GET: TipoAccionesDePersonal/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAccionesDePersonal = await _context.TipoAccionPersonal
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoAccionesDePersonal == null)
            {
                return NotFound();
            }

            return View(tipoAccionesDePersonal);
        }

        // GET: TipoAccionesDePersonal/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoAccionesDePersonal/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Acciones")] TipoAccionesDePersonal tipoAccionesDePersonal)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoAccionesDePersonal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoAccionesDePersonal);
        }

        // GET: TipoAccionesDePersonal/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAccionesDePersonal = await _context.TipoAccionPersonal.FindAsync(id);
            if (tipoAccionesDePersonal == null)
            {
                return NotFound();
            }
            return View(tipoAccionesDePersonal);
        }

        // POST: TipoAccionesDePersonal/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Acciones")] TipoAccionesDePersonal tipoAccionesDePersonal)
        {
            if (id != tipoAccionesDePersonal.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoAccionesDePersonal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoAccionesDePersonalExists(tipoAccionesDePersonal.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoAccionesDePersonal);
        }

        // GET: TipoAccionesDePersonal/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAccionesDePersonal = await _context.TipoAccionPersonal
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoAccionesDePersonal == null)
            {
                return NotFound();
            }

            return View(tipoAccionesDePersonal);
        }

        // POST: TipoAccionesDePersonal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoAccionesDePersonal = await _context.TipoAccionPersonal.FindAsync(id);
            _context.TipoAccionPersonal.Remove(tipoAccionesDePersonal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoAccionesDePersonalExists(int id)
        {
            return _context.TipoAccionPersonal.Any(e => e.Id == id);
        }
    }
}
