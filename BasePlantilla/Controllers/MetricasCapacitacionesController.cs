﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using Newtonsoft.Json;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class MetricasCapacitacionesController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public MetricasCapacitacionesController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public string CargaDatosXMes()
        {
            var capacitaciones = _context.Capacitacion
                                    .Include(x => x.CapacitacionEmpleado)
                                    .Select(x => new { Mes = x.FechaInicio.Month, Cuenta = x.CapacitacionEmpleado.Count() });
                
            var labels = new List<string>();
            var data = new List<string>();

            foreach (var capacitacion in capacitaciones) {
                labels.Add(capacitacion.Mes.ToString());
                data.Add(capacitacion.Cuenta.ToString());
            }

            var datos = new Dictionary<string, List<string>>
            {
                {"labels", labels},
                {"data", data}
            };

            return JsonConvert.SerializeObject(datos);
        }
    }
}
