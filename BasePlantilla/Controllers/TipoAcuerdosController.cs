﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class TipoAcuerdosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public TipoAcuerdosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: TipoAcuerdos
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoAcuerdos.ToListAsync());
        }

        // GET: TipoAcuerdos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAcuerdo = await _context.TipoAcuerdos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoAcuerdo == null)
            {
                return NotFound();
            }

            return View(tipoAcuerdo);
        }

        // GET: TipoAcuerdos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoAcuerdos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Acuerdo")] TipoAcuerdo tipoAcuerdo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoAcuerdo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoAcuerdo);
        }

        // GET: TipoAcuerdos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAcuerdo = await _context.TipoAcuerdos.FindAsync(id);
            if (tipoAcuerdo == null)
            {
                return NotFound();
            }
            return View(tipoAcuerdo);
        }

        // POST: TipoAcuerdos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Acuerdo")] TipoAcuerdo tipoAcuerdo)
        {
            if (id != tipoAcuerdo.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoAcuerdo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoAcuerdoExists(tipoAcuerdo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoAcuerdo);
        }

        // GET: TipoAcuerdos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAcuerdo = await _context.TipoAcuerdos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoAcuerdo == null)
            {
                return NotFound();
            }

            return View(tipoAcuerdo);
        }

        // POST: TipoAcuerdos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoAcuerdo = await _context.TipoAcuerdos.FindAsync(id);
            _context.TipoAcuerdos.Remove(tipoAcuerdo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoAcuerdoExists(int id)
        {
            return _context.TipoAcuerdos.Any(e => e.Id == id);
        }
    }
}
