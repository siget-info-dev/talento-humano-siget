﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class FasesEvaluacionesController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public FasesEvaluacionesController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: FasesEvaluaciones
        public async Task<IActionResult> Index()
        {
            var talentoHumanoDbContext = _context.FasesEvaluaciones.Include(f => f.TipoEvaluaciones);
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: FasesEvaluaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fasesEvaluaciones = await _context.FasesEvaluaciones
                .Include(f => f.TipoEvaluaciones)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fasesEvaluaciones == null)
            {
                return NotFound();
            }

            return View(fasesEvaluaciones);
        }

        // GET: FasesEvaluaciones/Create
        public IActionResult Create()
        {
            ViewData["TipoEvaluacionesId"] = new SelectList(_context.TipoEvaluaciones, "Id", "Nombre");
            return View();
        }

        // POST: FasesEvaluaciones/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NombreFase,PorcentajeEvaluacion,TipoEvaluacionesId")] FasesEvaluaciones fasesEvaluaciones)
        {
            if (ModelState.IsValid)
            {
                _context.Add(fasesEvaluaciones);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["TipoEvaluacionesId"] = new SelectList(_context.TipoEvaluaciones, "Id", "Id", fasesEvaluaciones.TipoEvaluacionesId);
            return View(fasesEvaluaciones);
        }

        // GET: FasesEvaluaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fasesEvaluaciones = await _context.FasesEvaluaciones.FindAsync(id);
            if (fasesEvaluaciones == null)
            {
                return NotFound();
            }
            ViewData["TipoEvaluacionesId"] = new SelectList(_context.TipoEvaluaciones, "Id", "Nombre", fasesEvaluaciones.TipoEvaluacionesId);
            return View(fasesEvaluaciones);
        }

        // POST: FasesEvaluaciones/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NombreFase,PorcentajeEvaluacion,TipoEvaluacionesId")] FasesEvaluaciones fasesEvaluaciones)
        {
            if (id != fasesEvaluaciones.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fasesEvaluaciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FasesEvaluacionesExists(fasesEvaluaciones.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TipoEvaluacionesId"] = new SelectList(_context.TipoEvaluaciones, "Id", "Id", fasesEvaluaciones.TipoEvaluacionesId);
            return View(fasesEvaluaciones);
        }

        // GET: FasesEvaluaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fasesEvaluaciones = await _context.FasesEvaluaciones
                .Include(f => f.TipoEvaluaciones)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fasesEvaluaciones == null)
            {
                return NotFound();
            }

            return View(fasesEvaluaciones);
        }

        // POST: FasesEvaluaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fasesEvaluaciones = await _context.FasesEvaluaciones.FindAsync(id);
            _context.FasesEvaluaciones.Remove(fasesEvaluaciones);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FasesEvaluacionesExists(int id)
        {
            return _context.FasesEvaluaciones.Any(e => e.Id == id);
        }
    }
}
