﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class CapacitacionController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public CapacitacionController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Capacitacion
        public async Task<IActionResult> Index()
        {
            var talentoHumanoDbContext = _context.Capacitacion.Include(c => c.Facilitadores);
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: Capacitacion/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var capacitacion = await _context.Capacitacion
                .Include(c => c.Facilitadores)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (capacitacion == null)
            {
                return NotFound();
            }

            return View(capacitacion);
        }

        // GET: Capacitacion/Create
        public IActionResult Create()
        {
            ViewData["FacilitadoresId"] = new SelectList(_context.Facilitadores.OrderBy(x=>x.Nombre), "Id", "Nombre");
            return View();
        }

        // POST: Capacitacion/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,FechaInicio,FechaFin,Descripcion,Estado,TipoCapacitacion,FacilitadoresId")] Capacitacion capacitacion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(capacitacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FacilitadoresId"] = new SelectList(_context.Facilitadores.OrderBy(x => x.Nombre), "Id", "Nombre", capacitacion.FacilitadoresId);
            return View(capacitacion);
        }

        // GET: Capacitacion/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var capacitacion = await _context.Capacitacion.FindAsync(id);
            if (capacitacion == null)
            {
                return NotFound();
            }
            ViewData["FacilitadoresId"] = new SelectList(_context.Facilitadores.OrderBy(x => x.Nombre), "Id", "Nombre", capacitacion.FacilitadoresId);
            return View(capacitacion);
        }

        // POST: Capacitacion/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,FechaInicio,FechaFin,Descripcion,Estado,TipoCapacitacion,FacilitadoresId")] Capacitacion capacitacion)
        {
            if (id != capacitacion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(capacitacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CapacitacionExists(capacitacion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FacilitadoresId"] = new SelectList(_context.Facilitadores.OrderBy(x => x.Nombre), "Id", "Nombre", capacitacion.FacilitadoresId);
            return View(capacitacion);
        }

        // GET: Capacitacion/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var capacitacion = await _context.Capacitacion
                .Include(c => c.Facilitadores)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (capacitacion == null)
            {
                return NotFound();
            }

            return View(capacitacion);
        }

        // POST: Capacitacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var capacitacion = await _context.Capacitacion.FindAsync(id);
            _context.Capacitacion.Remove(capacitacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CapacitacionExists(int id)
        {
            return _context.Capacitacion.Any(e => e.Id == id);
        }

        public IActionResult DocumentosCapacitacion(int id)
        {
            return RedirectToAction("index", "DocumentosCapacitacion", new { id });
        }

        public IActionResult CapacitacionEmpleados(int id)
        {
            return RedirectToAction("index", "CapacitacionEmpleados", new { id });
        }
    }
}
