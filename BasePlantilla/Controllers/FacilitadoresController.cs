﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class FacilitadoresController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public FacilitadoresController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Facilitadores
        public async Task<IActionResult> Index()
        {
            return View(await _context.Facilitadores.ToListAsync());
        }

        // GET: Facilitadores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facilitadores = await _context.Facilitadores
                .FirstOrDefaultAsync(m => m.Id == id);
            if (facilitadores == null)
            {
                return NotFound();
            }

            return View(facilitadores);
        }

        // GET: Facilitadores/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Facilitadores/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,Proveedor")] Facilitadores facilitadores)
        {
            if (ModelState.IsValid)
            {
                _context.Add(facilitadores);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(facilitadores);
        }

        // GET: Facilitadores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facilitadores = await _context.Facilitadores.FindAsync(id);
            if (facilitadores == null)
            {
                return NotFound();
            }
            return View(facilitadores);
        }

        // POST: Facilitadores/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,Proveedor")] Facilitadores facilitadores)
        {
            if (id != facilitadores.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(facilitadores);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FacilitadoresExists(facilitadores.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(facilitadores);
        }

        // GET: Facilitadores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facilitadores = await _context.Facilitadores
                .FirstOrDefaultAsync(m => m.Id == id);
            if (facilitadores == null)
            {
                return NotFound();
            }

            return View(facilitadores);
        }

        // POST: Facilitadores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var facilitadores = await _context.Facilitadores.FindAsync(id);
            _context.Facilitadores.Remove(facilitadores);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FacilitadoresExists(int id)
        {
            return _context.Facilitadores.Any(e => e.Id == id);
        }
    }
}
