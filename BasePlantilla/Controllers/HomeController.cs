﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BasePlantilla.Models;
using Microsoft.Graph;
using SistemaGestionTalentoHumano.Services;
using Microsoft.Identity.Web;
using System.Security.Claims;
using System.Threading;

namespace BasePlantilla.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger
            , GraphServiceClient graphClient
            )
        {
            _logger = logger;
           Graph.Initialize(graphClient);

        }

        //public IActionResult Index()
        //{
        //    return View();
        //}

        [AuthorizeForScopes(Scopes = new[] { "Directory.Read.All" })]
        public async Task<IActionResult> Index()
        {
            //se obliga a que se firme el empleado en la aplicacion contra el AD
            var consulta = Graph.ConsultaUsuario().Result;
            //var usuario = User.FindFirst(ClaimTypes.Role);

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
