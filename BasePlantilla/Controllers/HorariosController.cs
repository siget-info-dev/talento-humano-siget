﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class HorariosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public HorariosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Horarios
        public async Task<IActionResult> Index()
        {
            var listado = _context.Horarios.ToList();
            foreach (var horario in listado) {
                horario.Dia = Tools.DiaSemanaById(horario.DiaSemana);
            }

            return View(listado);
        }

        // GET: Horarios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horarios = await _context.Horarios
                .FirstOrDefaultAsync(m => m.Id == id);
            if (horarios == null)
            {
                return NotFound();
            }
            horarios.Dia = Tools.DiaSemanaById(horarios.DiaSemana);

            return View(horarios);
        }

        // GET: Horarios/Create
        public IActionResult Create()
        {
            ViewData["DiaSemana"] = new SelectList(Tools.DiasSemana(), "Id", "Dia"); ;
            return View();
        }

        // POST: Horarios/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Descripcion,ToleranciaMaxima,DiaSemana,Inicio,Final,PorDefecto")] Horarios horarios)
        {
            if (ModelState.IsValid)
            {
                _context.Add(horarios);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(horarios);
        }

        // GET: Horarios/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horarios = await _context.Horarios.FindAsync(id);
            if (horarios == null)
            {
                return NotFound();
            }
            ViewData["DiaSemana"] = new SelectList(Tools.DiasSemana(), "Id", "Dia", horarios.DiaSemana); 

            return View(horarios);
        }

        // POST: Horarios/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Descripcion,ToleranciaMaxima,DiaSemana,Inicio,Final,PorDefecto")] Horarios horarios)
        {
            if (id != horarios.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(horarios);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HorariosExists(horarios.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(horarios);
        }

        // GET: Horarios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horarios = await _context.Horarios
                .FirstOrDefaultAsync(m => m.Id == id);
            if (horarios == null)
            {
                return NotFound();
            }
            horarios.Dia = Tools.DiaSemanaById(horarios.DiaSemana);
            return View(horarios);
        }

        // POST: Horarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var horarios = await _context.Horarios.FindAsync(id);
            _context.Horarios.Remove(horarios);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HorariosExists(int id)
        {
            return _context.Horarios.Any(e => e.Id == id);
        }

        public async Task<IActionResult> AplicaPermiso(int Id) {

            var horario = _context.Horarios.Where(x => x.Id == Id).FirstOrDefault();

            var empleados = from e in _context.Empleado select new { Id = e.Id, nombreCompleto = e.GetNombreCompleto() };

            ViewData["Empleados"] = new SelectList(empleados, "Id", "nombreCompleto");
            return View(horario);
        }
    }
}
