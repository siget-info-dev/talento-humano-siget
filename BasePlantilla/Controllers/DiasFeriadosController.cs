﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class DiasFeriadosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public DiasFeriadosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: DiasFeriados
        public async Task<IActionResult> Index()
        {
            return View(await _context.DiasFeriados.ToListAsync());
        }

        // GET: DiasFeriados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diasFeriados = await _context.DiasFeriados
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diasFeriados == null)
            {
                return NotFound();
            }

            return View(diasFeriados);
        }

        // GET: DiasFeriados/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DiasFeriados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Descripcion,Fecha")] DiasFeriados diasFeriados)
        {
            if (ModelState.IsValid)
            {
                _context.Add(diasFeriados);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(diasFeriados);
        }

        // GET: DiasFeriados/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diasFeriados = await _context.DiasFeriados.FindAsync(id);
            if (diasFeriados == null)
            {
                return NotFound();
            }
            return View(diasFeriados);
        }

        // POST: DiasFeriados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Descripcion,Fecha")] DiasFeriados diasFeriados)
        {
            if (id != diasFeriados.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(diasFeriados);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiasFeriadosExists(diasFeriados.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(diasFeriados);
        }

        // GET: DiasFeriados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diasFeriados = await _context.DiasFeriados
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diasFeriados == null)
            {
                return NotFound();
            }

            return View(diasFeriados);
        }

        // POST: DiasFeriados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var diasFeriados = await _context.DiasFeriados.FindAsync(id);
            _context.DiasFeriados.Remove(diasFeriados);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DiasFeriadosExists(int id)
        {
            return _context.DiasFeriados.Any(e => e.Id == id);
        }
    }
}
