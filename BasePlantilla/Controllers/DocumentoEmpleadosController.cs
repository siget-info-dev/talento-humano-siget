﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class DocumentoEmpleadosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public DocumentoEmpleadosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: DocumentoEmpleados
        public async Task<IActionResult> Index(int? id)
        {
            var talentoHumanoDbContext = _context.DocumentoEmpleado
                                            .Include(d => d.Documento)
                                            .Include(d => d.Empleado)
                                            .Where(d => d.EmpleadoId == id);

            ViewData["CodigoEmpleado"] = id;
            ViewData["NombreEmpleado"] = _context.Empleado.ToList().Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: DocumentoEmpleados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var documentoEmpleado = await _context.DocumentoEmpleado
                .Include(d => d.Documento)
                .Include(d => d.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (documentoEmpleado == null)
            {
                return NotFound();
            }

            return View(documentoEmpleado);
        }

        // GET: DocumentoEmpleados/Create
        public IActionResult Create(int? Id)
        {
            ViewData["NombreEmpleado"] = _context.Empleado.Where(x => x.Id == Id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            ViewData["EmpleadoId"] = Id;

            var doc = from a in _context.Documentos
                       where !(from b in _context.DocumentoEmpleado select b.DocumentoId).Contains(a.Id)
                       select new { a.Id, a.Nombre }; 

            ViewData["DocumentoId"] = new SelectList(doc, "Id", "Nombre");
            return View();
        }

        // POST: DocumentoEmpleados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NombreArchivo,DocumentoId,EmpleadoId,FFArchivo")] DocumentoEmpleado documentoEmpleado)
        {
            if (ModelState.IsValid)
            {
                string NombreArchivo = "";
                if (documentoEmpleado.FFArchivo != null)
                {
                    NombreArchivo = Tools.subeArchivo(documentoEmpleado.FFArchivo);
                }

                documentoEmpleado.NombreArchivo = NombreArchivo;
                _context.Add(documentoEmpleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = documentoEmpleado.EmpleadoId });

            }
            ViewData["DocumentoId"] = new SelectList(_context.Documentos, "Id", "Nombre", documentoEmpleado.DocumentoId);
            return View(documentoEmpleado);
        }

        // GET: DocumentoEmpleados/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var documentoEmpleado =  _context.DocumentoEmpleado.Include(x => x.Empleado).Where(x => x.Id == id).FirstOrDefault();
            if (documentoEmpleado == null)
            {
                return NotFound();
            }
            ViewData["DocumentoId"] = new SelectList(_context.Documentos, "Id", "Nombre", documentoEmpleado.DocumentoId);
            return View(documentoEmpleado);
        }

        // POST: DocumentoEmpleados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NombreArchivo,DocumentoId,EmpleadoId")] DocumentoEmpleado documentoEmpleado)
        {
            if (id != documentoEmpleado.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(documentoEmpleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DocumentoEmpleadoExists(documentoEmpleado.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DocumentoId"] = new SelectList(_context.Documentos, "Id", "Nombre", documentoEmpleado.DocumentoId);
            return View(documentoEmpleado);
        }

        // GET: DocumentoEmpleados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var documentoEmpleado = await _context.DocumentoEmpleado
                .Include(d => d.Documento)
                .Include(d => d.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (documentoEmpleado == null)
            {
                return NotFound();
            }

            return View(documentoEmpleado);
        }

        // POST: DocumentoEmpleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var documentoEmpleado = await _context.DocumentoEmpleado.FindAsync(id);
            _context.DocumentoEmpleado.Remove(documentoEmpleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DocumentoEmpleadoExists(int id)
        {
            return _context.DocumentoEmpleado.Any(e => e.Id == id);
        }
    }
}
