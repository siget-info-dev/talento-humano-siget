﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class ProfesionOficioController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public ProfesionOficioController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: ProfesionOficio
        public async Task<IActionResult> Index()
        {
            return View(await _context.ProfesionOficio.ToListAsync());
        }

        // GET: ProfesionOficio/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesionOficio = await _context.ProfesionOficio
                .FirstOrDefaultAsync(m => m.Id == id);
            if (profesionOficio == null)
            {
                return NotFound();
            }

            return View(profesionOficio);
        }

        // GET: ProfesionOficio/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ProfesionOficio/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre")] ProfesionOficio profesionOficio)
        {
            if (ModelState.IsValid)
            {
                _context.Add(profesionOficio);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(profesionOficio);
        }

        // GET: ProfesionOficio/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesionOficio = await _context.ProfesionOficio.FindAsync(id);
            if (profesionOficio == null)
            {
                return NotFound();
            }
            return View(profesionOficio);
        }

        // POST: ProfesionOficio/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] ProfesionOficio profesionOficio)
        {
            if (id != profesionOficio.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(profesionOficio);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProfesionOficioExists(profesionOficio.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(profesionOficio);
        }

        // GET: ProfesionOficio/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesionOficio = await _context.ProfesionOficio
                .FirstOrDefaultAsync(m => m.Id == id);
            if (profesionOficio == null)
            {
                return NotFound();
            }

            return View(profesionOficio);
        }

        // POST: ProfesionOficio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var profesionOficio = await _context.ProfesionOficio.FindAsync(id);
            _context.ProfesionOficio.Remove(profesionOficio);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProfesionOficioExists(int id)
        {
            return _context.ProfesionOficio.Any(e => e.Id == id);
        }
    }
}
