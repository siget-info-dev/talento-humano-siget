﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class PrestacionEmpleadosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public PrestacionEmpleadosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: PrestacionEmpleados
        public async Task<IActionResult> Index(int Id)
        {
            var talentoHumanoDbContext = _context.PrestacionEmpleado
                .Include(p => p.Empleado)
                .Include(p => p.Prestaciones)
                .Where(x => x.EmpleadoId == Id);

            ViewData["CodigoEmpleado"] = Id;
            ViewData["NombreEmpleado"] = _context.Empleado.ToList().Where(x => x.Id == Id).Select(x => x.GetNombreCompleto()).FirstOrDefault();

            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: PrestacionEmpleados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prestacionEmpleado = await _context.PrestacionEmpleado
                .Include(p => p.Empleado)
                .Include(p => p.Prestaciones)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (prestacionEmpleado == null)
            {
                return NotFound();
            }

            ViewData["EmpleadoId"] = id;


            return View(prestacionEmpleado);
        }

        // GET: PrestacionEmpleados/Create
        public IActionResult Create(int id)
        {

            var prestacionGerencial = _context.Empleado.Where(x => x.Id == id).FirstOrDefault().PrestacionesGerenciales;

            var prestaciones = _context.Prestaciones.Where(x => x.PrestacionGerencial == false);

            if (prestacionGerencial)
            {
                prestaciones = _context.Prestaciones;
            }

            ViewData["NombreEmpleado"] = _context.Empleado.Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            ViewData["EmpleadoId"] = id;
            ViewData["PrestacionesId"] = new SelectList(prestaciones, "Id", "Nombre");
            return View();
        }

        // POST: PrestacionEmpleados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FechaEntrega,PrestacionesId,EmpleadoId")] PrestacionEmpleado prestacionEmpleado)
        {
            if (ModelState.IsValid)
            {
                _context.Add(prestacionEmpleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { Id = prestacionEmpleado.EmpleadoId });
            }
            ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", prestacionEmpleado.EmpleadoId);
            ViewData["PrestacionesId"] = new SelectList(_context.Prestaciones, "Id", "Nombre", prestacionEmpleado.PrestacionesId);
            return View(prestacionEmpleado);
        }

        // GET: PrestacionEmpleados/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var prestacionEmpleado = await _context.PrestacionEmpleado.FindAsync(id);
            if (prestacionEmpleado == null)
            {
                return NotFound();
            }

            var prestacionGerencial = _context.Empleado.Where(x => x.Id == _context.PrestacionEmpleado.Where(x => x.Id == id)
                                           .FirstOrDefault().EmpleadoId).FirstOrDefault().PrestacionesGerenciales;

            var prestaciones = _context.Prestaciones.Where(x => x.PrestacionGerencial == false);

            if (prestacionGerencial)
            {
                prestaciones = _context.Prestaciones;

            }

            ViewData["EmpleadoId"] = id;
            ViewData["PrestacionesId"] = new SelectList(prestaciones, "Id", "Nombre", prestacionEmpleado.PrestacionesId);
            return View(prestacionEmpleado);
        }


        // POST: PrestacionEmpleados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FechaEntrega,PrestacionesId,EmpleadoId")] PrestacionEmpleado prestacionEmpleado)
        {
            if (id != prestacionEmpleado.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(prestacionEmpleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PrestacionEmpleadoExists(prestacionEmpleado.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmpleadoId"] = id;
            ViewData["PrestacionesId"] = new SelectList(_context.Prestaciones, "Id", "Nombre", prestacionEmpleado.PrestacionesId);
            return View(prestacionEmpleado);
        }

        // GET: PrestacionEmpleados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prestacionEmpleado = await _context.PrestacionEmpleado
                .Include(p => p.Empleado)
                .Include(p => p.Prestaciones)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (prestacionEmpleado == null)
            {
                return NotFound();
            }
            ViewData["EmpleadoId"] = id;

            return View(prestacionEmpleado);
        }

        // POST: PrestacionEmpleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var prestacionEmpleado = await _context.PrestacionEmpleado.FindAsync(id);
            _context.PrestacionEmpleado.Remove(prestacionEmpleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PrestacionEmpleadoExists(int id)
        {
            return _context.PrestacionEmpleado.Any(e => e.Id == id);
        }

        public async Task<IActionResult> IndexPrestacion(int id)
        {
            var prestacionesEmpleado = await _context.PrestacionEmpleado.Include(x => x.Empleado).Include(x => x.Prestaciones).Where(x => x.Prestaciones.Id == id).ToListAsync();
            if (prestacionesEmpleado == null)
            {
                return NotFound();
            }

            ViewData["NombrePrestacion"] = _context.Prestaciones.Where(x => x.Id == id).Select(x => x.Nombre).FirstOrDefault();

            return View(prestacionesEmpleado);
        }
    }
}
