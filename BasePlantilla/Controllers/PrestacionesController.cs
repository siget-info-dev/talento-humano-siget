﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class PrestacionesController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public PrestacionesController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Prestaciones
        public async Task<IActionResult> Index()
        {
            return View(await _context.Prestaciones.ToListAsync());
        }

        // GET: Prestaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prestaciones = await _context.Prestaciones
                .FirstOrDefaultAsync(m => m.Id == id);
            if (prestaciones == null)
            {
                return NotFound();
            }

            return View(prestaciones);
        }

        // GET: Prestaciones/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Prestaciones/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,PrestacionGerencial")] Prestaciones prestaciones)
        {
            if (ModelState.IsValid)
            {
                _context.Add(prestaciones);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(prestaciones);
        }

        // GET: Prestaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prestaciones = await _context.Prestaciones.FindAsync(id);
            if (prestaciones == null)
            {
                return NotFound();
            }
            return View(prestaciones);
        }

        // POST: Prestaciones/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,PrestacionGerencial")] Prestaciones prestaciones)
        {
            if (id != prestaciones.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(prestaciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PrestacionesExists(prestaciones.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(prestaciones);
        }

        // GET: Prestaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prestaciones = await _context.Prestaciones
                .FirstOrDefaultAsync(m => m.Id == id);
            if (prestaciones == null)
            {
                return NotFound();
            }

            return View(prestaciones);
        }

        // POST: Prestaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var prestaciones = await _context.Prestaciones.FindAsync(id);
            _context.Prestaciones.Remove(prestaciones);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PrestacionesExists(int id)
        {
            return _context.Prestaciones.Any(e => e.Id == id);
        }

        /// <summary>
        /// realiza la asignacion de la prestacion a todos los empleados que apliquen.
        /// </summary>
        /// <param name="PrestacionId"></param>
        /// <returns>Vista</returns>
        public IActionResult GenerarListado(int PrestacionId) {
            var PrestacionGerencia = _context.Prestaciones.Where(x => x.Id == PrestacionId).Where(x => x.PrestacionAplicada == false).FirstOrDefault();

            if (PrestacionGerencia == null) {
                return RedirectToAction(nameof(Index));
            }

            IQueryable<Empleado> empleados;

            if (PrestacionGerencia.PrestacionGerencial)
            {
                empleados = _context.Empleado.Where(x => x.Activo == true).Where(x => x.PrestacionesGerenciales == true);
            }
            else {
                empleados = _context.Empleado.Where(x => x.Activo == true);
            }

            foreach (var empleado in empleados) {
                PrestacionEmpleado  prestacion =
                new PrestacionEmpleado
                {
                    FechaEntrega = DateTime.Now,
                    PrestacionesId = PrestacionId,
                    EmpleadoId = empleado.Id
                };

                _context.Add(prestacion);
            }

            PrestacionGerencia.PrestacionAplicada = true;

            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }


    }
}
