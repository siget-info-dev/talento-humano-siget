﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class TipoEvaluacionesController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public TipoEvaluacionesController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: TipoEvaluaciones
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoEvaluaciones.ToListAsync());
        }

        // GET: TipoEvaluaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoEvaluaciones = await _context.TipoEvaluaciones
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoEvaluaciones == null)
            {
                return NotFound();
            }

            return View(tipoEvaluaciones);
        }

        // GET: TipoEvaluaciones/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoEvaluaciones/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre")] TipoEvaluaciones tipoEvaluaciones)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoEvaluaciones);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoEvaluaciones);
        }

        // GET: TipoEvaluaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoEvaluaciones = await _context.TipoEvaluaciones.FindAsync(id);
            if (tipoEvaluaciones == null)
            {
                return NotFound();
            }
            return View(tipoEvaluaciones);
        }

        // POST: TipoEvaluaciones/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] TipoEvaluaciones tipoEvaluaciones)
        {
            if (id != tipoEvaluaciones.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoEvaluaciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoEvaluacionesExists(tipoEvaluaciones.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoEvaluaciones);
        }

        // GET: TipoEvaluaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoEvaluaciones = await _context.TipoEvaluaciones
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoEvaluaciones == null)
            {
                return NotFound();
            }

            return View(tipoEvaluaciones);
        }

        // POST: TipoEvaluaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoEvaluaciones = await _context.TipoEvaluaciones.FindAsync(id);
            _context.TipoEvaluaciones.Remove(tipoEvaluaciones);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoEvaluacionesExists(int id)
        {
            return _context.TipoEvaluaciones.Any(e => e.Id == id);
        }
    }
}
