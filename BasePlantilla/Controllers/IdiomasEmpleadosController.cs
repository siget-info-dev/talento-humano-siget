﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class IdiomasEmpleadosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public IdiomasEmpleadosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: IdiomasEmpleados
        public async Task<IActionResult> Index(int? id)
        {
            var talentoHumanoDbContext = _context.IdiomasEmpleado
                .Include(i => i.Empleado)
                .Include(i => i.Idioma)
                .Where(i=> i.EmpleadoId == id);

            ViewData["CodigoEmpleado"] = id;
            ViewData["NombreEmpleado"] = _context.Empleado.ToList().Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: IdiomasEmpleados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var idiomasEmpleado = await _context.IdiomasEmpleado
                .Include(i => i.Empleado)
                .Include(i => i.Idioma)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (idiomasEmpleado == null)
            {
                return NotFound();
            }

            return View(idiomasEmpleado);
        }

        // GET: IdiomasEmpleados/Create
        public IActionResult Create(int? id)
        {
            ViewData["NombreEmpleado"] = _context.Empleado.Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            ViewData["EmpleadoId"] = id;

            ViewData["IdiomaId"] = new SelectList(_context.Idiomas, "Id", "Idioma");
            return View();
        }

        // POST: IdiomasEmpleados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NivelIdioma,IdiomaId,EmpleadoId")] IdiomasEmpleado idiomasEmpleado)
        {
            if (ModelState.IsValid)
            {
                _context.Add(idiomasEmpleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { Id = idiomasEmpleado.EmpleadoId });
            }
            return View(idiomasEmpleado);
        }

        // GET: IdiomasEmpleados/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var idiomasEmpleado = await _context.IdiomasEmpleado.Include(x => x.Empleado).Where(x => x.Id == id).FirstOrDefaultAsync();
                
            if (idiomasEmpleado == null)
            {
                return NotFound();
            }
            ViewData["IdiomaId"] = new SelectList(_context.Idiomas, "Id", "Idioma", idiomasEmpleado.IdiomaId);
            return View(idiomasEmpleado);
        }

        // POST: IdiomasEmpleados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NivelIdioma,IdiomaId,EmpleadoId")] IdiomasEmpleado idiomasEmpleado)
        {
            if (id != idiomasEmpleado.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(idiomasEmpleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!IdiomasEmpleadoExists(idiomasEmpleado.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { Id = idiomasEmpleado.EmpleadoId});
            }
            ViewData["IdiomaId"] = new SelectList(_context.Idiomas, "Id", "Idioma", idiomasEmpleado.IdiomaId);
            return View(idiomasEmpleado);
        }

        // GET: IdiomasEmpleados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var idiomasEmpleado = await _context.IdiomasEmpleado
                .Include(i => i.Empleado)
                .Include(i => i.Idioma)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (idiomasEmpleado == null)
            {
                return NotFound();
            }

            return View(idiomasEmpleado);
        }

        // POST: IdiomasEmpleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var idiomasEmpleado = await _context.IdiomasEmpleado.FindAsync(id);
            _context.IdiomasEmpleado.Remove(idiomasEmpleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { id = idiomasEmpleado.EmpleadoId});
        }

        private bool IdiomasEmpleadoExists(int id)
        {
            return _context.IdiomasEmpleado.Any(e => e.Id == id);
        }
    }
}
