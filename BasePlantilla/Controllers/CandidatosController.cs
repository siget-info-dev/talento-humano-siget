﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class CandidatosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public CandidatosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Candidatos
        public async Task<IActionResult> Index()
        {
            var talentoHumanoDbContext = _context.Candidato.Include(c => c.Educacion).Include(c => c.ProfesionOficio);
            
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: Candidatos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidato
                .Include(c => c.Educacion)
                .Include(c => c.ProfesionOficio)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidato == null)
            {
                return NotFound();
            }

            return View(candidato);
        }

        // GET: Candidatos/Create
        public IActionResult Create()
        {
            ViewData["EducacionId"] = new SelectList(_context.Educacion, "Id", "Nombre");
            ViewData["ProfesionOficioId"] = new SelectList(_context.ProfesionOficio, "Id", "Nombre");
            return View();
        }

        // POST: Candidatos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,PrimerNombre,SegundoNombre,PrimerApellido,SegundoApellido,ApellidoCasada,Telefono,Email,NombreArchivo,ProfesionOficioId,EducacionId,FFArchivo")] Candidato candidato)
        {
            if (ModelState.IsValid)
            {
                if (candidato.FFArchivo != null)
                {
                    candidato.NombreArchivo = Tools.subeArchivo(candidato.FFArchivo);
                }

                _context.Add(candidato);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EducacionId"] = new SelectList(_context.Educacion, "Id", "Nombre", candidato.EducacionId);
            ViewData["ProfesionOficioId"] = new SelectList(_context.ProfesionOficio, "Id", "Nombre", candidato.ProfesionOficioId);
            return View(candidato);
        }

        // GET: Candidatos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidato.FindAsync(id);
            if (candidato == null)
            {
                return NotFound();
            }
            ViewData["EducacionId"] = new SelectList(_context.Educacion, "Id", "Nombre", candidato.EducacionId);
            ViewData["ProfesionOficioId"] = new SelectList(_context.ProfesionOficio, "Id", "Nombre", candidato.ProfesionOficioId);
            return View(candidato);
        }

        // POST: Candidatos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,PrimerNombre,SegundoNombre,PrimerApellido,SegundoApellido,ApellidoCasada,Telefono,Email,NombreArchivo,ProfesionOficioId,EducacionId")] Candidato candidato)
        {
            if (id != candidato.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(candidato);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CandidatoExists(candidato.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EducacionId"] = new SelectList(_context.Educacion, "Id", "Nombre", candidato.EducacionId);
            ViewData["ProfesionOficioId"] = new SelectList(_context.ProfesionOficio, "Id", "Nombre", candidato.ProfesionOficioId);
            return View(candidato);
        }

        // GET: Candidatos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidato
                .Include(c => c.Educacion)
                .Include(c => c.ProfesionOficio)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidato == null)
            {
                return NotFound();
            }

            return View(candidato);
        }

        // POST: Candidatos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var candidato = await _context.Candidato.FindAsync(id);
            _context.Candidato.Remove(candidato);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CandidatoExists(int id)
        {
            return _context.Candidato.Any(e => e.Id == id);
        }
    }
}
