﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class HorariosEmpleadosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public HorariosEmpleadosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: HorariosEmpleados
        public async Task<IActionResult> Index(int Id)
        {
            ViewData["HorarioId"] = Id;
            ViewData["Horario"] = _context.Horarios.Where(x=> x.Id == Id).Select(x=>x.Descripcion).FirstOrDefault();
            ViewData["FechasDesde"] = _context.Horarios.Where(x => x.Id == Id).Select(x => x.Inicio).FirstOrDefault();
            ViewData["FechasFin"] = _context.Horarios.Where(x => x.Id == Id).Select(x => x.Final).FirstOrDefault();


            var talentoHumanoDbContext = _context.HorariosEmpleado.Include(h => h.Empleado)
                    .Include(h => h.Horarios).Where(h=>h.HorariosId == Id);
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        //// GET: HorariosEmpleados/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var horariosEmpleado = await _context.HorariosEmpleado
        //        .Include(h => h.Empleado)
        //        .Include(h => h.Horarios)
        //        .FirstOrDefaultAsync(m => m.id == id);
        //    if (horariosEmpleado == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(horariosEmpleado);
        //}

        // GET: HorariosEmpleados/Create
        public IActionResult Create(int Id)
        {
            ViewData["Horario"] = _context.Horarios.Find(Id).Descripcion;
            ViewData["HorariosId"] = Id;

            var empleados = _context.Empleado.Select(x => new { NombreCompleto = x.GetNombreCompleto(), Id = x.Id });

            ViewData["empleados"] = new SelectList(empleados, "Id", "NombreCompleto");
            return View();
        }

        // POST: HorariosEmpleados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,EmpleadoId")] HorariosEmpleado horariosEmpleado)
        {
            //todo: Se hace la asignacion a horariosId de esta forma por que no viaja desde la vista
            //se tiene que hacer el cambio.
            var id = horariosEmpleado.id;
            horariosEmpleado.HorariosId = id;
            if (ModelState.IsValid)
            {
                horariosEmpleado.id = 0;
                _context.Add(horariosEmpleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id });
            }

            ViewData["Horario"] = _context.Horarios.Find(horariosEmpleado.id).Descripcion;
            ViewData["HorariosId"] = horariosEmpleado.id;

            var empleados = _context.Empleado.Select(x => new { NombreCompleto = x.GetNombreCompleto(), Id = x.Id });
            ViewData["empleados"] = new SelectList(empleados, "Id", "NombreCompleto");
            return View(horariosEmpleado);
        }

        //// GET: HorariosEmpleados/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var horariosEmpleado = await _context.HorariosEmpleado.FindAsync(id);
        //    if (horariosEmpleado == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", horariosEmpleado.EmpleadoId);
        //    ViewData["HorariosId"] = new SelectList(_context.Horarios, "Id", "Id", horariosEmpleado.HorariosId);
        //    return View(horariosEmpleado);
        //}

        //// POST: HorariosEmpleados/Edit/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for 
        //// more details, see htt p://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("id,HorariosId,EmpleadoId")] HorariosEmpleado horariosEmpleado)
        //{
        //    if (id != horariosEmpleado.id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(horariosEmpleado);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!HorariosEmpleadoExists(horariosEmpleado.id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", horariosEmpleado.EmpleadoId);
        //    ViewData["HorariosId"] = new SelectList(_context.Horarios, "Id", "Id", horariosEmpleado.HorariosId);
        //    return View(horariosEmpleado);
        //}

        // GET: HorariosEmpleados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horariosEmpleado = await _context.HorariosEmpleado
                .Include(h => h.Empleado)
                .Include(h => h.Horarios)
                .FirstOrDefaultAsync(m => m.id == id);
            if (horariosEmpleado == null)
            {
                return NotFound();
            }

            return View(horariosEmpleado);
        }

        // POST: HorariosEmpleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var horariosEmpleado = await _context.HorariosEmpleado.FindAsync(id);
            _context.HorariosEmpleado.Remove(horariosEmpleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { id = horariosEmpleado.HorariosId });
        }

        private bool HorariosEmpleadoExists(int id)
        {
            return _context.HorariosEmpleado.Any(e => e.id == id);
        }
    }
}
