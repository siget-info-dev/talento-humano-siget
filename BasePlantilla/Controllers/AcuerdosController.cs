﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class AcuerdosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public AcuerdosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Acuerdos
        public async Task<IActionResult> Index()
        {
            var talentoHumanoDbContext = _context.Acuerdos.Include(a => a.Acuerdo).Include(a => a.Empleado);
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: Acuerdos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var acuerdos = await _context.Acuerdos
                .Include(a => a.Acuerdo)
                .Include(a => a.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (acuerdos == null)
            {
                return NotFound();
            }

            return View(acuerdos);
        }

        // GET: Acuerdos/Create
        public IActionResult Create()
        {
            var empleados = _context.Empleado.ToList().Select(x => new { x.Id, nombreCompleto = x.GetNombreCompleto() });

            ViewData["AcuerdoId"] = new SelectList(_context.TipoAcuerdos, "Id", "Acuerdo");
            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "nombreCompleto");
            return View();
        }

        // POST: Acuerdos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NumeroAcuerdo,FechaAcuerdo,Comentarios,EmpleadoId,AcuerdoId")] Acuerdos acuerdos)
        {
            if (ModelState.IsValid)
            {
                _context.Add(acuerdos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            var empleados = _context.Empleado.ToList().Select(x => new { x.Id, nombreCompleto = x.GetNombreCompleto() });

            ViewData["AcuerdoId"] = new SelectList(_context.TipoAcuerdos, "Id", "Acuerdo", acuerdos.AcuerdoId);
            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "nombreCompleto", acuerdos.EmpleadoId);
            return View(acuerdos);
        }

        // GET: Acuerdos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var acuerdos = await _context.Acuerdos.FindAsync(id);
            if (acuerdos == null)
            {
                return NotFound();
            }

            var empleados = _context.Empleado.ToList().Select(x => new { x.Id, nombreCompleto = x.GetNombreCompleto() });

            ViewData["AcuerdoId"] = new SelectList(_context.TipoAcuerdos, "Id", "Acuerdo", acuerdos.AcuerdoId);
            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "nombreCompleto", acuerdos.EmpleadoId);
            return View(acuerdos);
        }

        // POST: Acuerdos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NumeroAcuerdo,FechaAcuerdo,Comentarios,EmpleadoId,AcuerdoId")] Acuerdos acuerdos)
        {
            if (id != acuerdos.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(acuerdos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AcuerdosExists(acuerdos.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            var empleados = _context.Empleado.ToList().Select(x => new { x.Id, nombreCompleto = x.GetNombreCompleto() });

            ViewData["AcuerdoId"] = new SelectList(_context.TipoAcuerdos, "Id", "Acuerdo", acuerdos.AcuerdoId);
            ViewData["EmpleadoId"] = new SelectList(empleados, "Id", "nombreCompleto", acuerdos.EmpleadoId);
            return View(acuerdos);
        }

        // GET: Acuerdos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var acuerdos = await _context.Acuerdos
                .Include(a => a.Acuerdo)
                .Include(a => a.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (acuerdos == null)
            {
                return NotFound();
            }

            return View(acuerdos);
        }

        // POST: Acuerdos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var acuerdos = await _context.Acuerdos.FindAsync(id);
            _context.Acuerdos.Remove(acuerdos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AcuerdosExists(int id)
        {
            return _context.Acuerdos.Any(e => e.Id == id);
        }
    }
}
