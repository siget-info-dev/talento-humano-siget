﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class MiembrosFamiliaController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public MiembrosFamiliaController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: MiembrosFamilia
        public async Task<IActionResult> Index()
        {
            return View(await _context.MiembrosFamilia.ToListAsync());
        }

        // GET: MiembrosFamilia/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var miembrosFamilia = await _context.MiembrosFamilia
                .FirstOrDefaultAsync(m => m.Id == id);
            if (miembrosFamilia == null)
            {
                return NotFound();
            }

            return View(miembrosFamilia);
        }

        // GET: MiembrosFamilia/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MiembrosFamilia/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Miembro")] MiembrosFamilia miembrosFamilia)
        {
            if (ModelState.IsValid)
            {
                _context.Add(miembrosFamilia);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(miembrosFamilia);
        }

        // GET: MiembrosFamilia/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var miembrosFamilia = await _context.MiembrosFamilia.FindAsync(id);
            if (miembrosFamilia == null)
            {
                return NotFound();
            }
            return View(miembrosFamilia);
        }

        // POST: MiembrosFamilia/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Miembro")] MiembrosFamilia miembrosFamilia)
        {
            if (id != miembrosFamilia.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(miembrosFamilia);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MiembrosFamiliaExists(miembrosFamilia.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(miembrosFamilia);
        }

        // GET: MiembrosFamilia/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var miembrosFamilia = await _context.MiembrosFamilia
                .FirstOrDefaultAsync(m => m.Id == id);
            if (miembrosFamilia == null)
            {
                return NotFound();
            }

            return View(miembrosFamilia);
        }

        // POST: MiembrosFamilia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var miembrosFamilia = await _context.MiembrosFamilia.FindAsync(id);
            _context.MiembrosFamilia.Remove(miembrosFamilia);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MiembrosFamiliaExists(int id)
        {
            return _context.MiembrosFamilia.Any(e => e.Id == id);
        }
    }
}
