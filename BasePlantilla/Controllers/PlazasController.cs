﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;
using SistemaGestionTalentoHumano.Services;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class PlazasController : Controller
    {
        private readonly IPlazaRepository _plazaRepository;
        private readonly TalentoHumanoDbContext _context;

        public PlazasController(
            IPlazaRepository plazaRepository,
            TalentoHumanoDbContext context)
        {
            _plazaRepository = plazaRepository;
            _context = context;
        }

        // GET: Plazas
        public IActionResult Index(int? tipoAlerta)
        {
            ViewBag.TipoAlerta = tipoAlerta;
            return View(_plazaRepository.GetAll());
        }

        // GET: Plazas/Details/5
        public IActionResult Details(int id)
        {
            var plaza = _plazaRepository.Get(id);
            if (plaza == null)
            {
                return NotFound();
            }

            return View(plaza);
        }

        // GET: Plazas/Create
        public IActionResult Create()
        {
            LoadViewData();
            return View();
        }

        // POST: Plazas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Codigo,Nombre,NombreCorto,DepartamentoSigetId,SubDepartamentoSigetId,Acuerdo,CodigoPlaza,PlazaActiva,FamiliaPlazasId,FechaCreacion")] Plaza plaza)
        {
            if (ModelState.IsValid)
            {
                _plazaRepository.Create(plaza);

                if (!_plazaRepository.Save())
                {
                    throw new Exception($"Error al guardar el objeto");
                }

                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }
            LoadViewData();
            return View(plaza);
        }

        // GET: Plazas/Edit/5
        public IActionResult Edit(int id)
        {
            var plaza = _plazaRepository.Get(id);

            if (plaza == null)
            {
                return NotFound();
            }

            LoadViewData();
            return View(plaza);
        }

        // POST: Plazas/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Codigo,Nombre,NombreCorto,DepartamentoSigetId,SubDepartamentoSigetId,Acuerdo,CodigoPlaza,PlazaActiva,FamiliaPlazasId,FechaCreacion")] Plaza plaza)
        {
            if (id != plaza.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _plazaRepository.Update(plaza);

                if (!_plazaRepository.Save())
                {
                    throw new Exception($"Error al actualizar el objeto con id: {id}.");
                }

                return RedirectToAction(nameof(Index), new { tipoAlerta = 1 });
            }
            LoadViewData();
            return View(plaza);
        }

        // GET: Plazas/Delete/5
        public IActionResult Delete(int id)
        {
            var plaza = _plazaRepository.Get(id);

            if (plaza == null)
            {
                return NotFound();
            }

            return View(plaza);
        }

        // POST: Plazas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plaza = _plazaRepository.Get(id);
            _plazaRepository.Delete(plaza);

            if (!_plazaRepository.Save())
            {
                throw new Exception($"Error al eliminar el objeto con id: {id}.");
            }

            return RedirectToAction(nameof(Index), new { tipoAlerta = 2 });
        }

        private void LoadViewData()
        {
            ViewData["GerenciaId"] = new SelectList(_context.Gerencias, "Id", "Nombre");
            ViewData["FamiliaPlazaId"] = new SelectList(_context.FamiliaPlazas, "Id", "Nombre");

            //if (plaza != null) {

            //    var familiaPlaza = _context.FamiliaPlazas.Where(x => x.Id == plaza.FamiliaPlazasId);

            //    ViewData["FamiliaPlazaId"] = new SelectList(familiaPlaza, "Id", "Nombre");

            //}

            ViewData["DepartamentoSigetId"] = new SelectList(_context.DepartamentosSiget, "Id", "Nombre");
        }

        public ActionResult<IEnumerable<DepartamentoSiget>> GetDepartamentosByGerencia(int? id)
        {
            var departamentosByGerencia = _context.DepartamentosSiget.ToList();
            if (id != null) { departamentosByGerencia = _context.DepartamentosSiget.Where(x => x.GerenciaId == id).ToList(); }
            return departamentosByGerencia;
        }
    }
}
