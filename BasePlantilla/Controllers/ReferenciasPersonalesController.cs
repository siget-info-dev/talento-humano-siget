﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class ReferenciasPersonalesController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public ReferenciasPersonalesController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: ReferenciasPersonales
        public async Task<IActionResult> Index(int? id)
        {
            var talentoHumanoDbContext = _context.ReferenciasPersonales.Include(r => r.Empleado).Where(r=> r.EmpleadoId == id);

            ViewData["CodigoEmpleado"] = id;
            ViewData["NombreEmpleado"] = _context.Empleado.ToList().Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            return View(await talentoHumanoDbContext.ToListAsync());
        }

        // GET: ReferenciasPersonales/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var referenciasPersonales = await _context.ReferenciasPersonales
                .Include(r => r.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (referenciasPersonales == null)
            {
                return NotFound();
            }

            return View(referenciasPersonales);
        }

        // GET: ReferenciasPersonales/Create
        public IActionResult Create(int? id)
        {
            ViewData["NombreEmpleado"] = _context.Empleado.Where(x => x.Id == id).Select(x => x.GetNombreCompleto()).FirstOrDefault();
            ViewData["EmpleadoId"] = id;
            return View();
        }

        // POST: ReferenciasPersonales/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,LugarTrabajo,Telefono,EmpleadoId")] ReferenciasPersonales referenciasPersonales)
        {
            if (ModelState.IsValid)
            {
                _context.Add(referenciasPersonales);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { Id = referenciasPersonales.EmpleadoId });
            }
            return View(referenciasPersonales);
        }

        // GET: ReferenciasPersonales/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var referenciasPersonales = await _context.ReferenciasPersonales
                .Include(x => x.Empleado)
                .Where(x => x.Id == id).FirstOrDefaultAsync();

            if (referenciasPersonales == null)
            {
                return NotFound();
            }
            ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", referenciasPersonales.EmpleadoId);
            return View(referenciasPersonales);
        }

        // POST: ReferenciasPersonales/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,LugarTrabajo,Telefono,EmpleadoId")] ReferenciasPersonales referenciasPersonales)
        {
            if (id != referenciasPersonales.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(referenciasPersonales);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReferenciasPersonalesExists(referenciasPersonales.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { Id = referenciasPersonales.EmpleadoId});
            }
            ViewData["EmpleadoId"] = new SelectList(_context.Empleado, "Id", "DireccionResidencia", referenciasPersonales.EmpleadoId);
            return View(referenciasPersonales);
        }

        // GET: ReferenciasPersonales/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var referenciasPersonales = await _context.ReferenciasPersonales
                .Include(r => r.Empleado)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (referenciasPersonales == null)
            {
                return NotFound();
            }

            return View(referenciasPersonales);
        }

        // POST: ReferenciasPersonales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var referenciasPersonales = await _context.ReferenciasPersonales.FindAsync(id);
            _context.ReferenciasPersonales.Remove(referenciasPersonales);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { id = referenciasPersonales.EmpleadoId });
        }

        private bool ReferenciasPersonalesExists(int id)
        {
            return _context.ReferenciasPersonales.Any(e => e.Id == id);
        }
    }
}
