﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Models;

namespace SistemaGestionTalentoHumano.Controllers
{
    public class PermisosController : Controller
    {
        private readonly TalentoHumanoDbContext _context;

        public PermisosController(TalentoHumanoDbContext context)
        {
            _context = context;
        }

        // GET: Permisos
        public async Task<IActionResult> Index()
        {
            return View(await _context.Permisos.Include(x=>x.Empleado).ToListAsync());
        }

        // GET: Permisos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permisos = await _context.Permisos.Include(x=>x.TipoPermiso)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (permisos == null)
            {
                return NotFound();
            }

            return View(permisos);
        }

        // GET: Permisos/Create
        public IActionResult Create()
        {   
            var tipoPermisos = _context.TipoPermisos.ToList();
            var empleados = _context.Empleado.ToList().Select(x=> new { 
                x.Id, 
                NombreCompleto = x.PrimerNombre
                                 + " " + x.SegundoNombre
                                 + " " + x.TercerNombre
                                 + " " + x.PrimerApellido
                                 + " " + x.SegundoApellido
                                 + " " + x.ApellidoCasada
            }).OrderBy(x => x.NombreCompleto);

            ViewData["TipoPermisos"] = new SelectList(tipoPermisos, nameof(TipoPermiso.Id), nameof(TipoPermiso.Permiso));
            ViewData["Empleados"] = new SelectList(empleados, "Id", "NombreCompleto");
            return View();
        }

        // POST: Permisos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,AplicaDescuento,Inicio,Fin,Comentario,EmpleadoId,TipoPermisoId")] Permisos permisos)
        {
            if (ModelState.IsValid)
            {
                permisos.Empleado = _context.Empleado.FirstOrDefault(x => x.Id == permisos.EmpleadoId);
                permisos.TipoPermiso = _context.TipoPermisos.FirstOrDefault(x => x.Id == permisos.IdPermiso);
                //permisos.Estado = 1;
                _context.Add(permisos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            var empleados = _context.Empleado.ToList().Select(x => new {
                x.Id,
                NombreCompleto = x.PrimerNombre
                     + " " + x.SegundoNombre
                     + " " + x.TercerNombre
                     + " " + x.PrimerApellido
                     + " " + x.SegundoApellido
                     + " " + x.ApellidoCasada
            }).OrderBy(x => x.NombreCompleto);

            ViewData["TipoPermisos"] = new SelectList(_context.TipoPermisos, nameof(TipoPermiso.Id), nameof(TipoPermiso.Permiso));
            ViewData["Empleados"] = new SelectList(empleados, "Id", "NombreCompleto");
            return View(permisos);
        }

        // GET: Permisos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permisos = await _context.Permisos.FindAsync(id);
            if (permisos == null)
            {
                return NotFound();
            }

            ViewData["TipoPermisoId"] = new SelectList(_context.TipoPermisos, "Id", "Permiso", permisos.TipoPermisoId);
            return View(permisos);
        }

        // POST: Permisos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,AplicaDescuento,Inicio,Fin,Estado,Comentario,EmpleadoId,TipoPermisoId")] Permisos permisos)
        {
            if (id != permisos.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(permisos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PermisosExists(permisos.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(permisos);
        }

        // GET: Permisos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permisos = await _context.Permisos.Include(x=>x.TipoPermiso)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (permisos == null)
            {
                return NotFound();
            }

            return View(permisos);
        }

        // POST: Permisos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var permisos = await _context.Permisos.FindAsync(id);
            _context.Permisos.Remove(permisos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PermisosExists(int id)
        {
            return _context.Permisos.Any(e => e.Id == id);
        }

     
    }
}
