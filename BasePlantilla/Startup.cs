using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SistemaGestionTalentoHumano.Data;
using SistemaGestionTalentoHumano.Services;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.UI;
using Microsoft.Graph;
using System.Net;
using System.Net.Http.Headers;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace BasePlantilla
{
    public class Startup
    {
        private readonly string administrador = RolesTypes.Administrador;
        private readonly string jefe = RolesTypes.Jefe;
        private readonly string auxiliar = RolesTypes.Auxiliar;
        private readonly string usuario = RolesTypes.Usuario;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // sin graph 
            //services.AddControllersWithViews();
            // sin graph

            services.AddDbContext<TalentoHumanoDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("TalentoHumanoContext"))
            );


            services.AddMvcCore().AddRazorRuntimeCompilation();

            //--------------Graph


            services.AddAuthentication(OpenIdConnectDefaults.AuthenticationScheme)
                    .AddMicrosoftIdentityWebApp(options =>
            {
                Configuration.Bind("AzureAd", options);

                options.Prompt = "select_account";

                options.Events.OnTokenValidated = async context =>
                {
                    var tokenAcquisition = context.HttpContext.RequestServices
                                        .GetRequiredService<ITokenAcquisition>();

                    var graphClient = new GraphServiceClient(
                        new DelegateAuthenticationProvider(async (request) =>
                        {
                            var token = await tokenAcquisition
                                .GetAccessTokenForUserAsync(Graph.Scopes, user: context.Principal);
                            request.Headers.Authorization =
                                new AuthenticationHeaderValue("Bearer", token);
                        })
                    );
                };

                options.Events.OnAuthenticationFailed = context =>
                {
                    var error = WebUtility.UrlEncode(context.Exception.Message);
                    context.Response
                        .Redirect($"/Home/ErrorWithMessage?message=Authentication+error&debug={error}");
                    context.HandleResponse();

                    return Task.FromResult(0);
                };

                options.Events.OnRemoteFailure = context =>
                {
                    if (context.Failure is OpenIdConnectProtocolException)
                    {
                        var error = WebUtility.UrlEncode(context.Failure.Message);
                        context.Response
                            .Redirect($"/Home/ErrorWithMessage?message=Sign+in+error&debug={error}");
                        context.HandleResponse();
                    }
                    return Task.FromResult(0);
                };
            })
            .EnableTokenAcquisitionToCallDownstreamApi(options =>
            {
                Configuration.Bind("AzureAd", options);
            }, Graph.Scopes)
            .AddMicrosoftGraph(options =>
            {
                options.Scopes = string.Join(' ', Graph.Scopes);
            })
            .AddInMemoryTokenCaches();

            services.AddControllersWithViews(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                .RequireAuthenticatedUser()
                                .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            }).AddMicrosoftIdentityUI();
            services.AddRazorPages();


            //-------------Graph

            // Registrando AutoMapper
            services.AddAutoMapper(typeof(Startup));
            // Registrando clases repositorios
            services.AddScoped<IBancoRepository, BancoRepository>();
            services.AddScoped<IFondoPensionRepository, FondoPensionRepository>();
            services.AddScoped<IGerenciaRepository, GerenciaRepository>();
            services.AddScoped<IDepartamentoSigetRepository, DepartamentoSigetRepository>();
            services.AddScoped<IPlazaRepository, PlazaRepository>();
            services.AddScoped<IEmpleadoRepository, EmpleadoRepository>();

            //Politicas 
            services.AddAuthorization(options =>
            {
                options.AddPolicy("RolAdministrador", policy => policy.RequireRole(administrador));
                options.AddPolicy("RolAuxiliar", policy => policy.RequireRole(auxiliar));
                options.AddPolicy("RolJefe", policy => policy.RequireRole(jefe));
                options.AddPolicy("RolUsuario", policy => policy.RequireRole(usuario));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
